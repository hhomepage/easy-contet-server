<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Login</title>
   <meta name="viewport" content="width=device-width">

<style>
  * { box-sizing:border-box; }

body {
  font-family: Helvetica;
  background: #eee;
  -webkit-font-smoothing: antialiased;
}

hgroup { 
  text-align:center;
  margin-top: 4em;
}

h1, h3 { font-weight: 300; }

h1 { color: #636363; }

h3 { color: #4a89dc; }

.form {
  width: 380px;
  margin: 4em auto;
  padding: 3em 2em 2em 2em;
  background: #fafafa;
  border: 1px solid #ebebeb;
  box-shadow: rgba(0,0,0,0.14902) 0px 1px 1px 0px,rgba(0,0,0,0.09804) 0px 1px 2px 0px;
}

.group { 
  position: relative; 
  margin-bottom: 45px; 
}

input {
  font-size: 18px;
  padding: 10px 10px 10px 5px;
  -webkit-appearance: none;
  display: block;
  background: #fafafa;
  color: #636363;
  width: 100%;
  border: none;
  border-radius: 0;
  border-bottom: 1px solid #757575;
}

input:focus { outline: none; }


/* Label */

label {
  color: #999; 
  font-size: 18px;
  font-weight: normal;
  position: absolute;
  pointer-events: none;
  left: 5px;
  top: 10px;
  transition: all 0.2s ease;
}


/* active */

input:focus ~ label, input.used ~ label {
  top: -20px;
  transform: scale(.75); left: -2px;
  /* font-size: 14px; */
  color: #4a89dc;
}


/* Underline */

.bar {
  position: relative;
  display: block;
  width: 100%;
}

.bar:before, .bar:after {
  content: '';
  height: 2px; 
  width: 0;
  bottom: 1px; 
  position: absolute;
  background: #4a89dc; 
  transition: all 0.2s ease;
}

.bar:before { left: 50%; }

.bar:after { right: 50%; }


/* active */

input:focus ~ .bar:before, input:focus ~ .bar:after { width: 50%; }


/* Highlight */

.highlight {
  position: absolute;
  height: 60%; 
  width: 100px; 
  top: 25%; 
  left: 0;
  pointer-events: none;
  opacity: 0.5;
}


/* active */

input:focus ~ .highlight {
  animation: inputHighlighter 0.3s ease;
}


/* Animations */

@keyframes inputHighlighter {
  from { background: #4a89dc; }
  to  { width: 0; background: transparent; }
}


/* Button */

.button {
  position: relative;
  display: inline-block;
  padding: 12px 24px;
  margin: .3em 0 1em 0;
  width: 100%;
  vertical-align: middle;
  color: #fff;
  font-size: 16px;
  line-height: 20px;
  -webkit-font-smoothing: antialiased;
  text-align: center;
  letter-spacing: 1px;
  background: transparent;
  border: 0;
  border-bottom: 2px solid #3160B6;
  cursor: pointer;
  transition: all 0.15s ease;
}
.button:focus { outline: 0; }


/* Button modifiers */

.buttonBlue {
  background: #4a89dc;
  text-shadow: 1px 1px 0 rgba(39, 110, 204, .5);
}

.buttonBlue:hover { background: #357bd8; }


/* Ripples container */

.ripples {
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  overflow: hidden;
  background: transparent;
}


/* Ripples circle */

.ripplesCircle {
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  opacity: 0;
  width: 0;
  height: 0;
  border-radius: 50%;
  background: rgba(255, 255, 255, 0.25);
}

.ripples.is-active .ripplesCircle {
  animation: ripples .4s ease-in;
}


/* Ripples animation */

@keyframes ripples {
  0% { opacity: 0; }

  25% { opacity: 1; }

  100% {
    width: 200%;
    padding-bottom: 200%;
    opacity: 0;
  }
}

footer { text-align: center; }

footer p {
  color: #888;
  font-size: 13px;
  letter-spacing: .4px;
}

footer a {
  color: #4a89dc;
  text-decoration: none;
  transition: all .2s ease;
}

footer a:hover {
  color: #666;
  text-decoration: underline;
}

footer img {
  width: 80px;
  transition: all .2s ease;
}

.link-btns{
    padding: 6px;
    text-decoration: none;
    border: 1px solid #E0E0E0;
    margin-bottom: 5px;
    display: block;
}

#company-view ul{
   list-style-type: none;
    margin: 0;
    border: 1px solid #c4c4c4;
    padding: 10px;
}

#user-info > div{
    display: flex;
    flex-direction: column;
    align-items: center;
}

#user-info img{
    height: 80px;
    border-radius: 50%;
    min-width: 80px;
}


footer img:hover { opacity: .83; }

footer img:focus , footer a:focus { outline: none; }

.text-align-start{
  text-align: start;
}

</style>

</head>


<body>
    <hgroup>
      <h1>Welcome To Easy Content</h1>
      <h3 class="message"><span>To continue please login</span></h3>
    </hgroup>
    
    <div class="form" id="company-view" style="display: none">
        <ul></ul>
        <div><a href="javascript:;" class="link-btns go-back">Go To login</a></div>
    </div>  
  
    <form id="login-form" class="form">
  
      <div id="user-info">
          
      </div>

      <div class="group">
        <input type="email" name="email"><span class="highlight"></span><span class="bar"></span>
        <label>Email</label>
      </div>
      <div class="group">
        <input type="password" name="password"><span class="highlight"></span><span class="bar"></span>
        <label>Password</label>
      </div>
      <button type="submit" class="button buttonBlue">Login
        <div class="ripples buttonRipples"><span class="ripplesCircle"></span></div>
      </button>

      <div><a href="{{ url('password/reset') }}" class="link-btns">Reset Password
      </a></div>
      

       <div><a href="{{ url('company') }}" class="link-btns">Create Your Own Company</a></div>

    </form>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
  <script>
      
      $(window, document, undefined).ready(function() {

          $('#login-form').on('submit', function(e)
          {
              e.preventDefault();
              toogleDisableSubBtn();
              var formData = $(this).serialize();

               $.ajax({
                type: 'POST',
                data: formData,
                dataType: 'JSON',
                url: "{{ url('/login') }}",
                success: function(res){

                  toogleDisableSubBtn(res.message);
                  showViewOfCompanies(res.data.companies);
                  $('#login-form').hide();                  

                },
                error: function(res)
                {
                    toogleDisableSubBtn(res.responseJSON.message);
                    checkIfUserAv(res.responseJSON);
                }
              })
          });

          function checkIfUserAv(res)
          {
              if( res.data.user )
              {
                 var html = '<div><div class="img-holder"><img src="'+res.data.user.avatar+'"></div><h3>'+ res.data.user.name +'</h3></div>';
                
                $('#user-info').html(html);

              }
              else
              {
                  $('#user-info').html('');
              }


          }

          function showViewOfCompanies(companies)
          {
              var companyHtml = '';

              companies.forEach(function(company){
                  companyHtml +='<li data-company-id="'+company.id+'">\
                    <button class="button buttonBlue text-align-start">'+company.name+'\
                      <div class="ripples buttonRipples"><span class="ripplesCircle"></span></div>\
                    </button></li>';
              });

              $('#company-view ul').html( companyHtml );

              $('#company-view').show();
          }

          function toogleDisableSubBtn(message){
            var btn = $('#login-form button[type="submit"]');
            var old = btn.attr('disabled');
            if( old !== undefined )
            {
                btn.removeAttr('disabled');
                btn.text('Login');
                $('.message span').text(message || "To continue please login");
            }
            else{
                btn.attr('disabled', true);
                btn.text('Submitting..');
            }
          }

          function changeMessage(message, type)
          {
              $('.message span').text(message);
          }

          $('.go-back').on('click', goBack);

          function goBack()
          {
              $('#company-view ul').html('');
              $('#company-view').hide();
              $('#login-form').show();
              toogleDisableSubBtn();
          }

          $(document).on('click','[data-company-id] button', function()
          {
              var companyId = $(this).parent().attr('data-company-id');

              var $this = $(this);

               $.ajax({
                  type: 'POST',
                  dataType: 'JSON',
                  url: "{{ url('/select-company') }}" + '/' + companyId,
                  success: function(res){
                    window.location = res.data.redirectTo;
                  },
                  error: function(res)
                  {
                      $this.attr('disabled', true);
                      changeMessage(res.responseJSON.message);
                  }
              })
          });

          $('input').blur(function() {
            var $this = $(this);
            if ($this.val())
              $this.addClass('used');
            else
              $this.removeClass('used');
          });

          $(document).on('click','.ripples', function(e) {

            var $this = $(this);
            var $offset = $this.parent().offset();
            var $circle = $this.find('.ripplesCircle');

            var x = e.pageX - $offset.left;
            var y = e.pageY - $offset.top;

            $circle.css({
              top: y + 'px',
              left: x + 'px'
            });

            $this.addClass('is-active');

          });

          $(document).on('animationend webkitAnimationEnd mozAnimationEnd oanimationend MSAnimationEnd','.ripples', function(e) {
            $(this).removeClass('is-active');
          });

        });

  </script>

</body>
</html>

