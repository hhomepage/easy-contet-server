<table border="0">
<tr>
	<td><img src="{{ $user->avatar }}"></td>
	<td>
		<h3>
		{{ $user->name }} has invited you to {{ app('company')->name }}
		</h3>
	</td>
</tr>
	
	@if( $projects )
	<tr>
		He wants you to assign these projects

		@foreach($projects as $project)
			<td>
				
				{{ $project->name }}

			</td>
		@endforeach	

	</tr>
@endif

</table>

<br><br><br>

<a href="{{ url('invitation/'. $invitation->id .'/token/' . $invitation->token) }}" style="padding:20px;border:1px solid #ccc;">Accpet the invitation</a>

