<?php namespace Modules\UserManagement\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;
class UserRequest extends FormRequest {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		$userNameRule = 'unique:users';
		if( $user = Auth::user()  ) {
			$userNameRule.= ",username," . $user->id;
		}
		return [
			'username' => $userNameRule,
			'about' => 'min:2',
			'birthday' => 'date',
			'name' => 'min:3',
			'address' => 'min:3',
			'tel' => 'min:5',
			'avatar_base64' => 'regex:/^(?:data\:image\/png\;base64\,)/'
		];
	}

}
