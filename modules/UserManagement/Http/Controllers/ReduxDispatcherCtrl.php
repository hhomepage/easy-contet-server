<?php namespace Modules\Usermanagement\Http\Controllers;
use Pingpong\Modules\Routing\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Events\Authenticate;
use Res;
use App\User;
use Modules\Project\Entities\Project;

class ReduxDispatcherCtrl extends Controller {

	public $company;

	public $user;

	public function __construct()
	{
		$this->user = Auth::user();

		$this->company = app('company');
	}

	public function companyChannel($roleName)
	{
		return 'company_' .  $this->company->unique . '_' . $this->company->id . ( is_null($roleName) ? '' : '_' . $roleName );
	}
	
	public function handleRequest( Request $request )
	{
		$args = $request->get('args')['apiArgs'];
		$eventData = array_merge(
			// [ $this->companyChannel('admin') => ['action' => $request->get('reduxAction'),'args' => $args ] ], 
			$this->getForOthers($request)
		);

		$doMain = $this->moreEvents($request);

		return $this->dispatchRedux( $eventData );

	}

	private function moreEvents($request)
	{
		$args = $request->get('args');

		if( isset($args['apiArgs'] ) )
		{
			$apiArgs = $args['apiArgs'];

			$action = $request->get('reduxAction')['type'];

			if( $action === 'REVOKE_USER_PROJECT' )
			{
				$user = User::find($apiArgs['userId']);

				$this->dispatchEvent( ['secret' => $user->generateSecret(), 'projectId'=> $apiArgs['projectId'] ],'project:user-revoked' );

				return false;
			}

			if( $action === 'ASSIGN_USER_PROJECT' )
			{
				$user = User::find($apiArgs['userId']);

				$this->dispatchEvent( ['secret' => $user->generateSecret(), 'projectId'=> $apiArgs['projectId'] ],'project:user-assigned' );

				return false;
			}

			return true;
		}

	}

	public function dispatchRedux( $eventData ){
		
		$actionKey = str_random(10);

		$this->dispatchEvent( [ 'key' => $actionKey, 'eventData' => $eventData ] );

		return Res::success(['key' => $actionKey]);
	}

	public function dispatchEvent($data, $event = 'redux:dispatch-action')
    {
        event( new Authenticate( $data , $event ) );
    }

    public function getForOthers( $request )
    {
    	$fianlList = [];

    	$args = $request->get('args');

    	if( isset( $args['apiArgs'] )  &&  isset( $args['apiArgs']['projectId'] ))
    	{
    		$users = Project::find( $args['apiArgs']['projectId'] )->assignedUsers;

    		if( !$users->isEmpty() )
    		{
    			$users = $users->each(function($user) {

    				$user->secret = $user->generateSecret(); 

    			})->toArray();

    			foreach($users as $user)
    			{
    				$fianlList[ $user['secret'] ] = $this->modifyAction($user, $request);
    			}

    		}
    	}
		
		return $fianlList;
    }

    public function modifyAction($user, $request)
    {
    	$args = $request->get('args')['apiArgs'];

    	return ['action' => $request->get('reduxAction'), 'args' => $args ];
    }
}