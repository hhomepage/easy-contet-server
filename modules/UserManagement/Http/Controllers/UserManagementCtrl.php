<?php namespace Modules\UserManagement\Http\Controllers;

use App\User;

use Modules\UserManagement\Entities\Role;
use Modules\UserManagement\Entities\Invitation;
use Illuminate\Http\Request;
use Modules\UserManagement\Repositories\UserRepository;
use App\Events\Authenticate;

use Modules\UserManagement\Http\Requests\UserRequest;
use Modules\UserManagement\Http\Requests\InvitationRequest;
use Modules\UserManagement\Http\Requests\ChangePasswordRequest;
use Modules\UserManagement\Http\Requests\AcceptInvitationRequest;
use Modules\UserManagement\Http\Requests\CheckUsernameRequest;
use Modules\UserManagement\Http\Requests\ChannelRequest;
use Modules\Project\Entities\Project;


use Pingpong\Modules\Routing\Controller;

use Res;
use Mail;
use Auth;
use App\Company;
use Session;
use App\Label;
use App\Traits\ModelCRUD;
use App\Channel;
use Modules\UserManagement\Http\Requests\AddCompanyRequest;
use App\Notification;


class UserManagementCtrl extends Controller {

	use ModelCRUD;

	protected $user;

	public $company;

	public function __construct() {
		
		$this->user = Auth::user();
		
		$this->company = app('company');

		$this->setModel( new User );
	}

	public function registerCompany(AddCompanyRequest $request)
	{
		$user = User::whereEmail( $request->get('email') )->first();

		$emailParams = ['password' => ''];

		if( !$user )
		{
			$password = str_random(5);
			
			$emailParams['password'] = $password;

			$user = new User;
			
			$user->name = $request->get('name');
			
			$user->username = str_slug($request->get('name'),'_');

			if( $u = User::whereUsername($user->username)->first() )
			{
				$user->username = $user->username . '_' . str_random(2); 
			}
			
			$user->password = $password;
			
			$user->email = $request->get('email');

			$user->unique_token = str_random(10);

			$user->save();
		}

		$company = new Company;

		$company->paid = 0;

		$company->active = 0;

		$company->user_id = $user->id;

		$company->name = $request->get('company');

		$company->unique = str_slug( $request->get('company') );

		if( $_company = Company::whereUnique($company->unique)->first() )
		{
			$company->unique = $company->unique .'_'. str_random(2); 
		}

		$company->save();

		$creatorToken = str_random(10);

		$company->assign($user, $creatorToken);

		$user->_assignRole($company, Role::whereName('admin')->first());

		$token = encrypt( $company->unique . '=:=' . $company->id . '=:=' . $user->id . '=:=' . $creatorToken );

		$emailParams['url'] = url('/activate/company/' . $token);
		$emailParams['company'] = $company;
		$emailParams['user'] = $user;

		Mail::send('email.activation', $emailParams, function($m) use ($company, $user){
			$m->from(config('app.email'),'Activate Your Company ' . $company->name);
			$m->to($user->email, $user->name );
			$m->subject('Activate Your Company Account');
		});

		$parts = explode('@', $user->email);

	    $astrekiEmail = substr($parts[0], 0, min(1, strlen($parts[0])-1)) . str_repeat('*', max(1, strlen($parts[0]) - 1)) . '@' . $parts[1];

		return Res::success([],'We have created your account. Please check your email on ' . $astrekiEmail . ' and follow it to complete registration process');
	}

	public function activateCompany($token)
	{
		try{
			$token = decrypt( $token );
		}
		catch( \Expection $e ) {
			return Res::fail([]);
		}

		list($companyUnique, $companyId, $userId, $creatorToken) = explode('=:=', $token);

		if( $company = Company::buildForActivation( $companyUnique, $companyId ) )
		{
			if( $owner = User::find( $userId ) )
			{
				$company->active = 1;

				$this->save( [], $company );

				Auth::loginUsingId( $owner->id );

				Session::set('company', $company->toJson());

				return Res::redirectToHome($company->unique);
			}
		}

		return Res::fail(['invalid']);
	}

	public function getRoles(){
		return Res::success(Role::all());
	}


	public function saveUserDetails(UserRequest $request)
	{	
		if( $request->has('avatar_base64') )
		{
			$img = str_replace(' ', '+', str_replace('data:image/png;base64,', '', $request->get('avatar_base64')));

			$newName = str_random(8) . '-'.time().'.'.'png';
			
			$this->user->saveAvatar($newName, $img);

			$this->user->avatar = $newName;
		}

		$this->user->fill($request->all())->save();

		$response = $this->user->fresh();
		
		if($response)
		{
			event( new Authenticate(
				 [
				 	'payload' => $this->user->generateSecret(),
				 	'redux' => $response
				 ], 'redux:user-updated') );


			return Res::success($response);
		}

		return Res::fail();

	}

	public function inviteNewUser( InvitationRequest $request ){
		
		$invs = $request->get('inv');

		foreach($invs  as $inv)
		{
			if( $user = User::whereEmail($inv['email'])->first() )
			{
				if( $this->company->belongs( $user ) )
				{
					return Res::fail([],'Already Invited');
				}
			}

			$invitation = Invitation::whereEmail($inv['email'])->first();

			$invitation = is_null($invitation) ? new Invitation() : $invitation;

			$invitation->email = $inv['email'];

			$invitation->role_id = Role::find($inv['role']['id'])->id;
			
			$invitation->company_id = $this->company->id;

			$invitation->accepted = 0;

			$invitation->token = str_random( 20 );
			
			$invitation->inviter_id = $this->user->id;

			$invitation = $this->save( $request->all() , $invitation, false);

			$projects = null;

			if( $request->has('projects') )
			{
				$projects = Project::waitForInvitation($request->get('projects') , $invitation);
			}

			Mail::send('email.invitation', ['invitation' => $invitation, 'projects' => $projects,'user'=> $this->user], function($m) use ($invitation){
				$m->from(config('app.email'),'Invitation From Easy Content ' . $this->company->name);
				$m->to($invitation->email, explode('@',$invitation->email)[0] );
			});

		}
		
		return Res::success();
	}

	public function isUniqueUsername(CheckUsernameRequest $request)
	{
		return Res::success();
	}

	public function notifyUserReload($company)
	{
		event( new Authenticate( ['company' => $company->generateUnique() ] , 'redux:user-refresh') );
	}

	public function acceptInvitation(Invitation $invitation, $token)
	{
		if( $invitation->token === $token && $invitation->accepted === 0)
		{
			$user = User::whereEmail( $invitation->email )->first();
			
			$company = $invitation->company;
			
			$response = [];

			if( $user )
			{
				$invitation->user_id = $user->id;

				$invitation->accepted = 1;

				$invitation->save();

				$user->_assignRole( $company, Role::find( $invitation->role_id ) );

				$company->assign($user);

				$invitation->assignIfProjects( $user );

				$this->notifyUserReload( $company );

				Auth::loginUsingId( $invitation->user_id );

				Session::put('company', $company->toJson() );

				return Res::redirectToHome($company->unique);
			}
			
			return view('auth.invitation')->withToken( encrypt( $token . '-|-' . $invitation->id ) )->withCompany($company);

		}

		return Res::fail([],'Stalled Token');
	}


	public function acceptingInvitation(AcceptInvitationRequest $request){
		try{
			$token = decrypt( $request->get('token') );
		}
		catch( \Expection $e ) {
			return Res::fail([]);
		}

		list( $token, $id ) = explode('-|-',$token);

		if( $inv = Invitation::find( $id ) )
		{
			if( $inv->token === $token && $inv->accepted === 0)
			{
				$user = new User;
				
				$user->fill($request->all());

				$user->unique_token = str_random(10);

				if( $u = User::whereUsername($user->username)->first() )
				{
					$user->username = $user->username . '_' . $u->id; 
				}
				
				$user->email = $inv->email;
				
				$this->save([], $user );

				$inv->accepted = 1;
				$inv->user_id = $user->id;
				$this->save([], $inv);

				$role = Role::find($inv->role_id);
				$company = Company::find($inv->company_id);
				$user->_assignRole( $company, $role );
				$company->assign($user);

				$inv->assignIfProjects($user);

				$this->notifyUserReload( $company );

				Auth::loginUsingId( $inv->user_id );

				Session::put('company', $company->toJson() );

				return Res::success( [ 'homeUrl' => Res::getHomeUrl($company->unique) ] );
			}
		}

		return Res::fail([]);
	}

	public function removeUser(User $user)
	{
		if( $this->company->belongs($user) && $this->company->user_id !== $user->id )
		{
			$user->detachRoles( $user->roles );

			$this->notifyUserReload( $this->company );

			$this->clearAttachedResources($user);

			$this->company->revoke( $user );

			return Res::success([],'User Is Removed Successfully');
		}

		return Res::fail([],'User doesnt belongs');
	}

	public function changePassword(ChangePasswordRequest $request)
	{
		$password = $request->get('password');
		
		$this->user->password = $password;
		
		if( $this->save([], $this->user) )
		{
			return Res::success([],'Password Changed');
		}

		return Res::fail([]);
	}

	public function updateRole(Role $role , User $user)
	{
		if( $user->id !== $this->user->id && $user->_assignRole($this->company, $role) )
		{
			$this->notifyUserReload( $this->company );

	        return Res::success([],'Role Is Changed');
		}

		return Res::fail([]);

	}

	public function searchUsers(Request $request){
		
		if( $request->has('query') )
		{
			$query = $request->get('query');

			$query = preg_match('^@',$query ) ? substr($query, 1, strlen($query)) : $query;
				
			$users = $this->company->users;

			$user = User::where('name','Like', $query)->orWhere('username','Like',$query)->orWhere('email','LIKE',$query)->get();
	
			return Res::success($user);
		}

		return Res::fail([]);
	}

	public function allLabels()
	{
		return Res::success($this->company->labels);
	}

	public function userByRoles( $role ) {
		
		$allRoles = Role::select(['id','name','display_name'])->get();

		$roles = $role === 'all' ? $allRoles->lists('id')->toArray(): [$role->id];

		$_users = $this->company->users->toArray();

		$users = [];

		foreach($_users as $user)
		{
			$users[ $user['id'] ] = $user;
		}

		$refinedUsers =  Role::from('role_user as role')->whereIn('user_id', array_keys( $users ) )->where('company_id', $this->company->id)->join('users', function($query){
				return $query->on('role.user_id','=','users.id');
		})->get();

		$_users = [];

		foreach( $refinedUsers as $user )
		{
			$userEntity = $users[ $user->user_id ];
			
			$roleEntity = $allRoles->filter(function($_role) use($user){
				return $user->role_id === $_role->id;
			})->first();

			$userEntity['role_name'] = $roleEntity->name;

			$_users[] = array_merge($userEntity , ['role' => $roleEntity ] );
		}

		$res = ['users' => array_values($this->filterListByRole( $_users )) ,'labels' => $this->company->labels ];

		if( count($_users) < count($users) )
		{
			$activeUsers = array_column($_users,'id');

			$nonActiveUsers = array_diff( array_keys( $users ), $activeUsers);

			if( count($nonActiveUsers) !== 0 )
			{
				$nonActiveUsers = array_values($nonActiveUsers);
				
				$nonActiveUsers = array_map(function($u) use($users) {
					
					return $users[ $u ];

				}, $nonActiveUsers);

				$res['nonActiveUsers'] = $nonActiveUsers;
			}
		}

		$res['notifications'] = $this->user->getMyNofications();

		return Res::success( $res );
	}

	public function filterListByRole( $list)
	{
		if( $this->user->isAdmin() ) return $list;

		return array_filter($list, function($user)
		{
			 if( $this->user->_hasRole('client') && $user['role_name'] === 'developer' )
	        {
	           return false;
	        }

	        if( $user['role_name'] === 'client' && $this->user->_hasRole('developer') )
	        {
	            return false;
	        }

	        if( $user['role_name'] === 'client' && $this->user->_hasRole('client') && $user['id'] !== $this->user->id )
	        {
	            return false;
	        }

			return true;
		});
	}

	public function saveChannel(ChannelRequest $request, $_channel = null)
	{
		if( $_channel !== null )
		{
			if( $_channel->isOwner($this->user) )
			{
				$channel = $this->save( $request->all(), $_channel, false);

				if( $channel->has('users') )
				{
					Res::broadCastDynamic('channel:edited', $this->prepareSockerParam($channel->users), $channel);
				}

				return Res::success();
			}

			return Res::fail();
		}

		$channel = new Channel;

		$channel->unique = str_random(10) . $this->company->id;

		$channel->user_id = $this->user->id;

		$channel->company_id = $this->company->id;

		$channel = $this->save($request->all(), $channel, false);

		$users = Role::ensureFromThisCompany( $request->get('users') );

		$users = is_array($users) ? $users : [];

		array_push($users, $this->user->id);

		$channel->assignUsers( $users );
		
	    $users = $channel->users;

	    foreach($users as $user)
		{
			$socketData['users'][] = $user->generateSecret();
		}

		$socketData['channel'] = $channel->loadUserIds();

		event(new Authenticate($socketData, 'channel:assign-user'));

		// return $socketData;

		return Res::success($channel);
	}

	public function prepareSockerParam($users){
  		
  		$_user = [];

  		foreach($users as $user)
		{
			$_user[] = $user->generateSecret();
		}

		return $_user;
	}

	private function notifyActiveChannelUsers($channel)
	{
		$channel = $channel->fresh();

		$to = $channel->users->map(function($u){ return $u->generateSecret(); });

		Res::broadCastDynamic('channel:changed-user', $to, $channel->loadUserIds() );
	}

	public function changeUserChannel(Channel $channel, User $user)
	{
		if( $channel->company_id === $this->company->id && $this->user->isAdmin() )
		{
			if( $channel->belongs($user) )
			{
				$channel->revokeUsers( [ $user->id ] );

				event(new Authenticate(['user' => $user->generateSecret(),'channel' => $channel->loadUserIds() ],'channel:revoke-user'));
			}
			else
			{
				$channel->assignUsers([ $user->id ]);

				event(new Authenticate(['users' => [$user->generateSecret()],'channel' => $channel->loadUserIds() ],'channel:assign-user'));
			}

			$this->notifyActiveChannelUsers($channel);

			return Res::success();
		}

		return Res::fail();
	}

	public function clearAttachedResources( $user )
	{
		$this->company->clearUserResources($user);
	}

	public function markNotificationRead(Notification $notification)
	{
		if( $notification->read === 0 && $notification->company_id === $this->company->id && $notification->user_id === $this->user->id)
		{
			$notification->read = 1;

			$notification->save();

			Res::broadCastDynamic('api:on-notification-read', $this->user->generateSecret(), $notification->id);
		}

		return Res::success($notification);
	}
}