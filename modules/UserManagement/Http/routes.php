<?php

Route::group([
	'middleware' => ['api','web','auth'], 
	'prefix' => 'api/v1',
	'namespace' => 'Modules\UserManagement\Http\Controllers'], 
	function()
	{
		Route::post('me','UserManagementCtrl@saveUserDetails');

		Route::get('role/{roles}/users', 'UserManagementCtrl@userByRoles');

		Route::post('redux/dispatch','ReduxDispatcherCtrl@handleRequest')->middleware('validate.nonce');

		Route::post('notification/{notification}/mark-read','UserManagementCtrl@markNotificationRead');

		Route::group([ 'middleware' => ['role:admin'],'prefix' => 'admin' ], function(){

			Route::post('channel/{channel}/user/{user}','UserManagementCtrl@changeUserChannel')->middleware('user.belongs_company');

			Route::post('channel/{channel?}','UserManagementCtrl@saveChannel');

			Route::get('roles','UserManagementCtrl@getRoles');

			Route::post('invite/users', 'UserManagementCtrl@inviteNewUser');
			
			Route::delete('remove/user/{user}','UserManagementCtrl@removeUser')->middleware('user.belongs_company');
			
			Route::post('update/role/{role}/user/{user}','UserManagementCtrl@updateRole')
				->middleware('user.belongs_company');

			Route::post('search/user', 'UserManagementCtrl@searchUsers');
		});

		Route::post('update/profile', 'UserManagementCtrl@saveUserDetails');
		Route::post('change/password','UserManagementCtrl@changePassword');
	});

Route::group([
	'middleware' => ['api','web'],
	'namespace' => 'Modules\UserManagement\Http\Controllers'
], function(){
	Route::get('invitation/{invitation}/token/{token}','UserManagementCtrl@acceptInvitation');
	Route::post('invitation/accepting','UserManagementCtrl@acceptingInvitation');
	Route::post('username/unique','UserManagementCtrl@isUniqueUsername');

	Route::post('company', 'UserManagementCtrl@registerCompany');
	Route::get('activate/company/{token}', 'UserManagementCtrl@activateCompany');
});


Route::bind('user',function($userId)
{
	if($user = App\User::find($userId))
	{
		return $user;
	}
	else
	{
		throw new \Illuminate\Database\Eloquent\ModelNotFoundException;
	}
});

Route::bind('invitation', function($inviation)
{
	if( $inviation = Modules\UserManagement\Entities\Invitation::find($inviation) )
	{
		return $inviation;
	}

	else
	{
		throw new \Illuminate\Database\Eloquent\ModelNotFoundException;
	}
});

Route::bind('roles', function($roleValue)
{
	$roleIdentifer = is_numeric($roleValue) ? 'id' : 'name';

	if( $roleValue === 'all' ) return 'all';

	$role = \Modules\UserManagement\Entities\Role::where($roleIdentifer, $roleValue)->first();

	if( $role )
	{
		return $role;
	}

	throw new \Illuminate\Database\Eloquent\ModelNotFoundException;
});

Route::bind('role', function($roleValue)
{
	$roleIdentifer = is_numeric($roleValue) ? 'id' : 'name';

	$role = \Modules\UserManagement\Entities\Role::where($roleIdentifer, $roleValue)->first();

	if( $role )
	{
		return $role;
	}

	throw new \Illuminate\Database\Eloquent\ModelNotFoundException;
});

Route::bind('notification',function($notificationId){

	if($notification = App\Notification::find($notificationId))
	{
		return $notification;
	}
	else
	{
		throw new \Illuminate\Database\Eloquent\ModelNotFoundException;
	}
});