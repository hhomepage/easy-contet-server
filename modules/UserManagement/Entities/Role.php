<?php namespace Modules\UserManagement\Entities;

use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole
{
	public static function ensureFromThisCompany($users){
		return self::from('role_user')->whereIn('user_id', $users)->where('company_id', app('company')->id)->lists('user_id')->toArray();
	}

	public static function findByName($name)
	{
		return self::whereName($name)->first();
	}
}