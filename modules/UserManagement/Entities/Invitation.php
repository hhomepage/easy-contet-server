<?php namespace Modules\UserManagement\Entities;
   
use Illuminate\Database\Eloquent\Model;
use Res;

class Invitation extends Model {

	protected $table = 'invitaions';

	protected $fillable = ['email'];

	public function inviter(){
		return $this->belongsTo('App\User','inviter_id', 'id');
	}

	public function user(){
		return $this->belongsTo('App\User','user_id','id');
	}

	public function company(){
		return $this->belongsTo('App\Company','company_id','id');	
	}

	public function projects()
	{
		return $this->belongsToMany('Modules\Project\Entities\Project','project_invitations','invitation_id','project_id');
	}

	public function assignIfProjects($user)
	{
		$projects = $this->projects;

		if( !$projects->isEmpty() )
		{
			$projects->map(function($project)use($user)
			{
				$project->assign($user);
				
				Res::broadCastDynamic('api:project-edited', $project->getMemberSecrets(), ['payload' => $project, 'key' => 'project-user:assigned']);
			});
		}
	}
}