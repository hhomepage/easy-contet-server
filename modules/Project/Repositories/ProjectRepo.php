<?php namespace Modules\Project\Repositories;
/**
 * Created by PhpStorm.
 * User: sishir
 * Date: 2/28/16
 * Time: 1:05 AM
 */
use Modules\Project\Entities\Project;
use Modules\Threads\Entities\Thread;
use App\Resource;
use Modules\UserManagement\Entities\Role;
class ProjectRepo
{

    /*
     * project
     * page
     * project
     * field
     * field-group
     *
     * */

    // so now repitetive finding

    protected $model;

    public function setModel($project){
        return $this->model = $project;
    }

    //based on the user roles the projects will be loaded
    

    public function getAll($company, $user){

       // return $
       

        return $projects;
    }

    public function getFresh($project)
    {
        $project->users = $project->userIds->lists('id');
        return $project;
    }

    public function filterUser($userIds)
    {
        $allRoles = Role::all();
        
        $users = Role::from('role_user')->whereIn('user_id', $userIds)->where('company_id', app('company')->id );

        if( \Auth::user()->isAdmin() ) return $userIds;

        foreach($users as $key => $user)
        {
            $role = $allRoles->where('id', $user->role_id);
            
            if( $role->name == 'client' && \Auth::user()->_hasRole('developer') )
            {
                unset($userIds[ $key ]);
            }

            if( $role->name == 'developer' && \Auth::user()->_hasRole('client') )
            {
                unset($userIds[$key]);
            }
        }

        return $userIds;
    }

    public function find( $project, $user, $threadId = null ) {

        $_boards = $project->boards()
            ->orderBy('project_boards.order','asc')
            ->get()->toArray();
        
        $boards = [];

        foreach( $_boards as $board )
        {
            $boards[ $board['id'] ] = $board;
        }

        $_final = [];

        $threads = Thread::where('project_id', $project->id)->whereIn('board_id', array_keys($boards) )->with(['checklists.checkitems','resources','labelIds','comments' => function($q){
        
            $q->orderBy('comments.created_at', 'desc');
        
        },'comments.resources'])->orderBy('threads.order','asc')->get();

        foreach($boards as $id => $board)
        {
            $_threads = $threads->where('board_id', $id);

            $_threads = array_values($_threads->toArray());

            $_threads = array_map(function($t){
            
                $t['labels'] = array_column($t['label_ids'],'label_id');
            
                unset($t['label_ids']);
                
                $resource = array_filter($t['resources'], function($r){

                    if( $r['meta']['cover'] )
                    {
                        return true;
                    }
                });

                if( count( $resource ) !== 0 )
                {
                    $resource = array_values($resource);

                    $resModel = new Resource;
        
                    $t['card_cover'] = $resModel->makeBase64($resource[0]['path']);

                    $t['cover_id'] = $resource[0]['id'];
                }

                if( isset( $t['checklists'] ) and is_array( $t['checklists'] ) )
                {
                    $t['checklists'] = array_map(function($c){

                        if( isset($c['checkitems'] ) ){
                            $totalChecked = array_column( $c['checkitems'],'checked' );
                            $c['totalChecked'] = array_sum($totalChecked);
                        }


                        return $c;

                    }, $t['checklists']);
                }

                return $t;
            
            }, $_threads);


            $board['threads'] = $_threads;

            $_final[] = $board;
        }

        return $_final;
    }

    public function determinCardCover($thread){
    }
}