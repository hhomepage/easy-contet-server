<?php
/**
 * Created by PhpStorm.
 * User: sishir
 * Date: 2/29/16
 * Time: 11:42 AM
 */

namespace Modules\Project\Http\Controllers;

use Res;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use Pingpong\Modules\Routing\Controller;

use Modules\Project\Entities\Project;
use Modules\Project\Entities\Feedback;
use Modules\Project\Entities\ScrumBoard;
use Modules\Project\Entities\Field;

use Modules\Threads\Entities\Thread;
use Modules\Threads\Entities\CheckList;
use Modules\Threads\Entities\CheckListItem;
use Modules\Project\Http\Requests\ThreadOrderRequest; 
use Modules\Project\Repositories\ProjectRepo;

use App\Traits\ModelCRUD;

use Modules\Project\Http\Requests\ProjectRequest;
use Modules\Project\Http\Requests\FeedbackRequest;
use Modules\Project\Http\Requests\SaveThreadRequest;
use Modules\Threads\Http\Requests\CheckListRequest;
use Modules\Threads\Http\Requests\CheckListItemRequest;
use Modules\Threads\Http\Requests\CommentRequest;
use Modules\Project\Http\Requests\UserIdsRequest;
use Modules\Project\Http\Requests\ScurmBoardRequest;
use Modules\Project\Http\Requests\AssignResoruceCommentRequest;
use Modules\Project\Http\Requests\ToogleThreadStatusRequest;
use Modules\Project\Http\Requests\UpdateOrderRequest;
use Modules\Project\Http\Requests\TaskBetweenBoardRequest;
use Modules\Project\Http\Requests\AssignDueDatesRequest;


use App\Http\Requests\LabelRequest;

use App\User;
use App\Comment;
use App\Label;
use App\Resource;
use App\Events\Authenticate;
use App\Activity;
use App\Notification;


class ProjectCtrl extends Controller
{

	use ModelCRUD;

    protected $user;
    protected $project;
    protected $company;

    public function __construct(Project $project,Guard $auth)
    {
        $this->company = app('company');
        $this->user = $auth->user();
        $this->setModel( $project );
        $this->project = $project;
    }

    public function disatchEvent($data, $event)
    {
        event(new Authenticate( $data , $event ) );
    }

    public function getTo($project)
    {
        return $project->users->map(function($user){
            return $user->generateSecret();
        });
    }
    
    /**
     * Creating a new project
     */

    public function createProject(ProjectRequest $request, Project $project = null)
    {
        $project->company_id = $this->company->id;

        $project->user_id = $this->user->id;

        $project->status = 1;

        $project->fill($request->all());

        $project->description = $this->stripDangerousTags($project->description);

        $users = [];


        if( $request->get('user_ids') )
        {
            $_users = $request->get('user_ids');

            if( in_array($this->user->id, $_users ) )
            {
                $key = array_search($this->user->id, $_users);
                unset($_users[$key]);
            }

            if( count($_users) !== 0 )
            {
                $users = app('company')->areFromSame( $_users ,'users.id')->lists('id')->toArray();
            }
        }

        //features to handle user assign sending, 

        $whatArray = [];
        $_emails = [];

        if( $request->get('users') )
        {
            $emailUsers = $request->get('users');

            $emails = array_column($emailUsers, 'email');

            $_emails = User::checkIfAlreadyRegistered($emails);

            foreach($emailUsers as $key => $em)
            {
                $emails[ $em['email'] ] = $em;
            }

            $_users = isset( $_emails['users'] ) ? $_emails['users'] : [];

            $users = array_merge($users,$_users);

            $_emails = $_emails['email'];
        }

        if( $request->has('copyFrom') && $request->has('copyWhat') && count($request->get('copyWhat')) !== -1)
        {
            $whatArray['values'] = $request->get('copyWhat');

            $whatArray['id'] = Project::find($request->get('copyFrom'));
        }

        $users = array_unique(array_merge( [ $this->user->id ] , $users ));

        $project->save();

        $project->assign( $users );

        //todo remaining
        // $project->copyProject( $whatArray );


        if( count($_emails) !== 0 )
        {
            foreach($_emails as $key => $em)
            {
                if( app('company')->alreadyInvited( $em ) )
                {
                    unset( $_emails[ $key ] );
                }
            }
        }

        Res::broadCastDynamic('api:project-created', $this->getTo($project), $project);

        return Res::success(['id' => $project->id,'emails' => $_emails]);
    }

    public function updateProject( ProjectRequest $request, Project $project )
    {
        $project->fill($request->all());

        $project->description = $this->stripDangerousTags($project->description);

        $project->save();

        Res::broadCastDynamic('api:project-edited', $this->getTo($project), $project);

        return Res::success();
    }

    public function assingUser( Project $project, User $user ){

        if( $project->belongs($user) || $user->id === $this->user->id )
        {
            return Res::fail(['assigned'=>false],'Already assinged');
        }

        if( $project->assign( $user ) )
        {
            $project = $project->fresh()->load('users');

            Res::broadCastDynamic('api:project-edited', $this->getTo($project), ['payload' => $project, 'key' => 'project-user:assigned']);

            return Res::success([]);
        }
    }

    public function revokeUsers( Project $project, User $user ){
        
        if( $project->belongs($user) )
        {   
            $to = $this->getTo($project);

            if( ($user->id !== $this->user->id) && $project->revoke( $user ) )
            {
                $project = $project->fresh()->load('users');

                Res::broadCastDynamic('api:project-edited', $to, ['payload' => $project, 'key' => 'project-user:revoked']);

                return Res::success($to);
            }
        
        }

        return Res::fail(['Not Assigned User'], 400);
    }

    public function createBoard(ScurmBoardRequest $request , Project $project, ScrumBoard $board = null){

        ScrumBoard::adjustOrders();

        $board->project_id = $project->id;

        $board->user_id = $this->user->id;

        $board->fill($request->all());

        $board->save();

        Res::broadCastDynamic('api:board-created', $project->getMemberSecrets( ['admin','developer'] ), $board->fresh() );

        return $board;
    }

    public function updateBoard(ScurmBoardRequest $request, Project $project, ScrumBoard $board)
    {
        if( $project->id === $board->project_id )
        {
            $board->fill($request->all());

            $board->save();

            Res::broadCastDynamic('api:board-edited', $project->getMemberSecrets( ['admin','developer'] ), $board->fresh() );
        }

        return Res::success();
    }








    public function restoreBoard( Project $project, ScrumBoard $board )
    {
        if( $project->id === $board->project_id )
        {
            $board->status = 1;
            
            return $this->save([], $board);
        }

        return Res::fail([],'Unable to handle the request');
    }

    public function removeBoard(Project $project, ScrumBoard $board)
    {
        if( $project->id === $board->project_id )
        {
            return $this->trash( $board );
        }

        return Res::fail([],'Unable to handle the request');
    }

    private function validateThreadRequest($project, $board, $thread, $force = false){

        if( $thread->exists && ( $thread->project_id !== $project->id || $thread->board_id !== $board->id || $board->project_id !== $project->id ) )
        {
            false;
        }

        return true;
    }

    public function createThread(SaveThreadRequest $request, Project $project, ScrumBoard $board, Thread $thread = null)
    {
        Thread::adjustOrders();
        
        $thread->status = $thread->exists ? $thread->status_n : 1;
        
        $thread->user_id = $this->user->id;
        
        $thread->project_id = $project->id;
        
        $thread->board_id = $board->id;
        
        $thread->due_date = date('Y-m-d H:i:s', strtotime('+ 2 days'));

        $thread->start_date = date('Y-m-d H:i:s', time());

        $thread->fill( $request->all() );
        
        $thread->description = $this->stripDangerousTags($thread->description);

        $thread->save();

        Res::broadCastDynamic('api:thread-created', $this->getProjectUserSecrets($project), $thread );

        return Res::success(['id' => $thread->id,'board_id' => $board->id, 'project_id' => $project->id]);
    }

    public function updateThread(SaveThreadRequest $request, Project $project, ScrumBoard $board, Thread $thread)
    {
        if(  $this->validateThreadRequest( $project, $board, $thread )  )
        {
            $thread->fill($request->all());

            $thread->description = $this->stripDangerousTags($thread->description);

            $changed = $thread->getDirty();

            $thread->save();

            Res::broadCastDynamic('api:thread-edited', $this->getProjectUserSecrets($project), $thread );

            Activity::makeFromDirty($project, $thread, $changed);

            return Res::success( $changed );      
        }

        return Res::fail([]);
    }



















    public function getProjectUserSecrets($project){
        return $project->getMemberSecrets( ['admin','developer'] );
    }

    public function removeThread(Project $project, ScrumBoard $board, Thread $thread)
    {
        if( !$thread->inTrash() && $this->validateThreadRequest($project, $board, $thread) )
        {
            return $this->trash( $thread );
        }

        return Res::fail([]);
    }


    public function changeThreadStatus(ToogleThreadStatusRequest $request, Project $project, ScrumBoard $board, Thread $thread){
        
        if( !$thread->status_n !== $request->get('status') && $this->validateThreadRequest($project, $board, $thread) )
        {
            $thread->status = $request->get('status');
            return $this->save([], $thread );
        }

        return Res::fail();
    }

    private function isValidThreadRequest(Project $project, Thread $thread){
        return $project->id === $thread->project_id;
    }

    public function assingThreadUser(Project $project, Thread $thread, User $user){
        
        if( !$thread->isAssigned($user) )
        {
            if( $thread->syncUsers( [ $user->id ], $this->user->id ) )
            {
                return Res::success( Thread::find( $thread->id )->users->lists('id') );
            }
        }

        return Res::fail(['Already Assigned User'], 400);
    }

    public function removeThreadUser(Project $project, Thread $thread, User $user){
        
        if( $thread->isAssigned($user) )
        {
            if( $thread->revokeUsers( [ $user->id ] ) )
            {
                return Res::success(['revoked' => true,'Success']);
            }
        }

        return Res::fail(['Not Assigned User'], 400);
    }

    public function saveCheckList(CheckListRequest $request, Project $project, Thread $thread, CheckList $checklist = null)
    {
        $checklist = ($checklist->exists) ? $checklist : new CheckList;

        $checklist->user_id = $this->user->id;
        
        $checklist->project_id = $project->id;
        
        $checklist->thread_id = $thread->id;

        return $this->save($request->all(), $checklist);
    }

    public function removeCheckList(Project $project, Thread $thread, CheckList $checklist){

        
        if( $thread->id === $checklist->thread_id )
        {
            return $this->delete($checklist);
        }

        return Res::fail();
    }

    public function saveCheckItem(CheckListItemRequest $request, Project $project, Thread $thread, CheckList $checklist, CheckListItem $checkItem = null)
    {
        if( $checklist->thread_id === $thread->id)
        {
            $checkItem = $checkItem->exists ? $checkItem : new CheckListItem;

            $checkItem->check_list_id = $checklist->id;

            return $this->save( $request->all('text'), $checkItem );
        }

        return Res::fail([]);
    }

    public function toogleCheckItem(Project $project, Thread $thread, CheckList $checklist, CheckListItem $checkItem)
    {
        $checkItem->checked = ( $checkItem->checked == 1 ) ? 0 : 1;
        
        if( ( !$checklist->only_me ) || $checklist->user_id == $this->user->id  )
        {
            $checkItem =  $this->save( [], $checkItem , false);

            if( $checkItem )
            {
                return Res::success(['checked' => $checkItem->checked, 'totalChecked' => $checklist->countCheckedItems() ] );
            }
        }

        return Res::fail();
    }

    public function removeCheckItem(Project $project, Thread $thread, CheckList $checklist, CheckListItem $checkItem ){

        if( $checkItem->check_list_id === $checklist->id && $checklist->thread_id === $thread->id )
        {
            return $this->delete($checkItem);
        }

        return Res::fail([]);
    }


    public function saveResource(AssignResoruceCommentRequest $request, Project $project, Thread $thread){
    
        if( $thread->resources()->saveMany(Resource::find($request->get('resource_ids' ) ) ) )
        {
           return Res::success($thread->resources()->get());
        }

        return Res::fail([],'The resource is already used');
    }

    private function clearAllCovers(Thread $thread, $resource ){
        
        if( $thread->has('resources') )
        {
            return Resource::whereIn( 'id', $thread->getResourceIds() )->update([
                'meta' => json_encode(['cover' => 0])
            ]);
        }       
    }

    public function toogleResourceCover(Project $project, Thread $thread, Resource $resource)
    {
        if( $resource->isImage() && $this->clearAllCovers($thread, $resource ) )
        {
            $resource->meta = json_encode([
                'cover' => $resource->meta['cover'] == 1 ? 0 : 1 
            ]);

           if( $resource->save() )
           {
               return Res::success(['log'=> \DB::getQueryLog(), 'all' => $thread->resources()->get(), 'image' => $resource->makeBase64() ]);
           }

        }

        return Res::fail();
    }

    public function removeResource(Project $project, Thread $thread, Resource $resource)
    {
        return $this->delete($resource);
    }


    public function saveComment(CommentRequest $request, Comment $comment = null)
    {
        $comment = $comment && $comment->exists ? $comment : new Comment;

        $comment->user_id = $this->user->id;
        
        $comment->text = $this->stripDangerousTags($request->get('text'));

        $comment = $this->save([], $comment, false);

        if( $request->has('resource_ids') )
        {
            $comment->resources()->saveMany( Resource::find($request->get('resource_ids') ) );
        }

        return Res::success($comment);

    }

    public function saveLabel(LabelRequest $request, Label $label = null)
    {
        if( $label && $label->exists && $label->company_id !== $this->company->id ) return Res::fail();
        $label = $label && $label->exists ? $label : new Label;
        $label->user_id = $this->user->id;
        $label->company_id = $this->company->id;
        return $this->save($request->all(), $label);
    }

    public function deleteLabel( Label $label = null)
    {
        if( $label->company_id === $this->company->id ){

            if( $label->threads->isEmpty() )
            {
               return $this->delete($label);
            }

        }

        return Res::fail();
    }

    public function deleteComment(Comment $comment = null)
    {
       return $this->delete($comment);
    }

    public function assignLabel(Project $project, Thread $thread, Label $label ){
        
        if($label->company_id === $this->company->id)
        {

           if( !$thread->hasLabel( $label ) )
           {
               $label->threads()->attach( [ $thread->id] );
           }
          
           return Res::success( $thread->labelIds()->get()->lists('label_id') );
        }

       return Res::fail();
    }

    public function revokeLabel( Project $project, Thread $thread, Label $label ){
        
        if(($label->company_id === $this->company->id) && $thread->hasLabel( $label ) )
        {
           if( $label->threads()->detach( [$thread->id] ) )
           {
                return Res::success();
           }
        }
       return Res::fail();
    }

    private function stripDangerousTags($string){
        $tags = array( 'script', 'iframe');
        $string = preg_replace('/<\/?(' . implode('|', $tags) . ')(.|\s)*?>/', '', $string );
        return $string;
    }

    private function checkIfForMentions($comment, $thread)
    {
        $ifMentions = preg_match_all('/@\[\<strong\stitle="([\d]+)">/', $comment->text, $matches);

        if( isset($matches[1] ) )
        {
            $comment->text = preg_replace('/title="[\d]+">/', '>', $comment->text);

            $comment->save();

            $userIds = array_unique($matches[1]);

            $users =  app('company')->canReceiveNotification($userIds);

            $to = [];

            if( !$users->isEmpty() )
            {                
                $users = User::find($users->lists('user_id')->toArray());

                foreach( $users as $user )
                {
                    if( $user->id !== $this->user->id )
                    {
                        $notification = Notification::createForMention($thread, $comment, $user);

                        $notification->thread = $thread;

                        $notification->comment = $comment;

                        $to[ $user->generateSecret() ] = $notification->toArray();
                    }
                }
                
                if( count($to) !== 0 )
                {
                    Res::broadCastDynamic( 'api:on-notification', $to, []);
                }
            }
        }
    }

    public function addCommentOnThreads(Project $project, Thread $thread, Comment $comment)
    {
        if( $thread->comments()->save($comment) )
        {
            $this->checkIfForMentions($comment, $thread);

            return Res::success($comment->load('resources'));
        }

        return Res::fail();
    }

    public function addCommentsOnFormField(Project $project, Field $field, Comment $comment){

        if( $field->comments()->save( $comment ) )
        {
            return Res::success($comment);
        }

        return Res::fail();
    }

    public function addCommentsOnFeedback(Project $project, Feedback $feedback, Comment $comment){

        if( $this->validFeedbackRequest($project, $feedback) && $feedback->comments()->save( $comment ) )
        {
            return Res::success($comment);
        }

        return Res::fail();

    }

    private function validFeedbackRequest($project, $feedback){
        return $project->id === $feedback->project_id && $feedback->company_id === $this->company->id;
    }

    public function addFeedback(FeedbackRequest $request, Project $project, Feedback $feedback = null){

        if( $feedback->exists && !$this->validFeedbackRequest($project, $feedback) )
        {
            return Res::fail();
        }

        $feedback = $feedback->exists ? $feedback : new Feedback;
        $feedback->company_id = $this->company->id;
        $feedback->user_id = $this->user->id;
        $feedback->project_id = $project->id;

        return $this->save($request->all(), $feedback );
    }

    public function deleteFeedback(Project $project, Feedback $feedback){

          if( $this->validFeedbackRequest($project, $feedback) )
          {
            return $this->delete($feedback);
          }

          return Res::fail();
    }

    private function makeRepo(){
        $repo = new ProjectRepo;

        $repo->setModel(new Project);
        
        return $repo;
    } 

    public function getAll()
    {
        $all = $this->user->projects()->where('projects.company_id', $this->company->id)->get();

        return Res::success($all);
    }

    public function getOne(Project $project)
    {
        $boards = $this->makeRepo()->find($project, $this->user);

        return Res::success($boards);
    }

    public function updateBoardOrders(UpdateOrderRequest $request, Project $project)
    {
        return Res::success( ScrumBoard::updateBoardOrder( $request->boards ) );
    }

    public function updateThreadOrders(ThreadOrderRequest $request, Project $project, ScrumBoard $board)
    {
        return Res::success( Thread::updateOrder( $request->threads, $project, $board ) );
    }   

    public function isValidThread(Project $project, Thread $thread = null)
    {
        if( $thread->exists )
        {
            if( $thread->project_id === $project->id )
            {
                return Res::success($thread);
            }
            else
            {
                return Res::fail([]);
            }
        }

        return Res::success();
    }

    public function getStatics(Project $project){

    }

    public function toogleTaskBetweenBoard(ThreadOrderRequest $request, Project $project, ScrumBoard $board, Thread $thread)
    {
        if( $project->id === $board->project_id && $thread->project_id === $project->id)
        {

            //to-do, check for the error condition

            if( $thread->board_id !== $board->id )
            {
                $oldBoard = $thread->board_id;

                $thread->board_id = $board->id;
                
                $thread->save();

                if( $request->has('threads') )
                {
                    Thread::updateOrder( $request->threads, $project, $board );
                }

                Res::broadCastDynamic('api:toogle-task-boards',$project->getMemberSecrets( ['admin','developer'] ),[
                    'thread' => $thread->id,
                    'oldBoard' => $oldBoard,
                    'newBoard' => $board->id,
                    'project' => $project->id,
                    'orders' => $request->has('threads') ? $request->threads : false
                ]);

                return Res::success();
            }
        }
    }

    public function assignDueDates(AssignDueDatesRequest $request, Project $project ){
        
        $threads = $request->get('threads');
        
        $success = Thread::where('project_id', $project->id)->whereStatus(1);

        $success = count($threads) === 1 ? $success->where('id', $threads[0]) : $success->whereIn('id', $threads); 

        // echo $success->toSql();

        $success = $success->update([
            'due_date' => $request->get('due_date')
        ]);

        if( $success )
        {
            $threads = Thread::whereIn('id', $threads)->get()->toArray();

            $payload = [];

            if( count($threads) !== 0 )
            {
                $payload['project_id'] = $project->id;
                $payload['threads'] = $threads;
            }

            $to = $project->assignedUsers->map(function($user)
            {
                return $user->generateSecret();
            });

            $to[] = $this->user->generateSecret();

            $this->handleDynamicEvents('thread:dueDateChanged', $to , $payload);
            
            return Res::success();
        }

        return Res::fail();
    }

    public function handleDynamicEvents($event, $to, $payload)
    {
        event(new Authenticate( ['_event' => 'thread:dueDateChanged', 'to' => $to,'payload' => $payload], 'redis:dynamic-event' ) );
    }
}