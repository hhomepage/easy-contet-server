<?php
/**
 * Created by PhpStorm.
 * User: sishir
 * Date: 2/29/16
 * Time: 11:42 AM
 */

namespace Modules\Project\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use Modules\Project\Entities\Project;
use Modules\Project\Http\Requests\ProjectRequest;
use Modules\Project\Http\Requests\UserIdsRequest;
use Pingpong\Modules\Routing\Controller;
use App\Traits\CrudTrait;
use App\User;
use Res;

class ProjectController extends Controller
{
    use CrudTrait;

    protected $user;

    public function __construct(Project $project,Guard $auth)
    {
        $this->setInstance($project);
        $this->user = $auth->user();
    }

    public function update( ProjectRequest $request, $model )
    {
        $this->setInstance( $model );
        return Res::success($this->save( $request->all() ));
    }

    public function create( ProjectRequest $request )
    {
        $project = $this->getInstance();

        $project->user_id = $this->user->id;

        $this->setInstance( $project );

        return Res::success($this->save($request->all()));
    }

    public function getByUser(User $user)
    {
        return Res::success($user->projects);
    }

    public function find(Project $project)
    {
        return Res::success($project->load('pages'));
    }

    public function createFromExisting(Project $project)
    {
        return Res::success($project->duplicate());
    }

    public function all()
    {
        $projects = $this->model->with('assignedUsers')->latest()->get();

        return Res::success( $projects );
    }

    public function loadQuestions(){

    }

    public function loadScrumboard(Project $project){
        return Res::success( $project->loadBoards() );
    }

    public function loadFileManager(){

    }

    public function loadContact(){

    }

    public function loadSetting(Project $project ){

        /**
         * Need To check user roles to:do
         */

        $assignedUsers = $project->load('assignedUsers.roles')->toArray()['assigned_users'];
        
        $responseData = $this->groupAssignedUsers($assignedUsers);

        return Res::success($responseData);
    }

    public function loadFeedback(){

    }


    public function groupAssignedUsers( $assignedUsers = [] ){

        $responseData = ['assignedUsers' => []];

        foreach ($assignedUsers as $user) 
        {
            if( is_array( $user['roles'] ) && count( $user['roles'] ) !== 0 )
            {
                $responseData['assignedUsers'][ $user['roles'][0]['name'] ][] = $user;
                unset($user['roles']);
            }

        }

        return $responseData;
    }

    /**
     * Assign Project TO user
     * Here we need to send proper 
     * data for redux store
     */

    public function assign(Request $request, Project $project){

        $userIds = $request->get('users');

        $users = $this->toogleAssignProject($userIds, $project);

        $responseData = $this->groupAssignedUsers($users);

        $responseData['users'] = $users;

        return Res::success($responseData);
    }

    public function revokeProject(Project $project, User $user)
    {
        $project->revoke($user);

        $project->load('assignedUsers.roles');

        $users = $project->assignedUsers->toArray();

        $responseData = $this->groupAssignedUsers($users);

        $responseData['users'] = $users;

        return Res::success($responseData);
    }

    public function toogleAssignProject( $userIds, Project $project)
    {
        $users = User::whereIn('id',$userIds)->get();

        $users = $users->filter( function($user){
            return $user->hasRole( [ 'client','developer' ] );
        });

        $project->assing($users->lists('id')->toArray() );

        $project->load('assignedUsers.roles');

        return $project->assignedUsers->toArray();
    }

}
