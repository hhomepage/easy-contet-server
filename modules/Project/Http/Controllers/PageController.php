<?php
/**
 * Created by PhpStorm.
 * User: sishir
 * Date: 2/29/16
 * Time: 11:42 AM
 */

namespace Modules\Project\Http\Controllers;
use App\Http\Requests\Request;
use Illuminate\Contracts\Auth\Guard;
use Modules\Project\Entities\Page;
use Modules\Project\Entities\Project;
use Modules\Project\Http\Requests\ProjectRequest;
use Modules\Project\Http\Requests\FormFiledSaveRequest;
use Pingpong\Modules\Routing\Controller;
use App\Traits\ModelCRUD;
use Res;

class PageController extends Controller
{
    use ModelCRUD;

    public function __construct(Page $page)
    {
        $this->setInstance($page);
    }

    public function savePage( ProjectRequest $request, Project $project, Page $page = null)
    {
        if( $page->exists && $project->id !== $page->project_id )
        {
          return Res::fail([]);
        }

        $page->project_id = $project->id;

        $response = $this->save($request->all(), $page);

        return $response;
    }

    public function remove(Project $project, Page $page )
    {
        if( $project->id === $page->project_id )
        {
            $project->status = 0;
            return $this->save([], $project);
        }

        return Res::fail([]);
    }

    public function saveFields(FormFiledSaveRequest $request, Project $project, Page $page )
    {
        if( $project->id === $page->project_id )
        {
            $project->status = 0;
            return $this->save([], $project);
        }

        return Res::fail([]);
    }
}