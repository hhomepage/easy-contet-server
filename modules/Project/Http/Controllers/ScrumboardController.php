<?php namespace Modules\Project\Http\Controllers;

use Modules\Project\Http\Requests\UpdateOrderRequest;
use Pingpong\Modules\Routing\Controller;
use Modules\Project\Http\Requests\ScurmBoardRequest;
use Modules\Project\Entities\Project;
use Modules\Project\Entities\ScrumBoard;
use Res;

class ScrumboardController extends Controller {
	
	public function index()
	{
		return view('project::index');
	}

	/*
		Returns the board Instance
		To:DO Need to check if the board is of the project
	*/

	public function save(ScurmBoardRequest $request, Project $project , ScrumBoard $board)
	{
		if( $board->updateBoard( $request->all() ) ){
			return Res::success( $board );
		}
		return Res::fail();
	}

	public function create(ScurmBoardRequest $request, Project $project){
		$board = new ScrumBoard;
		$board->project_id = $project->id;
		$board->fill( $request->all() );
		$board->save();
		return Res::success( $board->load('threads') );
	}

	/**
	 * Update order of multiple board
	 */

	public function updateOrder(UpdateOrderRequest $request, Project $project)
	{
		return Res::success( ScrumBoard::updateBoardOrder( $request->boards ) );
	}

	/*
		Soft Deleting
	*/
		
	public function toogleTrashing()
	{

	}
}