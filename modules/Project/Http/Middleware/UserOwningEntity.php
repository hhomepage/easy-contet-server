<?php namespace Modules\Project\Http\Middleware; 

use Closure;
use Auth;
use Modules\Project\Entities\Project;

class UserOwningEntity {

    protected $user;

    public function __construct(){
        $this->user = Auth::user();
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $model)
    {
        $strict = false;

        if( preg_match('/^@/', $model) )
        {
            $model = substr($model, 1, strlen( $model ) );
            
            $strict = true;
        }

        if( $request->route( $model ) )
        {
            $entity = $request->route($model);

            if( $strict === false && $this->user->isAdmin() )
            {
                return $next($request);
            }

            if( $entity->user_id !== $this->user->id )
            {
                return \App::abort(403);
            }
        }

    	return $next($request);
    }
    
}
