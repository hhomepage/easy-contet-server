<?php namespace Modules\Project\Http\Middleware; 

use Closure;
use Session;
use Res;

class AddNonceToken {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
    	$response = $next($request);

        if( $response->getStatusCode() === 200 )
        {
            $token['token'] = str_random(10);

            $token['url'] = $request->url();

            $nonce = encrypt( $token['token'] . '=|=' . $token['url'] ); 

            Session::put('redux_nonce', json_encode( $token ) );

            if( method_exists($response, 'getContent') )
            {
                $content = json_decode($response->getContent(), true);

                $content['nonce_token'] = $nonce;

                return response()->json($content, $response->getStatusCode());
            }
        }

        return $response;
    }
    
}
