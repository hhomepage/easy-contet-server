<?php namespace Modules\Project\Http\Middleware; 

use Closure;
use Auth;

class ThreadOnProject {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if( $request->route('project') && $request->route('thread') )
        {
            $project = $request->route('project');
            $thread = $request->route('thread');

            if( $project->id !== $thread->project_id )
            {
                return \App::abort(403);
            }
        }

       return $next($request);
    }
    
}
