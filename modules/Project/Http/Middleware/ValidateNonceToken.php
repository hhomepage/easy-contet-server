<?php namespace Modules\Project\Http\Middleware; 

use Closure;
use Res;
use Session;

class ValidateNonceToken {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $reduxData = $request->session()->pull('redux_nonce');

        $reduxData = json_decode( $reduxData, true );

        $nonce = $request->get('nonce');

        if( $nonce !== '' )
        {
            try{
                $userRedux = decrypt( $nonce );
            }
            catch( \Expection $e ) {
                return Res::fail([]);
            }

            list($token, $url) = explode('=|=', $userRedux ); 

            if( $token === $reduxData['token'] && $url === $reduxData['url'] )
            {
                return $next($request);
            }
        }

        return Res::fail(['nonce invalid']);
    }
    
}
