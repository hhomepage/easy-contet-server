<?php namespace Modules\Project\Http\Middleware; 

use Closure;

use Auth;
use Res;

class ProjectBelongsUser {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $model = null)
    {
        if( Auth::user()->id !== $request->route('project')->user_id ){
            return Res::fail([''],'This project doesn\'t belongs to you' );
        }

    	return $next($request);
    }
    
}
