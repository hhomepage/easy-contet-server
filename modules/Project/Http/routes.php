<?php

Route::group(['middleware' => ['web','auth','api','user.belongs_company:auth'], 'prefix' => 'api/v1', 'namespace' => 'Modules\Project\Http\Controllers'], function()
{
	Route::group(['middleware' => ['company.project'], 'prefix' => 'project'], function(){

		Route::group(['middleware'=>['role:admin','redux.nonce'],'prefix' => 'admin'], function(){
			//project
			Route::post('/','ProjectCtrl@createProject');
			Route::post('/{project}','ProjectCtrl@updateProject');
						
			Route::delete('/{project}/forever','ProjectCtrl@removeProject')->middleware('user.project');


			Route::group(['middleware' => ['user.belongs_company']], function(){
	
				//assigning to project
				Route::post('{project}/assign/user/{user}','ProjectCtrl@assingUser');
				Route::delete('{project}/revoke/user/{user}','ProjectCtrl@revokeUsers');
			});

		});


		//for develoer permission checking is needed
			Route::group(['prefix' => 'admin'], function(){

				Route::post('{project}/board/{scrumboard}/thread/{thread}/toogle-task','ProjectCtrl@toogleTaskBetweenBoard')
					->middleware('my.entity:scrumboard')
					->middleware(['role:admin|developer','thread.project','user.belongs_project:auth'])
					->middleware('my.entity:thread');
			});
		//future work to:do

		Route::group(['middleware'=>['role:admin|developer','thread.project','user.belongs_project:auth','redux.nonce'],'prefix' => 'admin'], function(){


			Route::post('{project}/board','ProjectCtrl@createBoard')->middleware('my.entity:scrumboard');

			Route::post('{project}/board/{scrumboard}','ProjectCtrl@updateBoard')->middleware('my.entity:scrumboard');
			
			Route::delete('{project}/board/{scrumboard}','ProjectCtrl@removeBoard')->middleware('my.entity:scrumboard');
			










			Route::post('{project}/update-due/threads', 'ProjectCtrl@assignDueDates');
			
			Route::post('{project}/board/{scrumboard}/restore','ProjectCtrl@restoreBoard')->middleware('my.entity:scrumboard');
			
			Route::delete('/{project}/board/{scrumboard}/forever','ProjectCtrl@deleteBoardPermanently')->middleware('my.entity:scrumboard');
			
			Route::post('/{project}/board/{scrumboard}/update-order/threads','ProjectCtrl@updateThreadOrders')->middleware('my.entity:scrumboard');
			
			Route::post('/{project}/update-order/boards','ProjectCtrl@updateBoardOrders')->middleware('my.entity:scrumboard');


			Route::group(['middleware' => ['thread.project']], function(){
				//thread
				Route::post('{project}/board/{scrumboard}/thread','ProjectCtrl@createThread');

				Route::post('{project}/board/{scrumboard}/thread/{thread}','ProjectCtrl@updateThread')->middleware('my.entity:thread');
				
				// Route::delete('{project}/board/{scrumboard}/thread/{thread}','ProjectCtrl@removeThread')->middleware('my.entity:thread');
				
				// Route::post('{project}/board/{scrumboard}/thread/{thread}/toogle','ProjectCtrl@changeThreadStatus')->middleware('my.entity:thread');
				// Route::delete('/{project}/board/{scrumboard}/thread/{thread}/forever','ProjectCtrl@deleteThreadPermanently')->middleware('my.entity:thread');
			});


			Route::group(['middleware' => ['user.belongs_company']], function(){
				Route::group( [ 'middleware' => ['user.belongs_project'] ], function(){
					Route::post('{project}/thread/{thread}/assign/user/{user}','ProjectCtrl@assingThreadUser');
					Route::delete('{project}/thread/{thread}/revoke/user/{user}','ProjectCtrl@removeThreadUser');
				});

			});
		});

		//client 
		Route::group(['middleware' => ['role:client','user.belongs_project:auth']], function(){
			
			Route::post('{project}/page/{page}/fill/questiorare','PageController@saveForm');

			Route::post('{project}/feedback/{feedback?}','PageController@saveFeedback');
			
			Route::delete('{project}/feedback/{feedback}','PageController@removeFeedback');

		});

		Route::group(['middleware' => ['role:client|admin','user.belongs_project:auth']], function(){

			Route::post('{project}/page/{page}/field/{field}/comment','PageController@addCommentOnField');

			Route::get('{project}/feedback','ProjectCtrl@getFeedback');
			
			Route::post('{project}/feedback/{feedback}/comment','PageController@addCommentOnFeedback');

		});


		Route::group(['middleware' => ['role:developer|admin','thread.project','user.belongs_project:auth','redux.nonce']], function(){

			Route::post('{project}/thread/{thread}/assing/resource','ProjectCtrl@saveResource');
			
			Route::get('{project}/thread/is-valid/{thread?}','ProjectCtrl@isValidThread');

			Route::post('{project}/thread/{thread}/resource/{resource}/make-cover','ProjectCtrl@toogleResourceCover')->middleware('morph.belongs:thread|resource');

			Route::delete('{project}/thread/{thread}/resource/{resource}/remove-cover','ProjectCtrl@toogleResourceCover')->middleware('morph.belongs:thread|resource');

			Route::post('{project}/thread/{thread}/checklist/{checklist?}','ProjectCtrl@saveCheckList')->middleware('my.entity:checklist');
			
			Route::delete('{project}/thread/{thread}/checklist/{checklist}','ProjectCtrl@removeCheckList')->middleware('my.entity:checklist');

			Route::post('{project}/thread/{thread}/checklist/{checklist}/checkitem/{checkitem?}','ProjectCtrl@saveCheckItem')->middleware('my.entity:checklist');

			Route::post('{project}/thread/{thread}/checklist/{checklist}/checkitem/{checkitem}/toogle','ProjectCtrl@toogleCheckItem');

			Route::delete('{project}/thread/{thread}/checklist/{checklist}/checkitem/{checkitem}','ProjectCtrl@removeCheckItem')->middleware('my.entity:checklist');

			Route::post('{project}/thread/{thread}/comment/{comment}','ProjectCtrl@addCommentOnThreads')->middleware('morph.belongs:thread|comment|empty')->middleware('my.entity:comment');

			Route::post('{project}/thread/{thread}/label/{label}/assign','ProjectCtrl@assignLabel');

			Route::delete('{project}/thread/{thread}/label/{label}/revoke','ProjectCtrl@revokeLabel');

			Route::post('label/{label?}','ProjectCtrl@saveLabel')->middleware('my.entity:label');
			Route::delete('label/{label}','ProjectCtrl@deleteLabel')->middleware('my.entity:label');

		});
		
		Route::get('all', 'ProjectCtrl@getAll');

		Route::get('{project}/load','ProjectCtrl@getOne');

	});


	Route::post('comment','ProjectCtrl@saveComment');


	Route::group(['middleware' => ['my.entity:@comment','redux.nonce']], function(){
		Route::post('comment/{comment?}','ProjectCtrl@saveComment');
		Route::post('comment/{comment}/assign/resource', 'ProjectCtrl@assignResourceToComment');
		Route::delete('comment/{comment}','ProjectCtrl@deleteComment');
	});
});

//model binding
Route::bind('page',function($pageId){

	if($page = Modules\Project\Entities\Page::find($pageId))
	{
		return $page;
	}
	else
	{
		throw new \Illuminate\Database\Eloquent\ModelNotFoundException;
	}
});

Route::bind('scrumboard',function($boardId){
	if($board = Modules\Project\Entities\ScrumBoard::find($boardId))
	{	
		return $board;
	}
	else
	{
		throw new \Illuminate\Database\Eloquent\ModelNotFoundException;
	}
});

Route::bind('field_group',function($fieldGroupId){

	if($fieldGroup = Modules\Project\Entities\FieldGroup::find($fieldGroupId))
	{
		return $fieldGroup;
	}
	else
	{
		throw new \Illuminate\Database\Eloquent\ModelNotFoundException;
	}
});

Route::bind('field',function($fieldId){

	if($field = Modules\Project\Entities\FieldValue::find($fieldId))
	{
		return $field;
	}
	else
	{
		throw new \Illuminate\Database\Eloquent\ModelNotFoundException;
	}
});


Route::bind('project',function($projectId){

	if($project = Modules\Project\Entities\Project::find($projectId))
	{
		return $project;
	}
	else
	{
		throw new \Illuminate\Database\Eloquent\ModelNotFoundException;
	}
});

