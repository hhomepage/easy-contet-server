<?php namespace Modules\Project\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ToogleThreadStatusRequest extends FormRequest {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'status' => 'required|in:' . implode(',', [0,1,2,3] ) ,
		];
	}

}
