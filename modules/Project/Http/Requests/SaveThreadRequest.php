<?php namespace Modules\Project\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SaveThreadRequest extends FormRequest {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'title' => 'required_creation:thread|min:2',
			'due_date' => 'date',
			'start_date' => 'date'
		];
	}

}
