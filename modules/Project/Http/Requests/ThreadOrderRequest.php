<?php namespace Modules\Project\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;
use Validator;
use Modules\Threads\Entities\Thread;

class ThreadOrderRequest extends FormRequest {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		Validator::extend('board_request', function( $attr, $value )
		{
			if( is_array( $value ) )
			{
				$board = $this->route('scrumboard');

				if( $board->project_id === $this->route('project')->id ){

					return $board->threads()->whereIn('threads.id', array_column($value,'id') )->count();
				}
			}

			return false;
		});

		return [
			'threads' => 'board_request'
		];
	}

}
