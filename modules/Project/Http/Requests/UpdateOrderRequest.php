<?php namespace Modules\Project\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

use Auth;
use Validator;
use Modules\Project\Entities\ScrumBoard;

class UpdateOrderRequest extends FormRequest {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return Auth::user()->hasRole(['admin']);
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		Validator::extend('boradids', function($attr, $value, $parameter, $validator){
				

			if( is_array( $value ) ) {
				
				$ids = array_column($value, 'id');

				if( count( $ids ) !== 0 ) {
					
					return ScrumBoard::areValidBoards( $ids, $this->route('project')->id );
				}
			}

			return false;
		});

		return [
			'boards'	=>	'boradids' 
		];
	}

}
