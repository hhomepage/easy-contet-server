<?php namespace Modules\Project\Http\Requests;
use Modules\Project\Repositories\RepositoryFactory;
use Illuminate\Foundation\Http\FormRequest;

class ProjectRequest extends FormRequest {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */


	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'title' => 'required_creation:project|min:5',
			'description' => 'min:5',
			'user_ids' => 'array',
			'users.*.email' => 'email',
			'users.*.role.name' => 'in:admin,client,developer', //need to be dynamic
			'copyFrom' => 'exists:projects,id,company_id,' . app('company')->id,
			'copyWhat' => 'array'
		];
	}

}
