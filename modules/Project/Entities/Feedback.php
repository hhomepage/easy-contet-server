<?php namespace Modules\Project\Entities;
   
use Illuminate\Database\Eloquent\Model;

class Feedback extends Model {

    protected $fillable = ['text','status'];

    protected $table = 'feedbacks';

    public function company(){
    	return $this->belongsTo('App\Company','company_id','id');
    }

    public function company(){
    	return $this->belongsTo('App\User','user_id','id');
    }
}