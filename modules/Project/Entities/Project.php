<?php namespace Modules\Project\Entities;
   
use Illuminate\Database\Eloquent\Model;
use App\User;
use Storage;

class Project extends Model {

    protected $fillable = ['title','description','status','fav'];

    protected $table = "projects";

    protected $appends = ['user_ids'];



    public function pages()
    {
        return $this->hasMany('Modules\Project\Entities\Page','project_id','id');
    }

    public function setSlugAttribute($value) {
        return $this->attributes['slug'] = str_slug($value);
    }

    public function assignedUsers()
    {
        return $this->belongsToMany('App\User','projects_users','project_id','user_id')->withTimeStamps();
    }

    public function assign($user)
    {
        $user = is_array($user) ? $user : [$user->id];
        return $this->assignedUsers()->syncWithoutDetaching($user);
    }

    public function users(){
        return $this->belongsToMany('App\User','projects_users','project_id')->withTimeStamps();
    }

    public function getUserIdsAttribute(){
        $ids = $this->users->lists('id')->toArray();
        unset($this->users);
        return $ids;
    }

    public function belongs(User $user)
    {
       return in_array($user->id,$this->users->lists('id')->toArray());
    }

    public function revoke(User $user){
        $query = $this->newQueryWithoutScopes()->from('projects_users');
        return $query->where('project_id',$this->id)->where('user_id',$user->id)->delete();
    }

    public function duplicate()
    {
        $duplicate = new Project();

        $duplicate->fill($this->toArray());

        $duplicate->save();

        $pages = $this->pages;

        if( !$pages->isEmpty() )
        {
            $this->pages->each(function($page) use( $duplicate )
            {
                $newPage = new Page();

                $newPage->fill($page->toArray());

                $newPage->project_id = $duplicate->id;

                $newPage->save();

                $entities = $page->getEntities();

                if( !$entities->isEmpty() )
                {
                    $entities->each(function($entity) use( $newPage )
                    {
                       if( isset( $entity->is_group ) )
                       {
                            $fieldGroup = new FieldGroup();
                            $fieldGroup->fill($entity->getAttributes());
                            $fieldGroup->page_id = $newPage->id;
                            $fieldGroup->save();

                            if( !$entity->fields->isEmpty() )
                            {
                                $entity->fields->each(function($field) use($newPage,$fieldGroup)
                                {
                                    $this->saveFieldValue($field, null, $fieldGroup->id);
                                });
                            }
                       }
                       else
                       {
                            $this->saveFieldValue($entity, $newPage->id);
                       }

                    });
                }

            });
        }

        return $duplicate;
    }

    public function saveFieldValue($field , $pageId = null, $groupId = null)
    {
        $newField = new FieldValue();
        $newField->fill($field->toArray());
        $newField->value = "";
        $newField->page_id = $pageId;
        $newField->group_id = $groupId;
        $newField->save();
    }

    public function boards(){
        return $this->hasMany('Modules\Project\Entities\ScrumBoard','project_id','id');
    }

    public function loadBoards(){
        return $this
            ->boards()
            ->orderBy('project_boards.order')
            ->with([
                'threads' => function($query){
                    return $query->orderBy('threads.order');
                },
                'threads.comments','threads.checklist.items','threads.users','threads.labels'])
            ->get();
    }

    public function company(){
        return $this->belongsTo('App\Company','company_id','id');
    }

    public function save(array $options = [])
    {
        $this->slug = $this->title;
        return parent::save($options);
    }

    public function getMemberSecrets($roles = [])
    {
        if( count($roles) === 0 )
        {
            $to = $this->assignedUsers->map(function($u)
            {
                return $u->generateSecret();
            });
        }
        else
        {
            $to = $this->assignedUsers->filter(function($u)use( $roles )
            {
                return $u->_hasRole( $roles );
            });

            $to = $to->map(function($u)
            {
                return $u->generateSecret();
            });
        }

        return array_unique($to->toArray());
    }

    /**
     * Save for inviation 
     */
    public static function waitForInvitation($ids , $inviation)
    {
        $inviation->load('projects');

        $projects = self::where('id', $ids)->where('company_id', app('company')->id)->get();
        
        if( !$projects->isEmpty() )
        {
            $insertArray = [];

            $projectIds = $projects->lists('id')->toArray();

            foreach($projectIds as $id)
            {
                $insertArray[] = ['project_id' => $id, 'invitation_id' => $inviation->id];
            }

            if( !in_array($id,  $inviation->projects->lists('id')->toArray() ) )
            {
                self::from('project_invitations')->insert($insertArray);
            }

            return $projects;
        }

    }
}