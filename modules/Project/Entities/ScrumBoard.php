<?php namespace Modules\Project\Entities;
   
use Illuminate\Database\Eloquent\Model;
use DB;

class ScrumBoard extends Model {
    protected $table = 'project_boards';
    
    protected $fillable = ['name','status','order'];

    protected $visisble = ['status'];

 	public function threads(){
        return $this->hasMany('Modules\Threads\Entities\Thread','board_id','id');
    }

    public static function areValidBoards( $ids = array(), $projectId){
    	$ids = is_array($ids) ? $ids : [$ids];

    	return self::whereIn( 'id', $ids )->where('project_id', $projectId)->count();
    }

    public static function adjustOrders()
    {
        DB::statement('UPDATE project_boards SET `order` = `order` + 1 WHERE `order` >= 0 ORDER BY `order` asc;');
    }

    public static function updateBoardOrder($boards){
    
        $queryStr = 'UPDATE project_boards SET `order` = (CASE id';

        foreach( $boards as $board ){
            $queryStr.=' WHEN ' . $board['id'] . ' THEN ' . $board['order'];
        }

        $queryStr = $queryStr . " END ) WHERE id IN (" . implode(',', array_column( $boards, 'id' ) ) . ')';

        return DB::statement($queryStr);
    }

    public function project()
    {
        return $this->belongsTo('Modules\Project\Entities\Project','project_id','id');
    }

    public function updateBoard($inputs){
        $this->fill($inputs);
        return $this->save();
    }
}