<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddChecklistTables extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('check_lists', function(Blueprint $table)
        {
            $table->increments('id');

            $table->integer('user_id')->unsigned();

            $table->integer('project_id')->unsigned();

            $table->integer('thread_id')->unsigned();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->foreign('project_id')->references('id')->on('projects')->onDelete('cascade');

            $table->foreign('thread_id')->references('id')->on('threads')->onDelete('cascade');

            $table->boolean('only_me')->default(0);

            $table->string('name');

            $table->timestamps();
        });

        Schema::create('check_list_item', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('check_list_id')->unsigned();
            $table->foreign('check_list_id')->references('id')->on('check_lists')->onDelete('cascade');
            $table->integer('checked_by')->unsigned()->nullable();
            $table->foreign('checked_by')->references('id')->on('users')->onDelete('cascade');
            $table->string('text');
            $table->boolean('checked')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('', function(Blueprint $table)
        {

        });
    }

}
