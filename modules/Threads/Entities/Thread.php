<?php namespace Modules\Threads\Entities;
   
use App\User;
use Illuminate\Database\Eloquent\Model;
use Modules\Project\Entities\BelongsContract;
use DB;
class Thread extends Model implements BelongsContract{

    protected $table = 'threads';
    

    protected $fillable = ['type','description','title','due_date','start_date','status'];

    const ACTIVE_THREAD = '1';
    const PENDING_THREAD = '2';
    const COMPLETED_THREAD = '3';
    const IS_TRASHED = 0;

    static public $status = [
        self::ACTIVE_THREAD => "Active",
        self::PENDING_THREAD => "Pending",
        self::COMPLETED_THREAD => "Completed",
        self::IS_TRASHED => "In Trash"
    ];


    protected $appends = ['status_n', 'user_ids'];

    public static function adjustOrders()
    {
        DB::statement('UPDATE threads SET `order` = `order` + 1 WHERE `order` >= 0 ORDER BY `order` asc;');
    }

    public function getStatusAttribute($value)
    {
        return isset( self::$status[$value] ) ? self::$status[$value] : 'Active';
    }

    public function getStatusNAttribute()
    {
        return isset($this->attributes['status']) ? $this->attributes['status'] : 1;
    }

    public function project()
    {
        return $this->belongsTo('Modules\Project\Entities\Project','project_id','id');
    }

    public function user()
    {
        return $this->belongsTo('App\User','user_id','id');
    }

    public function comments()
    {
        return $this->morphMany('App\Comment','commentable');
    }

    public function checklists()
    {
        return $this->hasMany('Modules\Threads\Entities\CheckList');
    }

    public function users()
    {
        return $this->belongsToMany('App\User','assigned_threads','thread_id','assigned_to')->withTimestamps();
    }

    public function getUserIdsAttribute(){
        $ids = $this->users->lists('id')->toArray();
        unset($this->users);
        return $ids;
    }

    public function resources()
    {
        return $this->morphMany('App\Resource','imagable');
    }

    public function getResourceIds(){
        return $this->resources->lists('id')->toArray();
    }

    public function belongs(User $user)
    {
        if( $this->project->belongs($user) )
        {
            return true;
        }
    }

    public function isAssigned(User $user)
    {
        $userIds = $this->users->lists('id')->toArray();

        return in_array($user->id , $userIds);
    }

    public static function updateOrder($threads, $project, $board)
    {
        $queryStr = 'UPDATE threads SET `order` = (CASE id';

        foreach( $threads as $thread ){

            if( is_integer( $thread['order'] ) ){
                $queryStr.=' WHEN ' . $thread['id'] . ' THEN ' . $thread['order'];
            }
        }

        $queryStr = $queryStr . " END ) WHERE id IN (" . implode(',', array_column( $threads, 'id' ) ) . ') and project_id = :projectId and board_id = :boardId';

        return DB::statement($queryStr,['projectId' => $project->id, 'boardId' => $board->id]);

        return $status;
    }

    public function labels(){
        return $this->morphToMany('App\Label','taggable','morph_labels');
    }

    public function hasLabel($label){
        $labels = $this->labels;
        return $labels->isEmpty( ) ? false : in_array($label->id, $labels->lists('id')->toArray());
    }

    public function labelIds(){
        return $this->labels()->select('morph_labels.label_id');
    }

    public function syncUsers( $users , $userId)
    {
        $prepare = [];

        foreach($users as $user)
        {
            $prepare[$user] = ['assigned_by' => $userId];
        }

        return $this->users()->syncWithoutDetaching( $prepare );
    }

    public function revokeUsers( $users )
    {
        return $this->users()->detach( $users );
    }

    public function syncLabels($labels)
    {
        return $this->labels()->syncWithoutDetaching( $labels );
    }

    public function revokeLabel( $users )
    {
        return $this->users()->detach( $users );
    }

    public function syncResources( $label )
    {
        return $this->resouces()->syncWithoutDetaching( $users );
    }

    public function inTrash(){

        return $this->status_n == self::IS_TRASHED;
    }

    public function revokeResource($resource){
        return true;
    }
}