<?php namespace Modules\Threads\Entities;
   
use Illuminate\Database\Eloquent\Model;

class CheckList extends Model {

	protected $table = 'check_lists';

    protected $fillable = ['name','only_me'];

    public function checkitems()
    {
    	return $this->hasMany('Modules\Threads\Entities\CheckListItem');
    }

    public function project()
    {
    	return $this->belongsTo('Modules\Project\Entities\Project','project_id','id');
    }

    public function countCheckedItems(){
        return array_sum($this->checkitems()->get()->lists('checked')->toArray());
    }
}