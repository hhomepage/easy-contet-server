<?php namespace Modules\Threads\Entities;
   
use Illuminate\Database\Eloquent\Model;

class CheckListItem extends Model {

    protected $fillable = ['text'];

    protected $casts = [
	    'checked' => 'boolean',
	];

    protected $table = 'check_list_item';

    public function checkList(){
    	return $this->belongsTo('Modules\Threads\Entities\CheckList');
    }

} 