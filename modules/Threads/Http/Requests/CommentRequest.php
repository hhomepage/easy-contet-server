<?php namespace Modules\Threads\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Threads\Policy\CommentControl;
use Modules\Threads\Policy\CommentCreatePolicy;
use Modules\Threads\Policy\ThreadPolicyFactory as PolicyFactory;
use Modules\Threads\Policy\ThreadControl;
use Validator;
use App\Resource;

class CommentRequest extends FormRequest {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{

		Validator::extend('resource_ids', function($attr, $value, $parameter, $validator){

        	if( is_array( $value ) )
        	{
        		$resources = Resource::whereIn('id',$value)->get();
        		
        		foreach($resources as $resource)
        		{
        			if( $resource->isAssigned() || $resource->user_id != \Auth::user()->id)
        			{
						return false;        				
        			}
        		}

        		return true;
        	}

        });


		return [
			'text' => 'required',
			'resource_ids' => 'resource_ids'
		];
	}

}
