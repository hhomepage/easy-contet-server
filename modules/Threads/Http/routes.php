<?php

Route::group(['middleware' => ['api','web','auth'], 'prefix' => 'api', 'namespace' => 'Modules\Threads\Http\Controllers'], function()
{
		/**
		 * This route can be called from admin, client, and manager
		 * Admin user can access all the routes
		 */

		/**
		 * Client can create threads on assigned project
		 * Manager can create threads easily on his assigned project
		 * Developer needs permission to create threads
		 */

		Route::post('project/{project}/thread','ThreadsController@create');

		/**
		 * Client can edit his threads
		 * Manager can edit all the threads in his assigned project
		 * Developer can edit his threads
		 * He can also edit other developers threads based on the permission
		 */

		Route::post('project/{project}/thread/{thread}','ThreadsController@edit');

		/**
		 * Can only be deleted by project mangaer and client and admin
		 * Client can be delete his threads,
		 * but manager can delete all the threads
		 * which is created by developers and other project manager
		 */

		Route::delete('project/{project}/thread/{thread}','ThreadsController@delete');

		/**
		 * This route can be all type user
		 * Manger can see all the threads
		 * Developer can see only his assigned threads
		 * Client Can see his threads and his assigned threads
		 */
		Route::get('project/{project}/thread','ThreadsController@get');

		Route::get('project/{project}/thread/{thread}','ThreadsController@find');

		/**
		 * A manager can comment in all threads
		 * A client can post in this assigned threads, and his own threads
		 */

		Route::post('project/{project}/thread/{thread}/comment','CommentController@create');

		/**
		 * A manager can edit other comments
		 * A client can edit his comments only
		 * A developer can also edit hist comments only
		 * No no, he can aslo edit other commetns if he has permission
		 */

		Route::post('project/{project}/thread/{thread}/comment/{comment}','CommentController@edit');

		/**
		 * A manager can delete comments of developers
		 * A developer which has permsission to delete other comments he can delete them
		 * A own created comments can be deleted by the all user
		 */

		Route::delete('project/{project}/thread/{thread}/comment/{comment}','CommentController@delete');

		Route::get('project/{project}/thread/{thread}/assign/{user}',['middleware'=>'thread.view','uses'=>'ThreadsController@assign']);

		Route::get('project/{project}/thread/{thread}/revoke/{user}',['middleware'=>'thread.view','uses'=>'ThreadsController@revoke']);

			Route::group(['middleware'=>['role:admin']], function()
			{
				Route::group(['prefix' => 'project'],function()
				{
					Route::post('{project}/board/{board}/update-order','ThreadsController@updateThreadsOrder');

					Route::post('{project}/board/{board}/threads', 'ThreadsController@createNew');

					Route::post('{project}/board/{board}/threads/{thread}','ThreadsController@update');

					Route::post('{project}/board/{board}/thread/{thread}/check-list','ThreadsController@addCheckList');

					Route::post('{project}/board/{board}/thread/{thread}/check-list/{checklist}','ThreadsController@updateCheckList');

					Route::delete('{project}/board/{board}/thread/{thread}/check-list/{checklist}','ThreadsController@deleteCheckList');
										
					Route::post('{project}/check-list/{checklist}/check-item','ThreadsController@addCheckListItem');

					Route::post('{project}/check-list/{checklist}/check-item/{checkitem}','ThreadsController@updateCheckListItem');
					
					Route::delete('{project}/check-list/{checklist}/check-item/{checkitem}','ThreadsController@deleteCheckListItem');

					//comment

					Route::group(['prefix'=>'{project}/board/{board}/thread/{thread}/comments'], function(){
						Route::post('/', 'CommentController@createComment');
							Route::delete('{comment}','CommentController@deleteComment');
							Route::post('{comment}','CommentController@editComment');
					});

					Route::group(['prefix'=>'{project}/board/{board}/thread/{thread}'], function(){
						
						Route::group(['prefix' => 'assign'], function(){
							Route::post('users','ThreadsController@assignUsers');
							Route::post('labels','ThreadsController@assignLabels');
							Route::post('resources','ThreadsController@assignResources');
						});

						Route::group(['prefix' => 'revoke'], function(){
							Route::post('users','ThreadsController@revokeUsers');
							Route::post('labels','ThreadsController@revokeLabels');
							Route::post('resources','ThreadsController@revokeResources');
						});

					});
				});
			});
});


Route::bind('thread',function($threadId){

	if($thread = Modules\Threads\Entities\Thread::find($threadId))
	{
		return $thread;
	}
	else
	{
		throw new \Illuminate\Database\Eloquent\ModelNotFoundException;
	}
});

Route::bind('checklist',function($checkListId){

	if($checkList = Modules\Threads\Entities\CheckList::find($checkListId))
	{
		return $checkList;
	}
	else
	{
		throw new \Illuminate\Database\Eloquent\ModelNotFoundException;
	}
});

Route::bind('checkitem',function($checkItemId){

	if($checkItem = Modules\Threads\Entities\CheckListItem::find($checkItemId))
	{
		return $checkItem;
	}
	else
	{
		throw new \Illuminate\Database\Eloquent\ModelNotFoundException;
	}
});