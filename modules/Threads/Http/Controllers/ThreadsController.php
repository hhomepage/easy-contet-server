<?php namespace Modules\Threads\Http\Controllers;

use App\User;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Modules\Project\Entities\Project;
use Modules\Project\Entities\ScrumBoard;
use Modules\Threads\Entities\Thread;
use Modules\Threads\Http\Requests\ThreadRequest;
use Modules\Threads\Http\Requests\ThreadSaveRequest;
use Modules\Threads\Policy\ThreadControl;
use Modules\Threads\Policy\ThreadViewPolicy;
use Modules\Threads\Repositories\ThreadRepo;
use Pingpong\Modules\Routing\Controller;
use Modules\Threads\Entities\CheckList;
use Modules\Threads\Entities\CheckListItem;
use Modules\Project\Http\Requests\ThreadOrderRequest;
use Res;
use Modules\Threads\Policy\ThreadPolicyFactory as PolicyFactory;

class ThreadsController extends Controller {

	protected $user;

	protected $repo;

	protected $policy;

	public function __construct(Guard $auth,ThreadRepo $repo)
	{
		$this->user = $auth->user();
		$this->repo = $repo;
	}

	public function create(ThreadRequest $request,Project $project)
	{

		$this->repo->setProject($project);

		return Res::success($this->repo->save($request->all()));

	}

	public function edit(ThreadRequest $request, Project $project, Thread $thread)
	{
		return Res::success($this->repo->edit($request->all(),$thread));
	}

	public function delete(Request $request, Project $project, Thread $thread)
	{
		$status = PolicyFactory::make($request,new ThreadControl());

		if($status)
		{
			return Res::success($this->repo->delete($thread));
		}

		return Res::fail("Not Authorized");
	}

	public function get(Request $request, Project $project)
	{
		if( PolicyFactory::make($request) )
		{
			$this->repo->setUser($this->user);

			return Res::success($this->repo->get($project));
		}

		return Res::fail("Not Authorized");
	}

	public function find(Request $request, Project $project, Thread $thread)
	{
		$threadPolicy = new ThreadViewPolicy();

		$threadPolicy->setThread($thread);

		if(PolicyFactory::make($request,$threadPolicy  ) )
		{
			$thread = $this->repo->find($thread)->toArray();

			unset($thread['assigned_to']);

			return Res::success($thread);
		}

		return Res::fail('Not Authorized');
	}

	public function assign(Request $request, Project $project, Thread $thread, User $user)
	{
		if( $project->belongs($user) )
		{
			$status = '';

			if( is_null( $thread->assignedTo()->where('assigned_to',$user->id)->first() ) )
			{
				$status = $thread->assignedTo()->attach( $user, ['assigned_by' => $this->user->id ]);
			}

			return Res::success($status,'Success Fully Assigned');
		}

		return Res::fail("","Project Does Not Belongs To User",FAIL);
	}

	public function revoke(Request $request, Project $project, Thread $thread, User $user)
	{
		if( $project->belongs($user) )
		{
			$status = $thread->assignedTo()->detach( $user->id );

			return Res::success($status,'Successfully Revoked');
		}

		return Res::fail("","Project Does Not Belongs To User",FAIL);
	}

	public function updateThreadsOrder( ThreadOrderRequest $request, Project $project, ScrumBoard $board )
	{
		return Res::success( Thread::updateOrder( $request->threads, $project, $board ) );
	}

	/**
	 * ToDO, need to implement the eloquent and elastic search repo 
	 * and use that function
	 */
	public function createNew( ThreadSaveRequest $request, Project $project, ScrumBoard $board ){
		
		$thread = new Thread;
		
		$thread->project_id = $project->id;
		
		$thread->board_id = $board->id;
		
		$thread->fill($request->all());

		$thread->checklist = [];

		$thread->comments = [];
		
		$thread->save();

		return Res::success( $thread );
	}

	public function update( ThreadSaveRequest $request, Project $project, ScrumBoard $board, Thread $thread ){
		
		$thread->fill($request->all());

		$thread->save();

		return Res::success($thread);
	}

	public function addCheckList( Request $request, Project $project, ScrumBoard $board, Thread $thread ) {
		
		$checkList = new CheckList;
		$checkList->project_id = $project->id;
		$checkList->thread_id = $thread->id;
		$checkList->fill($request->all());
		$checkList->user_id =  \Auth::user()->id;
		$checkList->save();
		$checkList->items = [];
		return Res::success($checkList);
	}

	public function updateCheckList(  Request $request, Project $project, ScrumBoard $board, Thread $thread, CheckList $checkList ){
		$checkList->fill($request->all());
		$checkList->save();
		return Res::success($checkList);
	}
	
	public function deleteCheckList( Project $project, ScrumBoard $board, Thread $thread, CheckList $checkList ){
		return Res::success($checkList->delete());
	}

	public function addCheckListItem( Request $request, Project $project, CheckList $checkList ){
		$checkItem = new CheckListItem;
		$checkItem->fill($request->all());
		$checkItem->check_list_id = $checkList->id;
		$checkItem->save();
		return Res::success($checkItem);
	}
	
	public function updateCheckListItem(Request $request, Project $project, CheckList $checkList, CheckListItem $checkItem){
		$checkItem->fill($request->all());
		$checkItem->checked_by = \Auth::user()->id;
		$checkItem->save();
		return Res::success($checkItem);
	}

	public function deleteCheckListItem( Request $request, Project $project, CheckList $checkList, CheckListItem $checkItem )
	{
		$checkItem->delete();
		return Res::success();
	}

	public function assignUsers(Request $request, Project $project, ScrumBoard $board, Thread $thread)
	{
		$users = is_array($request->get('inputs')) ? $request->get('inputs') : [$request->get('inputs')];

		$users = $thread->syncUsers( $users, $this->user->id );

		return Res::success( $users['attached'] );
	}

	public function revokeUsers(Request $request, Project $project, ScrumBoard $board, Thread $thread)
	{
		$users = $thread->revokeUsers( $request->get('inputs') );

		return Res::success($users);
	}
}