<?php
/**
 * Created by PhpStorm.
 * User: sishir
 * Date: 3/7/16
 * Time: 11:48 PM
 */

namespace Modules\Threads\Http\Controllers;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Modules\Project\Entities\Project;
use App\Comment; 
use Modules\Threads\Entities\Thread;
use Pingpong\Modules\Routing\Controller;
use Res;
use Modules\Project\Entities\ScrumBoard;
use Modules\Threads\Repositories\CommentRepo;
use Modules\Threads\Http\Requests\CommentRequest;

class CommentController extends Controller
{
    protected $comment;
    protected $repo;

    protected $user;

    public function __construct(Comment $comment,Guard $guard)
    {
        $this->comment = $comment;
        $this->repo = new CommentRepo();
        $this->repo->setUser($guard->user());
        $this->user = $guard->user();
    }

    public function create(CommentRequest $request, Project $project, Thread $thread)
    {
        return Res::success($this->repo->save($request->all() , $thread));
    }

    public function edit(CommentRequest $request, Project $project, Thread $thread, Comment $comment)
    {
        return Res::success($this->repo->edit($request->all(),$comment,$thread));
    }

    public function delete(Request $request, Project $project, Thread $thread, Comment $comment)
    {
        return Res::success($this->repo->delete($comment));
    }

    public function createComment(Request $request, Project $project, ScrumBoard $board, Thread $thread)
    {
        $comment = new Comment;

        $comment->fill($request->all());

        $comment->user_id = $this->user->id;

        $thread->comments()->save($comment);

        return Res::success($comment);
    }

    public function editComment(Request $request, Project $project, ScrumBoard $board, Thread $thread, Comment $comment)
    {
        $comment->fill($request->all());

        $comment->save();

        return Res::success($comment);
    }

    public function deleteComment( Request $request, Project $project, ScrumBoard $board, Thread $thread, Comment $comment ){
        $comment->delete();

        return Res::success();
    }
}