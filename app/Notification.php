<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
 	protected $table = 'notifications';

 	const MENTION = 1;
 	const THREAD_COMMENT = 2;
    const ASSIGNED_THREAD = 3;
    const ASSIGNED_PROJECT = 4;

    static public $status = [
        self::MENTION => "has mentioned you in the comment",
        self::THREAD_COMMENT => "has commented on your task",
        self::ASSIGNED_THREAD => "has assigned you the task",
        self::ASSIGNED_PROJECT => "has assigned you the project"
    ];

    protected $appends = ['text'];

    public function getTextAttribute(){
    	return self::$status[ $this->type ];
    }
   
    public function thread()
    {
        return $this->belongsTo('Modules\Threads\Entities\Thread','thread_id','id');
    }

    public function comment()
    {
        return $this->belongsTo('App\Comment','comment_id','id');
    }

    public static function createForMention($thread, $comment, $receiver)
    {
    	$mention = new static();

    	$mention->type = '1';

    	$mention->project_id = $thread->project_id;

    	$mention->company_id = app('company')->id;

    	$mention->thread_id = $thread->id;

        $mention->read = 0;

    	$mention->comment_id = $comment->id;

    	$mention->user_id = $receiver->id;

    	$mention->creator_id = \Auth::user()->id;

    	$mention->save();

    	return $mention;
    }
}
