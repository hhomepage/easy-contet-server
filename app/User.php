<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use JWTAuth;
use App\Chat;
use Modules\UserManagement\Entities\Role;
use Storage;

class User extends Authenticatable
{
    use EntrustUserTrait;

    protected $fillable = [
        'name', 'email', 'password','username','info','gender','tel','address','about','birthday','avatar'
    ];


    protected $appends = [
        'avatar_name'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    protected $hidden = [
        'password', 'remember_token','unique_token'
    ];

    public function getAvatarNameAttribute(){
        return isset($this->attributes['avatar']) ? $this->attributes['avatar'] : null;
    }

    public function getAvatarAttribute($avatar)
    {

        if( $avatar )
        {
            return asset('uploads/' . $this->avatar_name);
        }

        $gender = $this->gender ? $this->gender : 'm';

        $gender = $this->gender == 'f' ?  'women' : 'man';
        
        $range = range(1,60);
        
        $n = $range[ array_rand($range) ];

        return 'https://randomuser.me/api/portraits/'.$gender.'/'.$n.'.jpg';
    }

    public function getFullPath( $name){
        return Storage::disk('public')->getDriver()->getAdapter()->applyPathPrefix($name);
    }


    public function saveAvatar($path, $data)
    {
        if( $this->avatar_name )
        {
            $this->removeAvatar();
        }

        return file_put_contents($this->getFullPath( $path ) , base64_decode($data));
    }

    public function removeAvatar()
    {
        return Storage::drive('public')->delete($this->avatar_name);
    }

    public function isAdmin(){
        return $this->_hasRole('admin');
    }

    public function setPasswordAttribute($value){
        $this->attributes['password'] = strlen($value) === 60 && preg_match('/^\$2y\$/', $value) ? $value : \Hash::make($value);
    }

    public function projects(){
        return $this->belongsToMany('Modules\Project\Entities\Project', 'projects_users','user_id');
    }

    public function threads()
    {
        return $this->hasMany('Modules\Threads\Entities\Thread','user_id','id');
    }

    public function isMyThread( $thread)
    {
        $ids = $this->threads->lists('id')->toArray();
        return in_array($thread->id, $ids);
    }

    public function assignedToMe()
    {
        return $this->belongsToMany('Modules\Threads\Entities\Thread','assigned_threads','assigned_to');
    }

    public function assignedByMe()
    {
        return $this->belongsToMany('Modules\Threads\Entities\Thread','assigned_threads','assigned_by');
    }

    public function myThreads( $project )
    {
        $myThreads = $this->threads;
        $assignedThreads = $this->assignedToMe()->where('project_id',$project->id)->get();
        $threads = $myThreads->merge($assignedThreads);
        return $threads->load('assignedTo');
    }

    public static function getRandomAdmins(){
        return self::from('role_user')->where('role_id','1')->inRandomOrder()->first()->user_id;
    }

    public function generateToken($payload = []){
        $tokenData = array_merge([
            'payload' => $this->generateSecret(),
            'timestamp' => time()
        ], $payload);

        return JWTAuth::fromUser($this, $tokenData);
    }

    public function myCompanies()
    {
        return $this->company()->where('company_users.active', 1)->get(['companies.name','companies.id']);
    }

    public function generateSecret(){
        return md5( md5( $this->id . '-' .$this->email . app('company')->id ) );
    }

    public function company() {
        return $this->belongsToMany('App\Company','company_users','user_id','company_id')->withTimestamps();
    }

    public function channels(){
        return $this->belongsToMany('App\Channel','channel_users','user_id','channel_id')->where('company_id', app('company')->id)->withTimestamps();
    }

    public function myChannels($column = 'id'){
        return $this->channels
            ->lists($column)
            ->toArray();
    }


    public function channelTypeChat(  )
    {
        $query = Chat::byChannel()->whereIn( 'channel_id', $this->myChannels() )->where('company_id',app('company')->id);

        $query = $query->latest()->groupBy('channel_id')->select(\DB::raw('MAX(id) as max_ids'))->orderBy('id','desc');

        $recentChats = $query->get()->lists('max_ids');

        return Chat::whereIn('id',$recentChats->toArray())->latest()->get();
    }

    public function userTypeChat()
    {
        $query = Chat::byPrivate()->where('company_id', app('company')->id);

        $query->where(function( $q ) {

            $q->orWhere( 'to', $this->id )->orWhere( 'author_id', $this->id );

        })->groupBy(['to','author_id'])->select(\DB::raw('MAX(id) as max_ids , SUM(IF(`read` = 0 , 1 , 0)) as totalUnread, `to`, `author_id`'))->orderBy('id','desc');

        $recentChats = $query->get();

        return [Chat::whereIn('id',$recentChats->lists('max_ids'))->latest()->get(), $recentChats];

    }

    public function myChats() {
        
        $chat['direct'] = $this->userTypeChat();
        
        $chat['channel'] = $this->channelTypeChat();

        return $chat;
    }

    public function isMyComapany($id){
        return $this->company()->where('company_id', $id)->count() !== 0;
    }

    public function assignCompany( $company ){
        $this->company()->syncWithoutDetaching( [ $company->id ] );
    }

    public function assign($roleId){
        return $this->roles()->sync([$roleId]);
    }

    public function _myRoles( $company = null )
    {
        $company = is_null( $company ) ? app('company') : $company;
        
        if( !isset( $this->myRole ) )
        {
            $rel = Role::from('role_user')->where('company_id', $company->id )->where('user_id', $this->id )->first();
            if( $rel )
            {
                $this->myRole = Role::where('id', $rel->role_id)->get(['name'])->lists('name')->toArray();
            }

        }

        return $this->myRole[0];
    }

    public function _hasRole($roles = [])
    {
        $roles = is_array($roles) ? $roles : [$roles];

        $myRole = $this->_myRoles();

        foreach($roles as $role)
        {
            if( in_array($myRole, $roles) )
            {
                return true;
            }
        }

        return false;
    }

    public function _assignRole( $company, $role )
    {
        $this->clearAllRole($company);

        return $this->from('role_user')->insert([
            'role_id' => $role->id,
            'user_id' => $this->id,
            'company_id' => $company->id
        ]);
    }

    public function clearAllRole($company)
    {
        return $this->from('role_user')->where('company_id', $company->id)->where('user_id', $this->id)->delete();
    }

    public function filterUserList($list)
    {
        if( $this->isAdmin() ) return $list;

        return array_filter($list, function($user)
        {
            if( $this->_hasRole('client') && $user['role_name'] === 'developer' )
            {
                return false;
            }

            if( $this->_hasRole('developer') && $user['role_name'] === 'client' )
            {
                return false;
            }

            return true;
        });
    }

    public function canInteract( $user )
    {
        if( $this->_hasRole('client') && $user->_hasRole('developer') )
        {
           return false;
        }

        if( $user->_hasRole('client') && $this->_hasRole('developer') )
        {
            return false;
        }

        if( $user->_hasRole('client') && $this->_hasRole('client') && $this->id !== $user->id)
        {
            return false;
        }

        return true;
    }

    public function notifications(){
        return $this->hasMany('App\Notification', 'user_id','id');
    }

    public function getMyNofications($id = null){

        $query =  $this->notifications()->where('notifications.company_id', app('company')->id)->with(['comment','thread'])->latest();

        if( $id )
        {
            $query->where('notifications.id', $id);
        }

        return $query->get();
    }

    public static function checkIfAlreadyRegistered($emails)
    {
        $sameCompanyUsers = app('company')->areFromSame( $emails ,'users.email');

        if( $sameCompanyUsers->isEmpty() )
        {
            return ['email' => $emails];
        }
        
        $companyUsers = $sameCompanyUsers->filter(function($u)use($emails){
            return in_array($u->email, $emails);
        });

        $emailUsers = array_diff($emails, $companyUsers->lists('email')->toArray());

        return ['users' => $companyUsers->lists('id')->toArray(),'email' => $emailUsers];
    }
}
