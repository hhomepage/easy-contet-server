<?php
/**
 * Created by PhpStorm.
 * User: sishir
 * Date: 3/6/16
 * Time: 12:32 PM
 */

namespace App;
use Illuminate\Database\Eloquent\Model;
use Image;
use Storage;
class Resource extends Model
{
    public $table = "resources";

    protected $fillable = [
    	'path',
    	'client_name',
    	'type',
    	'ext',
    	'size',
    	'last_modified', 
     	'company_id',
    	'user_id',
    	'name',
    	'folder',
        'caption',
    ];

    public function getMetaAttribute($value)
    {
        return json_decode($value, true);
    }

    protected $appends = ['public_path','download_path'];

    public function getPublicPathAttribute(){
        return $this->isImage() ? asset('uploads/' . $this->name) : '';
    }

    public function getDownloadPathAttribute(){
        return url('api/v1/download/resource/' . $this->id );
    }

    public function isImage(){
        return $this->type == 'image';
    }

    public function isRawFile(){
        return $this->type == 'text';
    }

    public function isApplication(){
        return !$this->isImage() && !$this->isRawFile();
    }

    public function imagable(){
        return $this->morphTo();
    }

    public function getType(){
        return 'imagable_id';
    }

    public function isAssigned(){
        return $this->imagable_id != 0;
    }

    public function isFolder(){
        return $this->folder == 1;
    }

    public function isCover(){
        return is_array($this->meta) && $this->meta['cover'] == 1 ? true : false; 
    }

    public function makeBase64($path = null)
    {
        $path = $path ? $path : $this->path;
        $image = Image::make( Storage::get( $path ) );
        $image->encode('png');
        $type = 'png';
        return 'data:image/' . $type . ';base64,' . base64_encode($image);
    }
}