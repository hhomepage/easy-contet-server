<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
//

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {

	Route::controllers([
		'password' => 'Auth\PasswordController',
	]);

	Route::get('auth/login', function(){

		if( Auth::check() && app('company'))
		{
			return Res::redirectToHome(app('company')->unique);
		}
		else
		{
			Auth::logout();
			return view('auth.login');
		}

	});

	Route::get('company', function()
	{
		return view('auth.company');
	
	})->middleware('guest');

	Route::get('/', function(){

		if( !Auth::user() )
		{
			return redirect('company');
		}
		else
		{	
			if( app('company') )
			{
				return Res::redirectToHome(app('company')->unique);
			}

			Auth::logout();

			return redirect('company');
		}
	});

    Route::post('login', function(Illuminate\Http\Request $request){

    	    if ( Auth::attempt( ['email' => $request->get('email'), 'password' => $request->get('password') ], true ) ) 
    	    {
    	    	$companies = Auth::user()->myCompanies();

    	    	if( !$companies->isEmpty() )
    	    	{
					return Res::success(['companies' => $companies],'Choose Your Company To Countinue Login');
    	    	}
    	    	else
    	    	{
    	    		Auth::logout();

    	    		return Res::fail(['comany' => false], 'You are not assigned to any companies. You can create your own');
    	    	}

        	}
        	else
        	{	
        		$user = App\User::where('email', $request->get('email') )->first();

        		$response = [];
        		$message = [];

        		if( $user )
        		{
        			$message = 'The Login Creadentails are invalid. Please try again';

        			$response['user'] = ['avatar' => $user->avatar ,'name' => $user->name ];
        		}


				return Res::fail($response, $message);    		
        	}
    });

    Route::post('select-company/{company}', function($company)
    {
    	if( Auth::user() && !app('company') )
    	{
    		$company = App\Company::find($company);

    		if( $company->isPaid() && $company->isActiveUser( Auth::user() ) )
    		{
    			Session::put('company', $company->toJson());
    			return Res::success(['redirectTo' => Res::getHomeUrl($company->unique)]);
    		}
    	}

		return Res::fail([],'You cannot login to this company. They might have not paid. Sorry');
    });

    Route::group(['middleware' => ['auth']], function(){

    	Route::get('/dev', function()
		{
			$html = file_get_contents('http://localhost:3000');

			$html = preg_replace("!(href|src)=('|\")\.\.\/(bower_components)!","$1=$2$3", $html);
				
			$css = file_get_contents('http://localhost:3000/app/index.css');

			exec('touch index.css');

			file_put_contents( public_path('css/index.css'), $css );	

			$html = preg_replace('!(href=(?:\'|"))app/index\.css!', '$1css/index.css', $html);

			$html = preg_replace("!(href|src)=('|\")app!","$1=$2src/app", $html);

			$html = preg_replace("!(href|src)=('|\")app!","$1=$2src/app", $html);

			return $html;

		});

        Route::get('logout', function()
        {
        	$user = Auth::user()->generateSecret();
        
        	$company = app('company')->generateUnique();
        
            Auth::logout();

        
            event(new App\Events\Authenticate(['user' => $user,'redirect' => url('auth/login'),'company' => $company],'app:logout'));
        
            Session::forget('company');
            return redirect('/auth/login');
        });

        Route::get('projects/{company?}', function($company = null)
    	{
    		if( is_null($company) )
    		{
    			return Res::redirectToHome( app('company')->unique );
    		}

    		return view('index');

    	})->name('home');
    });

	Route::group(['prefix' => 'api/v1','middleware' => ['auth']], function() {
		Route::post('upload', 'ResourceController@uploadFile');
		Route::get('download/resource/{resource}','ResourceController@downloadFile');
		Route::get('preview/resource/{resource}','ResourceController@renderFile');
		Route::post('resource/{resource}', 'ResourceController@editResource')->middleware(['my.entity:resource','redux.nonce']);
		Route::post('resource/create', 'ResourceController@makeDirectory');
		Route::delete('resource/{resource}', 'ResourceController@deleteResource')->middleware(['my.entity:resource','redux.nonce']);
		Route::post('resource/crawl', 'ResourceController@getResources');

		Route::get('me', function() {

			$user = Auth::user();

			$company = ['company' => json_decode(Session::get('company'), true)['id'] ];

			$user->token = $user->generateToken( $company );

			$user->role_name = $user->_myRoles();

			$user->company = App\Company::find( $company['company'] );

			event( new App\Events\Authenticate( array_merge($user->toArray(), ['channels' => $user->channels->lists('unique')->toArray() ] ) ,'authentication') );

			$user = $user->toArray();

			unset($user['channels']);

			return Res::success( $user );
		});


		Route::get('chat-init','ChatController@init');

		Route::post('chat', 'ChatController@newMessage');

		Route::post('chat/stopped-typing', 'ChatController@stoppedTyping');

		Route::post('chat/user-typing', 'ChatController@userTyping');

		Route::post('chat/search', 'ChatController@searchChats');
		
		Route::get('chat/jump/{chat}', 'ChatController@getJumpToChat');

		Route::post('chat/load-more/{chat?}','ChatController@loadMoreChats');

		Route::post('chat/mark-read','ChatController@changeChatStatus');

		Route::post('chat/{chat}','ChatController@editChat');
		
		Route::delete('chat/{chat}','ChatController@deleteChat');

	});
});

Route::bind('label',function($labelId){

	if($label = App\Label::find($labelId))
	{
		return $label;
	}
	else
	{
		throw new \Illuminate\Database\Eloquent\ModelNotFoundException;
	}
});

Route::bind('channel',function($channelId){

	if($channel = App\Channel::find($channelId) )
	{
		return $channel;
	}
	else
	{
		throw new \Illuminate\Database\Eloquent\ModelNotFoundException;
	}
});

Route::bind('chat',function($chatId){
	if($chat = App\Chat::find($chatId) )
	{
		return $chat;
	}
	else
	{
		throw new \Illuminate\Database\Eloquent\ModelNotFoundException;
	}
});

Route::bind('resource',function($resourceId){

	if($resource = App\Resource::find($resourceId))
	{
		return $resource;
	}
	else
	{
		throw new \Illuminate\Database\Eloquent\ModelNotFoundException;
	}
});