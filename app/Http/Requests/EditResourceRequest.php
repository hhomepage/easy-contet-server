<?php 
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditResourceRequest extends FormRequest {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return $this->route('resource')->user_id === \Auth::user()->id;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'img_base64' => 'regex:/^(?:data\:image\/png\;base64\,)/'
		];
	}

}
