<?php

namespace App\Http\Middleware;

use Closure;

use Auth;

class BelongsToProject
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    protected $models = [];

    public function handle($request, Closure $next, $userSource = 'auth')
    {
        $user = $userSource === 'auth' ? Auth::user() : $request->route('user');

        if( $request->route('project') &&  ( ( $userSource == 'auth' && !$user->isAdmin() ) || $userSource != 'auth' ) )
        {
            $project = $request->route('project');

            if( $project->belongs( $user ) )
            {
                return $next($request);
            }
            return \App::abort(403);
        }

        return $next($request);
    }
}
