<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Session;
use Res;

class Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if( Auth::check() && app('company') && app('company')->isActiveUser(Auth::user()) )
        {
            return $next($request);
        }
        else
        {
            Auth::logout();

            Session::forget('company');

            if ( $request->ajax() || $request->wantsJson() ) {

                return Res::fail(['redirect' => url('auth/login')], '',401);
            
            } else {

                return redirect('auth/login');
            }
        }
    }
}
