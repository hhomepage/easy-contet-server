<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use Auth;
use Res;
class UserCompany
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $source = 'route')
    {
        $user = $source === 'route' ? $request->route('user') : Auth::user(); 

        if( $user )
        {
            $company = json_decode( Session::get('company') , true);

            if( $user->isMyComapany( $company['id'] ) )
            {
                return $next($request);
            }
            else
            {
                return Res::fail([],'You donot belong to this company', 403);
            }
        }
        
        return $next($request);
    }
}
