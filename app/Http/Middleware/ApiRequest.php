<?php

namespace App\Http\Middleware;
use JWTAuth;
use Closure;
use Auth;
class ApiRequest
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle( $request, Closure $next )
    {

        // $type = $request->header('X-TYPE') ;

        // $type = $type ? $type : $request->get('type');

        // if ( $type == 'admin' )
        // {
        //     Auth::loginUsingId( 1 );
        // }

        // if ( $type == 'developer' )
        // {
        //     Auth::loginUsingId( 2 );
        // }

        // if ( $type == 'client' )
        // {
        //     Auth::loginUsingId( 3 );
        // }

        // if( Auth::check() )
        // {
        //     \Session::put('company', \App\Company::find(1)->toJson());
        // }
        
        return $next( $request );
    }
}
