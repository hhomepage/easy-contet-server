<?php

namespace App\Http\Middleware;

use Closure;

class MorphBelongs
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $param)
    {
        $params = explode('|', $param);

        $model = $params[0];

        $morphed = $params[1];
        
        if( $request->route($morphed) && count( $params ) === 3 )
        {
            $_morphed = $request->route($morphed);

            if( $_morphed->{$_morphed->getType()} != 0 )
            {
                return \App::abort(403);
            }

            else
            {
                 return $next($request);
            }
        }


        if( $request->route($model) && $request->route( $morphed ) )
        {
            $morphed = $request->route($morphed);
            $model = $request->route($model);

            $typeKey = $morphed->getType();

            if( $morphed->{$typeKey} != $model->id )
            {
                return \App::abort(403);
            }
        }

        return $next($request);
    }
}
