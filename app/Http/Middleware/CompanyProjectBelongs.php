<?php

namespace App\Http\Middleware;

use Closure;
use Res;
use Session;


class CompanyProjectBelongs
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $company = json_decode( Session::get('company') , true);

        if( $request->route('project') )
        {
            $project = $request->route('project');

            if( $project->company_id !== $company['id'] )
            {
                return Res::fail([],'Unathroized', 403);
            }
        }

        return $next($request);
    }
}
