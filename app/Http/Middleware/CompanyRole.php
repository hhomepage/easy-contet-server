<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class CompanyRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $roles)
    {            

        if ( Auth::guest() || !Auth::user()->_hasRole( explode('|', $roles) ) ) {

            abort(403);
        }

        return $next( $request );
    }
}
