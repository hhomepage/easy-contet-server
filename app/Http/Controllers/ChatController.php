<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Http\Requests\TypingEventRequest;
use App\Http\Requests\ChatStatusChangeRequest;
use App\Http\Requests\LoadMoreChatRequest;

use App\User;
use App\Chat;
use App\Channel;
use App\Company; 
use Auth;
use Res;
use App\Resource;

use App\Http\Requests\ChatSearchRequest;
use App\Repositories\ChatSearch\ChannelSearchHandler;
use App\Repositories\ChatSearch\UserSearchHandler;
use App\Repositories\ChatSearch\HasSearchHandler;
use App\Repositories\ChatSearch\DateSearchHandler;
use App\Repositories\ChatSearch\QueySearchHandler;

class ChatController extends Controller
{
    protected $user;
	protected $company;

    public function __construct()
    {
    	$this->user = Auth::user();
        $this->company = app('company');
    }

    public function init( Request $request ) {
        
        $chats = $this->user->myChats();

        $channels = $this->user->channels->load('userIds')->toArray();

        foreach($channels as $key => $channel)
        {
            $channels[$key]['user_ids'] = array_column($channel['user_ids'],'user_id');
        }

        return Res::success([
            'recentChat' => $this->buildChats($chats), 
            'channels' => $channels,
        ]);
    }

    private function getUnread($unreads, $mainId)
    {
        foreach($unreads as $unread)
        {
            if( $unread['author_id'] === $mainId )
            {
                return (int)$unread['totalUnread'];
            }
        }
    }

    private function buildChats($chats)
    {
        $toReturn = ['direct' => [], 'channel' => []];

        $unreads = $chats['direct'][1];

        $chats['direct'] = $chats['direct'][0];

        foreach($chats['direct'] as $chat)
        {
            if( $chat->to === $this->user->id || $chat->author_id === $this->user->id )
            {
                $mainId = $chat->to === $this->user->id ? $chat->author_id : $chat->to;

                $chat->unread = $this->getUnread($unreads, $mainId);

                $toReturn['direct'][ $mainId ][] = $chat;
            }
        }

        foreach($chats['channel'] as $chat)
        {
            $toReturn['channel'][$chat->channel_id][] = $chat;
        }

        // print_r($unreads->toArray());

        return $toReturn;
    }

    public function searchChats(ChatSearchRequest $request)
    {
        $request = $request->all();

        if( count(array_keys($request)) === 0 && count(array_keys($request)) < 4)
        {
            return Res::fail([], 'Search Invalid', 422);
        }

        $searchHandler = new ChannelSearchHandler();
            
        $searchHandler->setSuccessor(new UserSearchHandler);

        $searchHandler->setSuccessor(new HasSearchHandler);

        $searchHandler->setSuccessor(new DateSearchHandler());

        $searchHandler->setSuccessor(new QueySearchHandler());

        list( $req, $query ) = $searchHandler->handle($request, Chat::where('company_id', $this->company->id ) );

        $results = $query->orderBy('created_at','desc')->paginate(10);

        $binding = $query->getBindings();
        
        if( !$results->isEmpty() )
        {
            $results = $results->toArray();

            $results['data'] =  $this->addMetaOnSearchResults($results['data']);
            $results['sql'] = $query->toSql();            
            $results['binding'] = $binding;      
        }
        else
        {
            $results = $results->toArray();

            $results['binding'] = $binding;      
            $results['sql'] = $query->toSql();            
        }

        return Res::success( $results );
    }

    private function addMetaOnSearchResults($results)
    {
        $forLoadingResources = [];

        $afterChunkLoaded = [];

        foreach($results as $key => $result)
        {
            $chunk = $this->loadAChunk( $result );

            $results[$key]['list'] = $chunk;

            $forLoadingResources = array_merge($forLoadingResources, $results[$key]['list']);
        }

        $forLoadingResources = $this->loadResources($forLoadingResources);

        // print_r($forLoadingResources);
        // die();

        foreach($forLoadingResources as $list)
        {
            $afterChunkLoaded[ $list['id'] ] = $list;
        }

        $idsOfResourcesChunk = array_keys($afterChunkLoaded);

        foreach($results as $key => $result)
        {
            $chunk = $result['list'];

            foreach($chunk as $chunkKey => $ch )
            {
                $results[ $key ]['list'][$chunkKey] = in_array($ch['id'], $idsOfResourcesChunk) ? $afterChunkLoaded[ $ch['id'] ] :  $ch;
            }
        }

        return $results;
    }

    /**
     * This is for loading the nearest ids of a chat
     * Need Some Improvements
     * 
     */

    private function loadAChunk($chat, $limit = 1)
    {
        $query = Chat::where('company_id', $this->company->id);

        $chat['channel_id'] ? $query->where('channel_id', $chat['channel_id'] ) : $query->where(function($query)use($chat){

            $query->orWhere(function($query)use($chat){
            
                return $query->where('author_id', $chat['to'])->where('to', $chat['author_id']);
            
            })->orWhere(function($query)use($chat){
            
                return $query->where('author_id', $chat['author_id'])->where('to', $chat['to']);
            
            })->distinct('chat.id');
        });

        $queryA = clone $query;

        $queryA->where('id', '>', $chat['id'])->orderBy('id','asc')->limit($limit);

        $query->where('id','<', $chat['id'])->orderBy('id', 'desc')->limit($limit);

        $nearestResutls = $query->union( $queryA )->get();

        $nearestResutls->push( $chat );

        $nearestResutls = $nearestResutls->sortBy('id')->values()->all();

        $nearestResutls = $limit === 1 ? $nearestResutls : $this->loadResources( $nearestResutls );

        return $nearestResutls;
    }


    public function loadResources($list)
    {
        $ids = array_column($list,'id');

        $list = is_array($list) ? $list : $list->toArray();

        $ids = array_unique($ids);

        $resources = Resource::whereIn( 'imagable_id', $ids )->get();

        if( !$resources->isEmpty() )
        {
            $allResources = [];
            
            foreach( $resources as $resource)
            {
                $allResources[ $resource->imagable_id ][] = $resource;
            }

            $allIds = array_keys($allResources);

            foreach($list as $key => $chat)
            {
                if( in_array($chat['id'], $allIds) )
                {
                    $list[$key]['resources'] = $allResources[ $chat['id'] ];
                }
            }
        }

        return $list;
    }
    

    /**
     * Load More
        
        -called when loading the chat for the first time
        -called for latest chat and oldest chat 
            *if the user search is enable then this case

     */

    public function loadMoreChats(LoadMoreChatRequest $request, $chat = null){

        $query = Chat::where('company_id', $this->company->id);

        $type = $request->get('type');

        $to = $request->get('to');

        if( $type === 'channel' )
        {
            if( $to = Channel::where('company_id', app('company')->id)->where('id', $to)->first() )
            {
                if( !$to->belongs($this->user ) )
                {
                    return Res::fail();
                }
            }
        }

        if( $type === 'direct' )
        {
            $to = User::find($to);

            if( $this->company->belongs( $to ) && !$this->user->canInteract( $to ) )
            {
                return Res::fail();
            }
        }

        if( $chat && $chat->isAssociatedWithUser( $this->user ) )
        {
            if( $request->has('direction') )
            {
                $request->get('direction') === 'ab' ? $query->where('chats.id', '<', $chat->id) : $query->where('chats.id' , '>', $chat->id);
            }
            else
            {
                return Res::fail();
            }
        }
       
        $type === 'channel' ? $query->where('channel_id', $to->id ) : $query->where(function($query)use($to){

            $query->orWhere(function($query)use($to){
            
                return $query->where('author_id', $to->id)->where('to', $this->user->id);
            
            })->orWhere(function($query)use($to){
            
                return $query->where('author_id', $this->user->id)->where('to', $to->id);
            
            });

        });

        $result = $query->orderBy('id', 'desc');

        $result = $result->paginate(20)->toArray();

        $result['data'] = $this->loadResources( $result['data'] );

        return Res::success( $result );
    }

    public function getJumpToChat(Chat $chat)
    {
        if( ($chat->author_id === $this->user->id || $chat->to === $this->user->id || $chat->channel_id) && $chat->company_id === $this->company->id )
        {
            if( $chat->channel_id )
            {
                if( $chat->channel->belongs( $this->user ) )
                {
                    return Res::success($this->loadAChunk($chat->toArray(), 10));
                }
                else
                {
                    return Res::fail();
                }
            }

            return Res::success($this->loadAChunk($chat->toArray(), 10));
        }
        
        return Res::fail();
    }

    public function validateRoles( $user )
    {
        if( $this->company->belongs( $user ) )
        {
            if( $user->_hasRole('client') && $this->user->_hasRole('developer') )
            {
                return false;
            }

            if( $this->user->_hasRole('client') && $user->_hasRole('developer') )
            {
                return false;
            }

            return true;
        }
    }

    public function editChat(Request $request, Chat $chat)
    {
        if( $chat->isOwner( $this->user ) && $request->has('message') )
        {
            $chat->message = $request->get('message');

            $chat->edited = 1;

            $to = $this->getUsersForSocketChat($chat);

            $payload['entity'] = $chat->getInvolvedEntity($this->user);

            $payload['type'] = $chat->getType();
            
            $payload['id'] = $chat->id;
           
            $chat->save();

            $payload['chat'] = $chat->toArray();

            Res::broadCastDynamic('chat:edited', $to, $payload );

            return Res::success();
        }

        return Res::fail();
    }

    public function getUsersForSocketChat($chat)
    {
        if( $chat->isChannel() )
        {
            $secrets[] = $chat->channel->unique;
        }
        else
        {
            $secrets[] = $chat->author->generateSecret();
            
            $secrets[] = $chat->receiver->generateSecret();
        }

        return $secrets;
    }

    public function deleteChat(Request $request, Chat $chat)
    {
        if( $chat->isOwner( $this->user ) )
        {
           $to = $this->getUsersForSocketChat($chat);

            $payload['type'] = $chat->getType();

            $payload['entity'] = $chat->getInvolvedEntity($this->user);
            
            $payload['id'] = $chat->id;

            $chat->delete();

           Res::broadCastDynamic('chat:deleted', $to, $payload );

           return Res::success();
        }

        return Res::fail();
    }

    public function checkForGiphy($message){

        if( preg_match('!^gif\:(.+)$!', $message, $matches) )
        {
            $search = $matches[1];
            $giphyResults = @file_get_contents('http://api.giphy.com/v1/gifs/search?q='. $search .'&api_key=dc6zaTOxFJmzC');

            $giphyResults = json_decode($giphyResults, true);

            if( $giphyResults && count($giphyResults['data']) !== 0 )
            {
                $randomKey = array_rand($giphyResults['data']);

                $message = 'Giphy Search: ' . $search . ' ' . $giphyResults['data'][$randomKey]['images']['downsized']['url'];
            }

        }

        return $message;
    }

    public function newMessage(Requests\ChatRequest $request)
    {
        $to = $request->get('to');

        $type = $request->get('type');

        $chat = null;

        if( $type === 'channel' )
        {
            $channel = Channel::find($to);

            if( $channel && $channel->belongs( $this->user ) )
            {
                $chat = new Chat;

                $chat->channel_id = $channel->id;

                $chat->type = 0;

                $chat->read = 1;

                $socketParam = ['to' => $channel->unique ]; 
            }
        }
        else
        {
            $to = User::find($to);

            if( $to &&  $this->validateRoles( $to ) )
            {
                $chat = new Chat;

                $chat->read = ( $to->id === $this->user->id ) ? 1 : 0;

                $chat->to = $to->id;

                $chat->type = 1;

                $socketParam = ['to' => $to->generateSecret() , 'from' => $this->user->generateSecret() ]; 
            }
        }

        if( $chat )
        {
            $chat->fill( $request->all() );

            $chat->author_id = $this->user->id;

            $chat->company_id = $this->company->id;

            $chat->message = $this->checkForGiphy( $chat->message );

            $chat->has_link = $this->checkIfUrl( $chat->message );

            $chat->message = $chat->message ? $chat->message : '';

            $chat->save();

            if( $request->has('resource_ids') )
            {
                $chat->resources()->saveMany( Resource::find( $request->get('resource_ids') ) );

                $chat->has_file = 1;

                $chat->save();
            }

            $chat->load('resources');

            $socketParam['payload'] = $chat;

            event(new \App\Events\Authenticate( $socketParam, 'redux:private-message' ));
            
            return Res::success();
        }

        return Res::fail();
    }


    public function checkIfUrl( $message )
    {
        if(preg_match_all('#\bhttps?://[^,\s()<>]+(?:\([\w\d]+\)|([^,[:punct:]\s]|/))#', $message) )
        {
            return 1;
        }
        
        return 0;
    }



















    public function validateTypingRequest( $request ){

        $from = $this->user;

        $channel = null;
        $user = null;
        $error = false;

        if( $request['type'] == 'direct' )
        {
            $user = User::find($request['to']);

            if( !$this->user->canInteract($user) )
            {
                return false;
            }

            if( $user->id === $from->id )
            {
                return false;
            }

            $params = [
                'to' => $user->generateSecret(),
                'type' => 'direct',
                'id' => $this->user->id,
                'writer' => $this->user->id
            ];
        }
        else if( $request['type'] == 'channel' )
        {
            $channel = Channel::where('id',$request['to'])->where('company_id', $this->company->id )->first();

            if( $from->_hasRole('client') || ($channel &&  !in_array($from->id, $channel->users->lists('id')->toArray() ) ) )
            {
                return false;
            }

            $params = [
                'to' => $channel->unique,
                'type' => 'channel',
                'id' => $channel->id,
                'writer' => $this->user->id
            ];
        }

        return $params;
    }

    public function stoppedTyping(TypingEventRequest $request)
    {
        if( $params = $this->validateTypingRequest( $request->all() ) )
        {
            if( is_array($params) )
            {
                 event(new \App\Events\Authenticate( $params, 'chat:stopped-typing' ));
            }
        }
    }

    public function userTyping(TypingEventRequest $request)
    {
        if( $params = $this->validateTypingRequest($request->all()) )
        {
            if( is_array($params) )
            {
              event( new \App\Events\Authenticate( $params, 'chat:start-typing' ));
            }
        }
    }

    public function changeChatStatus(ChatStatusChangeRequest $request){

        $type = $request->get('type');

        $entity = $request->get('entity');

        $error = true;

        if( $type === 'direct' )
        {
            $error = Chat::markAsRead( User::find( $entity ) , $this->user, $this->company );

            $error = $error ? $error->isEmpty() : true;

        }
        else
        {
            if( $channel = Channel::findChannel($entity) )
            {
                if( $channel->belongs($this->user) &&  Chat::where('channel_id', $entity)->count() !== 0)
                {
                    $error = false;
                }
            }
        }

        if( $error === false )
        {
            Res::broadCastDynamic('chat:set-read', [$this->user->generateSecret() ], 
                [
                    'type' => $type,
                    'entity' => $entity
                ]
            );

        }

        return Res::success();
    }
}