<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Label;
use App\Http\Requests;
use Res;
use Session;

class LabelController extends Controller
{
    public function __construct(){
    }

    public function create( Request $request, $label = null){
    	$label = $label ?: new Label();
    	$label->fill( $request->all() );
        $label->company_id = 1;
    	$label->save();

    	return Res::success($label);
    }

    public function delete(Label $label){
    	$label->delete();
    	return Res::success();
    }
}
