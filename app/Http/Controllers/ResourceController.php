<?php namespace App\Http\Controllers;
	
use Illuminate\Http\Request;
use App\Http\Requests as PostRequest;
use App\Repositories\ResourceRepo;
use Storage;
use File;
use Res;
use Session;
use App\Resource;
use Image;
use Modules\Threads\Entities\Thread;
use App\Events\Authenticate;


class ResourceController extends Controller{

	public $repo;

	protected $user;

	protected $company;

	public function __construct()
	{
		$this->user = \Auth::user();
		
		$this->repo = new ResourceRepo;
		
		$this->company = json_decode(Session::get('company'), true);
		
		$this->rootPath = $this->company['unique'];

		if( !Storage::exists( $this->rootPath ) )
		{
			Storage::makeDirectory( $this->rootPath );

			$response = $this->repo->saveResource([
				'path' => $this->rootPath,
				'user_id' => $this->user->id,
				'company_id' => $this->company['id'],
				'folder' => true, 
				'name' => $this->company['unique']
			]);
		}

		$this->rootPath .=  DIRECTORY_SEPARATOR;
	}

	public function uploadFile( PostRequest\UploadFileRequest $request )
	{
		if( !\Request::hasFile('uploaded_file') )
		{
			return Res::fail($_FILES);
		}

		$files = $request->file('uploaded_file');

		$path = $this->rootPath . $request->get('path');

		if( !Storage::exists( $path ) )
		{
			Storage::makeDirectory( $path );
		}

		$response = [];

		foreach($files as $file)
		{
			$ext = $file->getMimeType();

			$type = explode('/',$file->getMimeType())[0];

			$clientName = $file->getClientOriginalName();

			$extension = $file->getClientOriginalExtension();

			$newName = str_random(7) . time() . ( $extension ?  ('.' . $extension ) : '');

			$_path = $path . DIRECTORY_SEPARATOR . $newName;

			Storage::put( $_path , File::get( $file ) );

			$size = Storage::size( $_path );

			$resource = $this->repo->saveResource([ 
				'path' => $_path,
				'name' => $newName,
				'client_name' => $request->has('name') ? $request->get('name') : $clientName , 
				'ext' => $extension, 
				'size' => $size,
				'last_modified' =>  Storage::lastModified( $_path ),
				'company_id' => $this->company['id'],
				'user_id' => $this->user->id,
				'type' => $type,
				'caption' => $request->has('comment') ?  $request->get('comment') : ''
			]);

			$response[] = $resource;

			if( in_array( $type, [ 'image' ] ) )
			{
				if( in_array( $extension , [ 'png','jpg','jpeg','bmp','JPEG','JPG', 'gif' ] )  )
				{
					$this->saveImage( $newName, File::get($file) );
				}
				else
				{
					$resource->type = $resource->ext;
					$resource->save();
				}
			}

		}

		return Res::success( $response );
	}

	public function editResource(PostRequest\EditResourceRequest $request, Resource $resource)
	{
		$caption = $request->get('caption');

		if( $request->has('img_base64') && $resource->isImage())
		{
			$img = str_replace(' ', '+', str_replace('data:image/png;base64,', '', $request->get('img_base64')));

			$name = pathinfo($resource->path, PATHINFO_FILENAME);

			$newName = substr($name, 0, strlen($name) - 4) . str_random(4);

			$path = dirname($resource->path);

			$newPath = $path . DIRECTORY_SEPARATOR . $newName . '.' . 'png';

			$fullPath = $this->getFullPath('local', $newPath);

			file_put_contents($fullPath,  base64_decode($img) );

			Storage::delete( $resource->path );

			Storage::disk('public')->delete($resource->name);

			$resource->ext = 'png';

			$resource->last_modified = Storage::lastModified( $newPath );

			$resource->path = $newPath;

			$resource->name = $newName . '.png';

			$resource->size = Storage::size( $newPath );

			$this->saveImage( $resource->name, Storage::get( $newPath ));
		}

		$resource->caption = $caption;

		$resource->save();

		return Res::success($resource->fresh());
	}

	public function getFullPath($disk = 'local', $name){
		return Storage::disk($disk)->getDriver()->getAdapter()->applyPathPrefix($name);
	}

	private function saveImage($name, $file)
	{
		Image::make($file)

        ->resize(100, 100 ,function($constraint)
        {
            $constraint->aspectRatio();
        })->save( $this->getFullPath('public', $name) );
	}

	public function makeDirectory(PostRequest\CreateDirectoryRequest $request)
	{
		$name = $request->get('name');

		$path = $request->get('path');

		$path = $path === DIRECTORY_SEPARATOR ? $path : $path . DIRECTORY_SEPARATOR;

		$path = $this->rootPath . $path . $name;

		if( Storage::exists( $path ) )
		{
			return Res::fail([],'Already Exists');
		}


		Storage::makeDirectory( $path );

		$response = $this->repo->saveResource([
			'path' => $path,
			'user_id' => $this->user->id,
			'company_id' => $this->company['id'],
			'folder' => true,
			'name' => $name
		]);

		return Res::success($response);
	}

	public function deleteResource( Resource $resource )
	{
		if( $resource->company_id === $this->company['id'] )
		{
			if( $this->user->isAdmin() || $this->user->id === $resource->user_id )
			{
				if( Storage::exists( $resource->path ) )
				{
					if( $resource->isFolder() )
					{
						if( $files = Storage::files( $resource->path ) )
						{
							return Res::fail(['files_exists' => count($files)]);
						}

						return Storage::deleteDirectory() ? Res::success() : Res::fail([],'Vender error');
					}

					if( Storage::delete( $resource->path ) )
					{
						if( $resource->isImage() )
						{
							Storage::disk('public')->delete( $resource->name );
						}
					}
				}

				if( $resource->isCover() )
				{
					$thread = $resource->imagable_id ?  Thread::find($resource->imagable_id) : false;
					
					if( $thread )
					{
						$project  = $thread->project;

						$to = $project->getMemberSecrets($this->user);

						$this->handleDynamicEvents('thread:removeCardCover', $to, ['project_id' => $project->id, 'thread_id' => $thread->id, 'board_id' => $thread->board_id]);
					}
				}
				
				$resource->delete();

				return Res::success();
			}
		}
		
		return Res::fail([],'Bad Request');

 	}


	public function getResources(PostRequest\DeleteResourceRequest $request)
	{
		$path = $request->get('path');
		
		$path = $this->rootPath . ( $path === '/' ? '' : $path );

		if( Storage::exists( $path ) )
		{
			$files = Storage::files( $path );

			$directories = Storage::directories( $path );

			$all = array_merge( $directories, $files );

			$names = array_map( function( $name )
			{
				return [ 'name' => basename($name) , 'path' => $name ];
			}, $all);

			$resources = $this->repo->getResources( array_column($names, 'name'), $this->company['id'] );
			
			$response = [];

			foreach ($resources as &$value)
			{
				$_resource = $names[ array_search( $value['name'], array_column($names, 'name') ) ];
				
				$value = array_merge($value, $_resource);
			}


			return Res::success($resources);
		}
	}

	public function withBase($path){
		return storage_path('app' . DIRECTORY_SEPARATOR . 'company' . DIRECTORY_SEPARATOR  . $path);
	}

	public function downloadFile(Resource $resource)
	{
		return response()->download( $this->withBase($resource->path) , $resource->client_name);
	}

	public function imgToBase64( $path )
	{
		$image = Image::make( Storage::get($path) );

		$image->encode('png');

		$type = 'png';

		return 'data:image/' . $type . ';base64,' . base64_encode($image);
	}

	public function renderFile(Resource $resource)
	{
		if( $resource->isRawFile() )
		{
			$res = [
				'file_content' => file_get_contents( $this->withBase( $resource->path ) )
			];

			return Res::success($res);
		}

		if( $resource->isImage() )
		{
			return Res::success(['file_content' => $this->imgToBase64( $resource->path ) ]);	
		}

		return Res::fail([]);
	}

	public function handleDynamicEvents($event, $to, $payload)
    {
        event(new Authenticate( ['_event' => 'thread:dueDateChanged', 'to' => $to,'payload' => $payload], 'redis:dynamic-event' ) );
    }
}