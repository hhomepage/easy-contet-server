<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Label extends Model
{
	protected $table = 'labels';

    protected $fillable = ['name','color'];

    public function threads(){
        return $this->morphedByMany('Modules\Threads\Entities\Thread','taggable','morph_labels');
    }

    public function getType(){
        return 'labeltype_id';
    }
}
