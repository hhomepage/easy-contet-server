<?php

	namespace App\Traits;
	use App\User;

	trait TestHelperTrait
	{
		public function callPOST($url, $data =  [],$files = [], $statusCode = 200, $companyId = 1)
		{
			return $this->_call('POST', $url, $data, $files , $statusCode, $companyId);
		}

		public function callGET($url, $data =  [],$files = [], $statusCode = 200, $companyId = 1)
		{
			return $this->_call('GET', $url, $data, $files , $statusCode, $companyId);
		}

		public function callDELETE($url, $data =  [], $files = [], $statusCode = 200, $companyId = 1)
		{
			return $this->_call('DELETE', $url, $data, $files , $statusCode, $companyId);
		}

		public function _call( $method, $url, $data, $files = [], $statusCode, $companyId)
		{
			$response = $this->withSession($this->buildSession($companyId))
				->call($method, '/api/v1/' . $url,$data, [], $files, ['HTTP_X-Requested-With' => 'XMLHttpRequest']);
			
			$this->assertResponseStatus($statusCode);

			return $this->parseResponse($response)['data'];
		}

		public function buildSession($companyId = 1){
			return ['company' => json_encode([
					'id' => $companyId,
					'unique' => 'abc-traders'
				])];
		}

		public function actAs( $id ){
			$user = User::find($id);
			$this->be($user);
			return $user;
		}

		public function beDeveloper(){
			return $this->actAs( 55 );
		}

		public function beAdmin(){
			return $this->actAs( 1 );
		}

		public function beClient(){
			return $this->actAs( 52 );
		}

		public function beDifferentUser(){
			return $this->actAs( 51 );
		}

		public function parseResponse($response, $pluckData = false){
			$res = json_decode($response->getContent(), true); 
			return $pluckData ? $res['data'] : $res;
		}
	}