<?php
/**
 * Created by PhpStorm.
 * User: sishir
 * Date: 2/29/16
 * Time: 1:45 PM
 */

namespace App\Traits;
use App\Company;
use Session;
use Res;

trait ModelCRUD{

	protected $model;
	public $repo;

	public function setModel($model){
		$this->model = $model;
		return $this;
	} 

	public function setRepo( $repo ){
		
		$this->repo = $repo;

		return $this;
	}

	public function save( array $array, $model = null, $response = true){

		$model = is_null( $model ) ? $this->model : $model;

		$model->fill($array);

		if( $model->save() )
		{
			if( $this->repo && method_exists($repo, 'save') )
			{
				$model = $this->repo->save( $model->toArray() );
			}
	
			if( $response )
			{
				return Res::success( $model->fresh() );
			}
				
			return $model->find( $model->id );
		}

		if( $response )
		{
			return Res::fail( [], 'Unkown Error', 503);
		}
	}

	private function executeOnRepo($command, $parameters){

		if( $this->repo && method_exists( $this->repo, $command ) )
		{
			return call_user_func([$this->repo,$command], $parameters);
		}
	}

	public function delete( $model )
	{
		if( $model->delete() )
		{
			$this->executeOnRepo( 'delete', $model->id );
			
			return Res::success([]);
		}

		return Res::fail([],'Unknon Error', 503);
	}

	public function trash($model, $response = true)
	{
		$model->status = 0;
		return $this->save([], $model, $response);
	}

	public function __call($method, $parameters)
	{
		return $this->executeOnRepo($method, $parameters);
	}
}