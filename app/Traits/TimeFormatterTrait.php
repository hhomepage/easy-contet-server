<?php namespace App\Traits;
use Carbon\Carbon;

trait TimeFormatterTrait
{
	public function getCreatedAtAttribute($date)
	{
		// return Carbon::createFromFormat('Y-m-d H:i:s', $date);
	    return $this->changeToUTC($date);
	}

	public function getUpdatedAtAttribute($date)
	{
		// return $date;
		// echo $date;
		 return $this->changeToUTC($date);
	    // return $this->changeToUTCzchangeToUTC($date);
	}

	private function changeToUTC($date)
	{
		$date = Carbon::createFromFormat('Y-m-d H:i:s', $date);


		// $timezone = $date->getTimezone();
		$timezone = $date->timezone->getName();

		return $date->format('Y-m-d H:i:s') . ' ' . $timezone;
	}
}