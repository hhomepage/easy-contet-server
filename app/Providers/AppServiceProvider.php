<?php

namespace App\Providers;

use App\Facade\ResponseHelper;
use Illuminate\Support\ServiceProvider;
use Validator;
use Modules\Project\Repositories\ProjectRepo;
use Auth;
use Illuminate\Http\Request;
use Modules\Project\Entities\Project;
use Session;
use App\Resource;
use App\Company;
use App\User;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
         
         Validator::extend('check_path', function($attr, $value, $parameter, $validator){
            
            $request = $this->app->make(Request::class);


            $attr = 'path';

            if( $request->has( $attr ) ) {
                $path = $request->get( $attr );
            }
            else{

                return false;
            }

            if( preg_match('/(?:\.\.\/|%00)/', $path ) )
            {
                return false;
            }


            $paths = explode('/', $path);

            $rootPath = $paths[0];

            if( $project = Project::whereSlug( $rootPath )->first() )
            {
                if( $project->belongs( Auth::user() ) || Auth::user()->isAdmin() )
                {
                    return true;
                }
            }

            if( in_array( $rootPath, ['chat','comments'] ) )
            {
                return true;
            }

            return false;

       });

        Validator::extend('resource_ids', function($attr, $value, $parameter, $validator){

            if( is_array( $value ) )
            {
                $resources = Resource::whereIn('id',$value)->get();
                
                foreach($resources as $resource)
                {
                    if( $resource->isAssigned() || $resource->user_id != \Auth::user()->id)
                    {
                        return false;                       
                    }
                }

                return true;
            }

        });

        Validator::extendImplicit('required_creation', function($attr, $value, $parameter, $attribute)
        {
            if( request()->get( $attr ) !== null )
            {
                return $value;
            }

            if( $parameter && request()->route($parameter[0] ) )
            {
                return true;
            }
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('responseHelper',function(){
            return new ResponseHelper();
        });

        $this->app->singleton('company', function()
        {
            if( Session::has('company') )
            {
                return Company::find(json_decode(Session::get('company'), true)['id']);
            }
        });
    }
}
