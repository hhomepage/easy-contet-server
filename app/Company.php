<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $table = 'companies';

    protected $fillable = ['name','unique'];

    public function users()
    {
    	return $this->belongsToMany('App\User','company_users','company_id','user_id')->withPivot(['active','token']);
    }

    public static function buildForActivation($unique, $id)
    {
        return self::where('id', $id)->where('unique', $unique)->where('active', 0)->first();
    }

    public function channels(){
    	return $this->hasMany('App\Channel','company_id','id');
    }

    public function labels(){
    	return $this->hasMany('App\Label','company_id','id');
    }

    public function generateUnique(){
        return 'company_' . $this->unique . '_' . $this->id;
    }

    public function assign( $user , $token = null){
        $this->users()->syncWithoutDetaching([ $user->id] );

        return $this->toogleActive($user, 1, $token);
    }

    public function belongs($user){
        return in_array( $user->id, $this->users->lists('id')->toArray() );
    }

    public function toogleActive($user , $active, $token = null)
    {
        $toUpdate = [
            'active' => $active
       ];

       if( $token )
       {
            $toUpdate['token'] = $token;
       }

        return $this->from('company_users')->where('company_users.company_id', $this->id)->where('company_users.user_id', $user->id)->update($toUpdate);
    }

    public function revoke($user)
    {
       return $this->toogleActive($user, 0);
    }

    public function isActiveUser($user)
    {
       $rel =  $this->freshQuery()->from('company_users')->where('user_id', $user->id)->where('company_id', $this->id)->select('active')->first();

       return $rel && $rel->active;
    }

    public function isPaid()
    {
       $createdDate = strtotime( '+10 days', strtotime((string)$this->created_at) );

       if( $this->active && (time() < $createdDate || $this->paid) )
       {
            return true;
       }
    }

    public function freshQuery(){
        return $this->newQuery();
    }

    public function clearUserResources($user){

        $projects = $user->projects()->where('company_id', $this->id)->lists('projects.id');

        if( !$projects->isEmpty() )
        {
            $this->freshQuery()->from('projects_users')->whereIn('project_id',$projects->toArray())->where('user_id', $user->id)->delete();
        }

        $threads = $user->assignedToMe()->whereIn('project_id', $projects->toArray())->lists('threads.id');

        if( !$threads->isEmpty() )
        {
            $this->freshQuery()->from('assigned_threads')->whereIn('thread_id',$threads->toArray())->where('assigned_to', $user->id)->delete();
        }

        $channels = $user->channels()->where('company_id', $this->id)->lists('channels.id');

        if( !$channels->isEmpty() )
        {
            $this->freshQuery()->from('channel_users')->whereIn('channel_id',$channels->toArray())->where('user_id', $user->id)->delete();
        }
    }

    public function areFromSame($array, $key){
        return $this->from('company_users')->join('users','users.id','=','company_users.user_id')->whereIn($key, $array)->where('company_id', $this->id)->where('active', 1)->get(['users.id','email']);
    }

    public function canReceiveNotification($userIds)
    {
        return $this->from('company_users')->whereIn('user_id', $userIds)->where('company_id', $this->id)->where('active', 1)->get();
    }

    public function invitations(){
        return $this->hasMany('Modules\UserManagement\Entities\Invitation','company_id','id')->where('accepted','0');
    }

    public function alreadyInvited($email)
    {
        return in_array($email, $this->invitations->lists('email')->toArray());
    }
}
