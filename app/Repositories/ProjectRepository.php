<?php namespace App\Repositories;

class ProjectRepository
{
	protected $driver;

	public function __construct(DataDriver $driver){
		$this->driver = $driver;
	}

	public function save($model)
	{
		$this->driver->save($model);
	}
}