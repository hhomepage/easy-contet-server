<?php namespace App\Repositories;
	
use Illuminate\Http\Request;
use App\Resource;

class ResourceRepo {

	public $model;

	public function __construct()
	{
		$this->model = new Resource;
	}

	public function getInstane($id = null)
	{
		return is_null($id) ? new Resource : Resource::find($id);
	}

	public function saveResource($data = [])
	{
		$model = $this->getInstane();

		$model->fill($data);
		
		if( $model->save() )
		{
			return $model;
		}
	}

	public function find($id) {
		return $this->model->find( $id );
	}

	public function delete($id)
	{
		return $this->find( $id )->delete();
	}

	public function moveToTrash($id)
	{
		return $this->model->whereId($id)->update(['in_trash' => true]);
	}

	public function getResources($names = [], $companyId){
		return $this->model->whereIn('name', $names)->where('company_id', $companyId)->groupBy('name')->get()->toArray();
	}
}