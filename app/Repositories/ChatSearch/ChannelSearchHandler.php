<?php namespace App\Repositories\ChatSearch;

use App\Repositories\ChatSearch\ChatSearchHandler;
use App\Channel;

class ChannelSearchHandler extends ChatSearchHandler{
	
	protected function process($request, $query){

		if( $channels = $this->isValid( $request ) )
		{
			$request = $this->modifyRequest($request);

			$query = $this->buildQuery($query, $channels);
		}

		return [$request, $query];
	}

	private function isValid($request)
	{
		if( isset($request['channel']) && isset($request['channel']['in']) )
		{
			$channels = $request['channel']['in'];

			$channels = is_array($channels) ? $channels : [$channels];

			return Channel::areMine($channels, $this->user, $this->company);
		}
	}

	private function modifyRequest($request)
	{
		if( isset($request['user']) && isset( $request['user']['to'] ) )
		{
			unset($request['user']['to']);
		}

		return $request;
	}

	private function buildQuery($query, $ids)
	{
		return $query->whereIn('channel_id', $ids);
	}
}