<?php namespace App\Repositories\ChatSearch;

use App\Repositories\ChatSearch\ChatSearchHandler;

class QueySearchHandler extends ChatSearchHandler{
		
	protected function process($request, $query)
	{
		if( isset( $request['query'] ) && is_array( $request['query'] ) && isset($request['query']['query']) )
		{
			$queries = $request['query']['query'];

			if( count($queries) !== 0 )
			{
				$query->where('message', 'LIKE', '%' . $queries[ count($queries) - 1 ] . '%');
			}
		}

		return [$request, $query];
	}
}