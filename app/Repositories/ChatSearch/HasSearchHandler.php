<?php namespace App\Repositories\ChatSearch;

use App\Repositories\ChatSearch\ChatSearchHandler;

class HasSearchHandler extends ChatSearchHandler{
		
	protected function process($request, $query)
	{
		if( isset( $request['has'] ) && is_array( $request['has'] ) && count($request['has'] ) !== 0 )
		{
			$has = $request['has'];

			if( isset($has['has'] ) )
			{
				$has = $has['has'];

				if( in_array('file', $has ) )
				{
					$query->where('has_file', 1);
				}

				if( in_array('link', $has) )
				{
					$query->where('has_link', 1);
				}
			}
		}

		return [$request, $query];
	}
}