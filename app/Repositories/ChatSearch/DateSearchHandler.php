<?php namespace App\Repositories\ChatSearch;

class DateSearchHandler extends ChatSearchHandler{
	
	protected function process($request, $query)
	{
		if( isset( $request['date'] ) 
				&& is_array( $request['date'] ) 
				&& count($request['date'] ) !== 0 )
		{
			$dates = $request['date'];
			
			$params = array_keys($dates);


			$hasAfterBefore = false;

			if( in_array('after',$params) )
			{
				$figure = $this->checkIfDateFigure($dates['after'][count($dates['after']) - 1]);

				if( $figure )
				{
					$hasAfterBefore = true;

					$query->where('created_at','>',  $figure['date'] );
				}
			}

			if( in_array('before', $params) )
			{

				if( $figure = $this->checkIfDateFigure($dates['before'][count($dates['before']) - 1]) )
				{
					$hasAfterBefore = true;

					$query->where('created_at','<', $figure['date']);
				}
			}

			if( in_array('on', $params) || in_array('during', $params) )
			{
				if( $hasAfterBefore )
				{
					$query = $this->executeOnAfterBefore(['on','during'], $dates, $query);
				}
				else
				{
					$query = $this->executeNotOnAfterBefore(['on','during'], $dates, $query);
				}
			}
		}

		return [$request, $query];
	}

	private function executeOnAfterBefore($expected, $dates, $query)
	{
		$params = array_keys($dates);

		foreach($expected as $expec)
		{
			if( in_array( $expec, $params ) )
			{
				if( $figure = $this->checkIfDateFigure( $dates[$expec][count($dates[$expec]) - 1] ) )
				{
					$column = $figure['type'] === 'day' ? 'dayofweek' : ( $figure['type'] === 'month' ? 'month' : '' );

					if( $column )
					{
						$query->where( \DB::raw($column  . '(created_at)'), $figure[ 'integer' ] );
					}
				}
			}
		}

		return $query;
	}

	private function executeNotOnAfterBefore($expected, $dates, $query)
	{
		$params = array_keys($dates);

		$exactDateChecks = [];

		foreach ($expected as $expec)
		{
			if( in_array($expec, $params) )
			{
				if( $figure = $this->checkIfDateFigure( $dates[$expec][count($dates[$expec]) - 1 ] ) )
				{
					if( $figure['type'] === 'date' || $figure['type'] === 'day' )
					{
						$exactDateChecks[] = $figure['date'];
					}
					else
					{
						$query->whereBetween('created_at', [ $figure['date'] , date('Y-m-t', strtotime( $figure['date'] ) ) ] );
					}
				}
			}			
		}

		if( count($exactDateChecks) !== 0 )
		{
			if( count($exactDateChecks) === 1 )
			{
				$query->where(\DB::raw('date(created_at)'), $exactDateChecks[0]);
			}
			else
			{
				array_map(function($date)use(&$query){

					$query->orWhere(\DB::raw('date(created_at)'),$date);

				}, $exactDateChecks);
			}
		}

		return $query;
	}

	private function checkIfDateFigure($string)
	{
		$figure = \DateTime::createFromFormat('D', $string);

		if( $figure )
		{
			if( $figure->format('Y-m-d') != date('Y-m-d') ){
				$figure->modify('-1 week');
			}
			//to do that 1 is a dynamic value need to convert
			return ['date' => $figure->format('Y-m-d') , 'integer' => ( date('w', $figure->getTimestamp() ) + 1 ), 'type' => 'day' ];
		}
		else if( $figure = \DateTime::createFromFormat( 'M', $string ) )
		{
			return ['date' => $figure->format('Y-m-01') , 'integer' => ( date('m', $figure->getTimestamp() ) ) , 'type' => 'month' ];
		}
		else
		{
			if( $figure = \DateTime::createFromFormat('Y-m-d', $string) )
			{
				return ['date' => $string ,'integer' => false, 'type' => 'date'];
			}
			else
			{
				if( $figure = strtotime( $string ) )
				{
					if( date('Y-m',$figure) === date('Y-m') && time() > $figure )
					{
						return ['date' => date('Y-m-d',$figure), 'integer' => false, 'type' => 'date'];
					}
				}
			}
		}
	}
}