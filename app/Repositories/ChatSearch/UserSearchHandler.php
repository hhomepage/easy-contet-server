<?php namespace App\Repositories\ChatSearch;

use App\Repositories\ChatSearch\ChatSearchHandler;
use App\User;
use Auth;

class UserSearchHandler extends ChatSearchHandler
{
	protected $validParam = ['from' => [] , 'to' => []];

	protected function process($request, $query){

		if( $users = $this->isValid($request) )
		{
			if( isset( $users['to'] ) && is_array( $users['to'] ) && count($users['to']) !== 0 )
			{
				$query->whereIn('to', $users['to']);
			}

			if( isset( $users['from'] ) && is_array( $users['from'] ) && count($users['from']) !== 0)
			{
				$query->whereIn('author_id', $users['from']);
			}
		}
		else
		{
			if( !isset($request['channel']) )
			{
				$query->where(function($query)
				{
					$query->orWhere('author_id', $this->user->id)->orWhere('to', $this->user->id)->orWhereIn('channel_id', $this->user->myChannels());
				});
			}
		}

		return [$request, $query];
	}

	private function hasContainValidParams($params){

		$validParams = array_keys($this->validParam);

		$return = [];

		foreach($validParams as $param)
		{
			if( isset( $params[ $param ] ) )
			{
				$users = $params[ $param ];

				$users = is_array($users) ? $users : [ $users ];

				$return[ $param ] = $users;
			}
		}

		return count($return) !== 0 ? $return : false;
	}

	public function checkIfFromSameCompany($users, $companyUsers){
		
		$return = [];

		foreach($users as $user)
		{
			if( in_array($user, $companyUsers) )
			{
				$return[] = $user;
			}
		}

		return $return;
	}

	private function isValid($request)
	{
		if( isset( $request['user'] ) )
		{
			$allParams = $request['user'];

			if( $params = $this->hasContainValidParams( $allParams ) )
			{
				$companyUsers = $this->company->users->lists('id')->toArray();

				$_users = $this->validParam;

				foreach($params as $param => $users)
				{
					if( $users = $this->checkIfFromSameCompany( $users, $companyUsers) )
					{
						$_users[ $param ] = $users;
					}
				}

				if( !isset( $request['channel'] ) )
				{
					if( count( $_users['to'] ) !== 0 && count( $_users['from'] ) !== 0)
					{
						if( in_array( $this->user->id, $_users['from'] ) )
						{
							$_users['from'] = [ $this->user->id ];
						}
						else if(in_array( $this->user->id, $_users['to'] ) )
						{
							$_users['to'] = [ $this->user->id ];
						}
						else
						{
							$_users['from'] = [ $this->user->id ];
						}
					}
					else
					{
						if( count( $_users['to'] ) !== 0 )
						{
							$_users['from'] = [$this->user->id];
						}
						else
						{
							$_users['to'] = [$this->user->id];
						}
					}
				}

				return $_users;
			}
		}
	}
}