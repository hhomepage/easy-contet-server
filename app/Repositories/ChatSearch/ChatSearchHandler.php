<?php namespace App\Repositories\ChatSearch;
use Auth;

abstract class ChatSearchHandler
{	
	protected $user;
	protected $company;

	function __construct()
	{
		$this->user = Auth::user();
		$this->company = app('company');
	}

	private $successor = null;

	final public function setSuccessor(ChatSearchHandler $handler)
    {
        if ($this->successor === null) {
            $this->successor = $handler;
        } else {
            $this->successor->setSuccessor($handler);
        }
    }

    final public function handle( $request, $query )
    {
        list($request, $query) = $this->process( $request, $query );
        
        if ( $this->successor !== null ) {

             list($request, $query) = $this->successor->handle( $request, $query );
        }

        return [$request, $query];
    }

    abstract protected function process($request, $query);
}