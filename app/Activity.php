<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
class Activity extends Model
{
    protected $table = 'activities';

    public static function makeFromDirty($project, $thread, $dirty){

    	foreach( $dirty as $key => $value )
    	{
	    	$ac = new static();
	    	
	    	$ac->project_id = $project->id;
	    	
	    	$ac->thread_id = $thread->id;
	    	
	    	$ac->user_id = Auth::user()->id;

	    	$ac->key = $key;

	    	$ac->value = $value;

	    	$ac->action = 'updated';

	    	$ac->save();
    	}
    }

    public static function makeFromRel($project, $thread, $action, $key, $value)
    {
    	$ac = new static();
	    	
	    $ac->project_id = $project->id;
	    	
	    $ac->thread_id = $thread->id;
	    	
	    $ac->user_id = Auth::user()->id;

	    $ac->key = $key;

    	$ac->value = $value;

    	$ac->action = $action;

    	$ac->save();
    }
}