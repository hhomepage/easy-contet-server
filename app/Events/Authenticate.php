<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class Authenticate extends Event Implements ShouldBroadcast
{
    use SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */

    protected $data;
    protected $channel;
    protected $event;

    public function __construct( $data, $as = 'auth')
    {
        $this->data = $data;
        $this->channel = 'AUTH';
        $this->event = $as;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [ $this->channel ];
    }

    public function broadcastWith(){
        return $this->data;
    }

    public function broadcastAs(){
        return $this->event;
    }
}
