<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Channel extends Model
{
    protected $table = 'channels';

    protected $fillable = ['name','description'];

    public function creator(){
    	return $this->belongsTo('App\User','user_id','id');
    }

    public static function findChannel($id)
    {
        return self::where('id', $id)->where('company_id', app('company')->id)->first();
    }

    public function isOwner($user)
    {
        return $this->company_id === app('company')->id && $this->user_id === $user->id;
    }

    public function users(){
    	return $this->belongsToMany('App\User','channel_users','channel_id','user_id')->withTimestamps();
    }

    public function userIds(){
        return $this->belongsToMany('App\User','channel_users','channel_id','user_id')->withTimestamps()->select('user_id');
    }

    public function scopePrivate($query){
    	return $query->wherePrivate(1);
    }

    public function scopePublic($query){
    	return $query->wherePrivate(0);
    }

    public function loadUserIds()
    {
        $channel = $this->toArray();

        $channel['user_ids'] = $this->userIds->lists('user_id');

        unset($channel['users']);

        return $channel;
    }

    public function listNonAssignedUser($users)
    {
        $currentUsers = $this->from('channel_users')->whereIn('user_id', $users)->where('channel_id',$this->id )->select('user_id')->get('user_id')->toArray();

        return array_values(array_diff($users, $currentUsers));
    }

    public function belongs($user){
        return in_array($user->id, $this->users->lists('id')->toArray());
    }

    public function revokeUsers($id){
        $query = $this->newQueryWithoutScopes()->from('channel_users');
        return $query->where('channel_id',$this->id)->where('user_id',$id)->delete();
    }

    public function assignUsers($ids = [])
    {
    	$this->users()->syncWithoutDetaching( $ids );
    }

    public function isPrivate(){
        return $this->private == 1;
    }   

    public function isPublic(){
        return $this->private == 0;
    }

    public static function areMine( $channels, $user, $company)
    {
        $query = self::from('channel_users')->where('user_id', $user->id);
    
        $query = $query->whereIn('channel_id', $channels);

        $channelIds = $query->lists('channel_id');

        if( !$channelIds->isEmpty() )
        {
            return self::whereIn('id', $channelIds->toArray())->where('company_id', $company->id)->lists('id')->toArray();
        }
    }
}