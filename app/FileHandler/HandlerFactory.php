<?php
/**
 * Created by PhpStorm.
 * User: sishir
 * Date: 3/18/16
 * Time: 11:34 PM
 */

namespace App\FileHandler;


class HandlerFactory
{
    public static $mimes = [
        'video' => VideoHandler::class,
        'image' => ImageHandler::class,
        'pdf' => PdfHandler::class,
        'audio' => AudioHandler::class
    ];

    public static function make($mime)
    {
        foreach(self::$mimes as $real => $handler)
        {
            if(is_string( $mime) && preg_match( '|'. $real.'|', $mime ) )
            {
                return new $handler;
            }
        }
    }
}