<?php
namespace App\FileHandler;
/**
 * Created by PhpStorm.
 * User: sishir
 * Date: 3/18/16
 * Time: 10:59 PM
 */
use Storage;
use File;
use App\Repos\Interfaces\Resource as ResourceInterface;
use Res;
use Illuminate\Support\Str;

abstract class HandlerContact
{
    protected $repo;

    public function setRepository(ResourceInterface $repo)
    {
        $this->repo = $repo;
        return $this;
    }

    public function handle( $file )
    {
        $ext = $file->getClientOriginalExtension();

        $fileNameFirst = self::getOrgName($file);

        $clientName = $fileNameFirst .'.'. $ext;

        $nameWithoutExt = ( preg_replace( '/[%]+/','', Str::ascii( $fileNameFirst ) ) );

        $name =  $nameWithoutExt. '.' . $ext;

        Storage::disk('local')->put( $name,  File::get( $file ) );

        $imageData['name'] = $name;

        return $imageData;
    }

    public static function getOrgName($file)
    {
        $ext = $file->getClientOriginalExtension();
        $name = $file->getClientOriginalName();
        $nameWithoutExt = str_replace('.' . $ext, '', $name);
        return $nameWithoutExt;
    }

    public static function getThumb($image)
    {
        return asset('uploads/'.$image);
    }

    abstract public function afterUpload( $path, $name ,$file);

    public static function render($name)
    {
        if( Storage::disk('local')->exists( $name ) )
        {
            return response()->download( storage_path('app') .'/'.$name , $name);
        }

        return Res::fail([],'Not Found',NOT_FOUND);
    }

    public static function exists($name)
    {
        return Storage::disk('local')->exists($name);
    }

    public static function delete($names)
    {
        foreach($names as $name)
        {
            if( self::exists($name))
            {
                Storage::disk('local')->delete( $name );
            }
        }
    }

    public static function getDownloadPath( $name )
    {
        if( self::exists( $name ) )
        {
            return url('file/download/' . $name);
        }
    }
}