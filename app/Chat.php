<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// use App\Traits\TimeFormatterTrait;

class Chat extends Model
{
    // use TimeFormatterTrait;

    protected $table = 'chats';

    protected $fillable = ['message'];


    public function author(){
    	return $this->belongsTo('App\User','author_id','id');
    }

    public function company(){
    	return $this->belongsTo('App\Company', 'company_id', 'id');
    }

    public function isAssociatedWithUser($user)
    {
        if( $this->company_id === app('company')->id )
        {
            if( $this->author_id === $user->id || $this->to === $user->id || $this->isFromChannel($user) )
            {
                return true;
            }
        }
    }


    public function isFromChannel($user)
    {
        if( $this->isChannel() && $this->channel && $this->channel->belongs($user) )
        {
            return true;
        }
    }

    public static function markAsRead($from, $to, $company)
    {
        if( $from )
        {
            $chats = self::where('author_id', $from->id)->where('to', $to->id)->where('type', 1)->where('read', 0)->where('company_id', $company->id)->first();

            if(  $chats )
            {
                if( self::whereIn('id', $chats->lists('id')->toArray())->update(['read' => 1]) )
                {
                    return $chats->lists('id');
                }
            }
        }
    }


    public function channel(){
    	return $this->belongsTo('App\Channel', 'channel_id', 'id');
    }

    public function receiver(){
    	return $this->belongsTo('App\User','to','id');
    }

    public function scopeByPrivate($query){
    	return $query->whereType(1);
    }

    public function scopeByChannel($query){
    	return $query->whereType(0);
    }

    public function getType(){
        return $this->isChannel() ? 'channel' : 'direct';
    }

    public function isChannel(){
        return !$this->type;
    }

    public function resources()
    {
        return $this->morphMany('App\Resource','imagable');
    }

    public function getResourceIds(){
        return $this->resources->lists('id')->toArray();
    }

    public function isOwner($user)
    {
        return $this->author_id === $user->id && $this->company_id === app('company')->id;
    }

    public function getInvolvedEntity($user){
        return $this->isChannel() ? $this->channel_id : ( $this->author_id === $user->id ? $this->to : $this->author_id );
    }
}
