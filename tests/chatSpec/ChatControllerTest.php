<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\User;
use App\Traits\TestHelperTrait as MyTrait;

class FileManagerRepo extends TestCase
{
	use Illuminate\Foundation\Testing\DatabaseTransactions;

	use MyTrait;

	public $repo;
	public $session;

	public function setUp()
	{
		parent::setUp();
		$this->actAs(1);
	}

	public function test_hello_world(){
		$this->assertTrue(true);
	}


	public function test_me(){
		$this->assertEquals(1,$this->callGET( 'me' )['id']);
	}


	 public function test_channel_add()
	 {
	 	$this->callPOST('admin/channel',[],[],422);

	 	$channel = $this->callPOST('admin/channel',['name' => 'Programming','description' => 'Talks only of program']);

	 	$this->assertArrayHasKey('id',$channel);

	 	$channelId = $channel['id'];

	 	$editedChannel = $this->callPOST('admin/channel/' . $channelId,['name' => 'Programming and designing', 'description' => 'Tasad aadasdas dasasda']);
	}

//'channel' => ['in' => 43 ] 
	public function test_search()
	{
		$res = $this->callPOST('chat/search',['date' => ['before' => ['Yesterday']]],[]);

		print_r($res);
	}
}