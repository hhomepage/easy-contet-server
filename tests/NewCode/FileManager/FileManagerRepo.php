<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Repositories\ResourceRepo as Repo;
use Modules\Project\Repositories\ProjectRepo;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use App\User;

class FileManagerRepo extends TestCase
{
	use Illuminate\Foundation\Testing\DatabaseTransactions;

	public $repo;
	public $session;

	public function setUp()
	{
		parent::setUp();
		exec('rm -rf ' . storage_path() . '/app/company/*');
		exec('rm -rf ' . public_path() . '/uploads/*');

		$this->repo = new Repo;

		$this->session = ['company' => 
			json_encode([
				'id' => 1,
				'unique' => 'abc-traders'
			])
		];

		$this->be( User::find(46) );
		
	}

	public function makeFile($name)
	{
		$stub = '/var/www/html/content/tests/files/' . $name;
                
        $path = sys_get_temp_dir() . '/' .$name;
        
        copy( $stub, $path );

        $file = new UploadedFile( $path, $name, filesize($path), 'image/png', null, true);

        return $file;
	}

	public function testAfterFileUpload()
	{
		$file = $this->makeFile('Abbott.png');
        
        $response = $this
        	->withSession( $this->session )
        	->call('POST', '/api/v1/upload', ['path' => 'testing'], [], [ 'uploaded_file' => $file ], [ 'HTTP_X-Requested-With' => 'XMLHttpRequest' ]);

       	$this->assertResponseStatus(422);

        $response = $this
        	->withSession( $this->session )
        	->call('POST', '/api/v1/upload', ['path' => 'my-project/newfolder'], [], [ 'uploaded_file' => $file ], [ 'HTTP_X-Requested-With' => 'XMLHttpRequest' ]);
       		
       	$this->assertResponseStatus(200);

        
        $this->be( User::find(32) );

        $response = $this
        	->withSession( $this->session )
        	->call('POST', '/api/v1/upload/posts', [], [], [ 'uploaded_file' => $file ], ['HTTP_X-Requested-With' => 'XMLHttpRequest']);

        $this->assertResponseOk();

        $response = $response->getContent();

        $response = json_decode($response, true)['data'];

        $this->assertTrue( Storage::exists( $response['path'] ) );

        $instance = $this->repo->getInstane( $response['id'] );

        $this->assertNotNull( $instance );

        $this->assertEquals('Abbott.png', $response['client_name']);

        $this->assertTrue( Storage::disk('public')->exists( $response['name'] ) );

        $file = $this->makeFile('test.php');
        
        $response = $this
        	->withSession( $this->session )
        	->call('POST', '/api/v1/upload', ['path' => 'testing'], [], [ 'uploaded_file' => $file ], [ 'Accept' => 'application/json' ]);

        $response = $response->getContent();

        $response = json_decode( $response, true )['data'];

        $this->assertTrue( Storage::exists( $response['path'] ) );

        $this->assertFalse( Storage::disk('public')->exists( $response['name'] ) );
        
        $response = $this
        	->withSession( $this->session )
        	->call('POST', '/api/v1/upload', [ 'path' => 'testing/../../' ], [  ], [ 'uploaded_file' => $file ], [ 'Accept' => 'application/json' ] );
    	
        $this->assertResponseStatus(403);


	}

	public function test_make_directory()
	{
		$this->be( User::find(46) );

		$response = $this
			->withSession($this->session)
			->call('POST','/api/v1/resource/create', [ 'name' => 'New Folder', 'path' => 'my-project' ] );

		$this->assertResponseStatus(200);

		$this->assertTrue(File::isDirectory( storage_path('app/company/abc-traders/my-project/New Folder') ));
	}

	public function test_slug_project_belongs_user()
	{
		$repo = new ProjectRepo;
		$this->assertTrue($repo->userBelongsBySlug( 46, 'my-project' ) );
	}


	public function test_resource_delete_request()
	{
		$this->be( User::find(46) );

		$response = $this
			->withSession($this->session)
			->call('POST','/api/v1/resource/create', [ 'name' => 'New Folder', 'path' => 'my-project' ] );

		$resource = json_decode($response->getContent(), true)['data'];

		$this->assertResponseOk();

		$response = $this
			->withSession($this->session)
			->call('POST','/api/v1/resource/delete/'.$resource['id'], [ 'path' => 'my-project/New Folder' ] );

		$this->assertResponseStatus(200);
	}

	public function test_get_all_resource(){
		
		$this->be( User::find(46) );

		$file = $this->makeFile('Abbott.png');

		$response = $this
			->withSession($this->session)
			->call('POST','/api/v1/resource/create', [ 'name' => 'New Folder', 'path' => 'my-project' ] );


		$response = $this
        	->withSession( $this->session )
        	->call('POST', '/api/v1/upload', ['path' => '/'], [], [ 'uploaded_file' => $file ], [ 'HTTP_X-Requested-With' => 'XMLHttpRequest' ]);

       	$this->assertResponseStatus(200);


        $response = $this
        	->withSession( $this->session )
        	->call('POST', '/api/v1/upload', ['path' => 'my-project/New Folder'], [], [ 'uploaded_file' => $file ], [ 'HTTP_X-Requested-With' => 'XMLHttpRequest' ]);
       		
       	$this->assertResponseStatus(200);

		$response = $this->withSession($this->session)
			->call('POST','/api/v1/resource/crawl',['path' => '/'],[],[],['HTTP_X-Requested-With' => 'XMLHttpRequest']);
		
		$response = json_decode($response->getContent(), true)['data'];

		print_r($response);
	}
}