<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Company;
use Modules\UserManagement\Entities\Invitation;
use Modules\Project\Entities\Project;
use App\User;
use Modules\Threads\Entities\Thread;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ProjectManagementRoutesTest extends TestCase
{
	use Illuminate\Foundation\Testing\DatabaseTransactions;

	protected $session;

	public function setUp()
	{
		parent::setUp();

		$this->session = ['company' => 
			json_encode([
				'id' => 1,
				'unique' => 'abc-traders'
			])
		];

		$this->beAdmin();
	}

	public function actAs( $id ){
		$user = User::find($id);
		$this->be($user);
		return $user;
	}

	public function beDeveloper(){
		return $this->actAs( 55 );
	}

	public function beAdmin(){
		return $this->actAs( 1 );
	}

	public function beClient(){
		return $this->actAs( 52 );
	}

	public function beDifferentUser(){
		return $this->actAs( 51 );
	}



	public function test_crud_project_status()
	{
		$response = $this->withSession($this->buildSession())
			->call('POST','/api/v1/project/admin',[],[],[], ['HTTP_X-Requested-With' => 'XMLHttpRequest']);

		$this->assertResponseStatus(422);

		$data = $this->parseResponse($response, true);

		$this->assertArrayHasKey('title', $data);

		$user = $this->beDeveloper();

		$response = $this->withSession($this->buildSession())
			->call('POST','/api/v1/project/admin',['title' => 'asdsad', 'description' => 'asdsad asd asdasd'],[],[], ['HTTP_X-Requested-With' => 'XMLHttpRequest']);

		$this->assertResponseStatus(403);

		$this->beAdmin();
		
		$response = $this->withSession($this->buildSession())
			->call('POST','/api/v1/project/admin',['title' => 'New Project', 'description' => 'asdsad asd asdasd'],[],[], ['HTTP_X-Requested-With' => 'XMLHttpRequest']);

		$this->assertResponseStatus(200);


		$p1  = $this->parseResponse($response, true);

		$this->assertEquals('New Project', $p1['title']);

		$response = $this->withSession($this->buildSession())
			->call('POST','/api/v1/project/admin',['title' => 'Next Project Project', 'description' => 'asdsad asd asdasd'],[],[], ['HTTP_X-Requested-With' => 'XMLHttpRequest']);

		$this->assertResponseStatus(200);

		$p2 = $this->parseResponse($response, true);

		$response = $this->withSession($this->buildSession())
			->call('POST','/api/v1/project/admin/'.$p2['id'], ['title' => 'New Project Is Edited', 'description' => 'asdsad asd asdasd'],[],[], ['HTTP_X-Requested-With' => 'XMLHttpRequest']);

		$this->assertResponseStatus(200);

		$_p2 = $this->parseResponse($response, true);

		$this->assertEquals('New Project Is Edited', $_p2['title']);

		$this->beDifferentUser();

		$response = $this->withSession($this->buildSession(2))
			->call('POST','/api/v1/project/admin/'.$p2['id'], ['title' => 'New Project Is Edited', 'description' => 'asdsad asd asdasd'],[],[], ['HTTP_X-Requested-With' => 'XMLHttpRequest']);

		$this->assertResponseStatus(403);

		$this->beAdmin();

		$response = $this->withSession($this->buildSession(1))
			->call('DELETE','/api/v1/project/admin/'.$p2['id']);

		$this->assertResponseStatus(200);
		
		$_p2 = $this->parseResponse($response, true);

		$this->assertEquals(0, $_p2['status']);

		$response = $this->withSession($this->buildSession(1))
			->call('DELETE','/api/v1/project/admin/'.$p2['id'].'/forever');

		$this->assertResponseStatus(200);

		$this->assertNull(Project::find($p2['id']));
	}


	public function test_other_scrumboard_request()
	{
		$ids = $this->buildFakeProjects();

		$this->withSession($this->buildSession())
			->call('POST','/api/v1/project/admin/'. $ids[0]. '/board',[],[],[],['HTTP_X-Requested-With' => 'XMLHttpRequest']);

		$this->assertResponseStatus(422);

		$response = $this->withSession($this->buildSession())
			->call('POST','/api/v1/project/admin/'. $ids[0]. '/board',['name'=>'Bug Fixing'],[],[],['HTTP_X-Requested-With' => 'XMLHttpRequest']);

		$this->assertResponseStatus(200);

		$response = $this->parseResponse($response, true);

		$this->assertEquals('Bug Fixing', $response['name']);

		$response = $this->withSession($this->buildSession())
			->call('POST','/api/v1/project/admin/'. $ids[0]. '/board/'.$response['id'],['name'=>'Bug Fixing Edited'],[],[],['HTTP_X-Requested-With' => 'XMLHttpRequest']);

		$this->assertResponseStatus(200);

		$response = $this->parseResponse($response, true);

		$this->assertEquals('Bug Fixing Edited', $response['name']);

		$this->withSession($this->buildSession())
			->call('DELETE','/api/v1/project/admin/'. $ids[1]. '/board/'.$response['id'],['name'=>'Bug Fixing Edited'],[],[],['HTTP_X-Requested-With' => 'XMLHttpRequest']);

		$this->assertResponseStatus(400);

		$response = $this->withSession($this->buildSession())
			->call('DELETE','/api/v1/project/admin/'. $ids[0]. '/board/'.$response['id'],['name'=>'Bug Fixing Edited'],[],[],['HTTP_X-Requested-With' => 'XMLHttpRequest']);

		$this->assertResponseStatus(200);

		$response = $this->parseResponse($response, true);

		$this->assertEquals(0, $response['status']);
	}

	public function test_thread_requetss()
	{
		$ids = $this->buildFakeProjects();

		$boardIds = array_column($this->buildFakeBoard( $ids ), 'id');

		$this->withSession($this->buildSession())
			->call('POST','/api/v1/project/admin/'. $ids[0]. '/board/'. $boardIds[0] . '/thread',[],[],[],['HTTP_X-Requested-With' => 'XMLHttpRequest']);
		$this->assertResponseStatus(422);

		$response = $this->withSession($this->buildSession())
			->call('POST','/api/v1/project/admin/'. $ids[0]. '/board/'. $boardIds[0] . '/thread',['title'=>'Please finish this sson','description'=>'asdas sad'],[],[],['HTTP_X-Requested-With' => 'XMLHttpRequest']);

		$response = $this->parseResponse($response, true);

		$this->assertResponseStatus(200);

		$this->assertEquals('Please finish this sson', $response['title']);

		$this->assertEquals(1, $response['status_n']);

		$response = $this->withSession($this->buildSession())
			->call('POST','/api/v1/project/admin/'. $ids[0]. '/board/'. $boardIds[0] . '/thread/'. $response['id'], ['title'=>'Please finish this soon','description'=>'asdas sad'],[  ],[  ],[ 'HTTP_X-Requested-With' => 'XMLHttpRequest'] );

		$this->assertResponseStatus(200);

		$response = $this->parseResponse($response, true);

		$this->assertEquals('Please finish this soon', $response['title']);

		$response = $this->withSession($this->buildSession())
			->call('DELETE','/api/v1/project/admin/'. $ids[0]. '/board/'. $boardIds[0] . '/thread/'. $response['id']);

		$this->assertResponseStatus(200);

		$response = $this->parseResponse($response, true);
		
		$this->assertEquals(0, $response['status_n']);

	}

	public function buildFakeThreads()
	{
		$projects = $this->buildFakeProjects();

		$boards = array_column($this->buildFakeBoard($projects),'id');

		$response = $this->withSession($this->buildSession())
			->call('POST','/api/v1/project/admin/'. $projects[0]. '/board/'. $boards[0] . '/thread',['title'=>'Urgent Bugs are risigng','description'=>'asdas sad'],[],[],['HTTP_X-Requested-With' => 'XMLHttpRequest']);
		
		$_threads[] = $this->parseResponse($response,true);

		$this->assertResponseStatus(200);

		$response = $this->withSession($this->buildSession())
			->call('POST','/api/v1/project/admin/'. $projects[1]. '/board/'. $boards[1] . '/thread',['title'=>'Please finish this sson','	description'=>'asdas sad'],[],[],['HTTP_X-Requested-With' => 'XMLHttpRequest']);

		$this->assertResponseStatus(200);
		$_threads[] = $this->parseResponse($response,true);

		$response = $this->withSession($this->buildSession())
			->call('POST','/api/v1/project/admin/'. $projects[1]. '/board/'. $boards[1] . '/thread',['title'=>'Do this quick','description'=>'asdas sad'],[],[],['HTTP_X-Requested-With' => 'XMLHttpRequest']);

		$this->assertResponseStatus(200);
		$_threads[] = $this->parseResponse($response,true);

		return $_threads;

	}

	public function buildFakeBoard($ids = [])
	{
		$response1 = $this->withSession($this->buildSession())
			->call('POST','/api/v1/project/admin/'. $ids[0]. '/board',['name'=>'Bug Fixing'],[],[],['HTTP_X-Requested-With' => 'XMLHttpRequest']);

		$this->assertResponseStatus(200);

		$response2 = $this->withSession($this->buildSession())
			->call('POST','/api/v1/project/admin/'. $ids[1]. '/board',['name'=>'New Issue'],[],[],['HTTP_X-Requested-With' => 'XMLHttpRequest']);

		$this->assertResponseStatus(200);

		$response3 = $this->withSession($this->buildSession())
			->call('POST','/api/v1/project/admin/'. $ids[2]. '/board',['name'=>'Test'],[],[],['HTTP_X-Requested-With' => 'XMLHttpRequest']);
		$this->assertResponseStatus(200);

		$boards = [ $this->parseResponse($response1, true), $this->parseResponse($response2, true), $this->parseResponse($response3, true)];

		return $boards;

	}


	public function buildFakeProjects()
	{
		$response = $this->withSession($this->buildSession())
			->call('POST','/api/v1/project/admin',['title' => 'New Project', 'description' => 'asdsad asd asdasd'],[],[], ['HTTP_X-Requested-With' => 'XMLHttpRequest']);

		$this->assertResponseStatus(200);

		$_response[] = $this->parseResponse($response, true);

		$response1 = $this->withSession($this->buildSession())
			->call('POST','/api/v1/project/admin',['title' => 'Next Project', 'description' => 'asdsad asd asdasd'],[],[], ['HTTP_X-Requested-With' => 'XMLHttpRequest']);

		$this->assertResponseStatus(200);
		$_response[] = $this->parseResponse($response1, true);

		$response2 = $this->withSession($this->buildSession())

			->call('POST','/api/v1/project/admin',['title' => 'New Next Project', 'description' => 'asdsad asd asdasd'],[],[], ['HTTP_X-Requested-With' => 'XMLHttpRequest']);
		$this->assertResponseStatus(200);
		
		$_response[] = $this->parseResponse($response2, true);

		return array_column($_response,'id');
	}



	public function test_user_assign_projects(){
		
		$projects = $this->buildFakeProjects();

		$user  = $this->beDifferentUser();

		$response = $this
			->withSession($this->buildSession())
			->call('POST','/api/v1/project/admin/'.$projects[0] . '/assign/user/' . $user->id);

		$this->assertResponseStatus(403);

		$user = $this->beDeveloper();

		$this->beAdmin();

		$response = $this
			->withSession($this->buildSession())
			->call('POST','/api/v1/project/admin/'.$projects[0] . '/assign/user/'.$user->id);

		$this->assertResponseOk();

		$project = Project::find($projects[0]);

		$this->assertTrue($project->belongs($user));

		$response = $this
			->withSession($this->buildSession())
			->call('DELETE','/api/v1/project/admin/'.$projects[0] . '/revoke/user/' . $user->id);

		$this->assertResponseOk();

		$user = $this->beDeveloper();

		$project = Project::find($projects[0]);

		$this->assertFalse($project->belongs($user));
	}


	public function test_user_assign_threads()
	{
		$threads = $this->buildFakeThreads()[0];

		$user  = $this->beDifferentUser();

		$response = $this
			->withSession($this->buildSession())
			->call('POST','/api/v1/project/admin/'.$threads['project_id'] . '/thread/'.$threads['id'].'/assign/user/' . $user->id);
		$this->assertResponseStatus(403);

		$user = $this->beDeveloper();

		$response = $this
			->withSession($this->buildSession())
			->call('POST','/api/v1/project/admin/'.$threads['project_id'] . '/thread/'.$threads['id'].'/assign/user/' . $user->id);
		
		$this->assertResponseStatus(403);

		$this->beAdmin();

		$response = $this
			->withSession($this->buildSession())
			->call('POST','/api/v1/project/admin/'.$threads['project_id'] . '/assign/user/' . $user->id);

		$this->assertResponseOk();

		$response = $this
			->withSession($this->buildSession())
			->call('POST','/api/v1/project/admin/'.$threads['project_id'] . '/thread/'.$threads['id'].'/assign/user/' . $user->id);
		

		$this->assertResponseStatus(200);
		
		$thread = Thread::find($threads['id']);

		$this->assertTrue($thread->isAssigned($user));

		$response = $this
			->withSession($this->buildSession())
			->call('DELETE','/api/v1/project/admin/'.$threads['project_id'] . '/thread/'.$threads['id'].'/revoke/user/' . $user->id);

		$this->assertResponseOk();
		
		$thread = Thread::find($threads['id']);

		$this->assertFalse($thread->isAssigned($user));

	}

	public function test_page_routes(){

		$projects = $this->buildFakeProjects();

		$response = $this->withSession($this->buildSession())
			->call('POST','/api/v1/project/admin/'.$projects[0].'/page',['title' => 'New Project', 'description' => 'asdsad asd asdasd'],[],[], ['HTTP_X-Requested-With' => 'XMLHttpRequest']);

		$this->assertResponseStatus(200);
	
		$page = $this->parseResponse($response, true);

		$this->assertEquals('New Project', $page['title']);

		$response = $this->withSession($this->buildSession())
			->call('POST','/api/v1/project/admin/'.$projects[0].'/page/'. $page['id'],['title' => 'New Project Edit', 'description' => 'asdsad asd asdasd'],[],[], ['HTTP_X-Requested-With' => 'XMLHttpRequest']);

		$page = $this->parseResponse($response, true);

		$this->assertEquals( 'New Project Edit', $page['title'] );
	}


	public function test_add_checklist_in_thread(){
		
		$threads = $this->buildFakeThreads();

		$thread = $threads[0];

		$user = $this->beDeveloper();
		$this->beAdmin();

		$this
			->withSession($this->buildSession())
			->call('POST','/api/v1/project/admin/'.$thread['project_id'] . '/assign/user/' . $user->id);
		
		$this
			->withSession($this->buildSession())
			->call('POST','/api/v1/project/admin/'.$thread['project_id'] . '/thread/'.$thread['id'].'/assign/user/' . $user->id);

		$this->assertResponseOk();

		$response = $this
				->withSession($this->buildSession())
				->call('POST', '/api/v1/project/'. $thread['project_id'] . '/thread/'. $thread['id'] . '/checklist',[],[],[],['HTTP_X-Requested-With' => 'XMLHttpRequest']);

		$this->assertResponseStatus(422);

		$this->beDifferentUser();

		$response = $this
				->withSession($this->buildSession())
				->call('POST', '/api/v1/project/'. $thread['project_id'] . '/thread/'. $thread['id'] . '/checklist',[],[],[],['HTTP_X-Requested-With' => 'XMLHttpRequest']);

		$this->assertResponseStatus(403);

		$this->beDeveloper();

		$response = $this
				->withSession($this->buildSession())
				->call('POST', '/api/v1/project/'. $thread['project_id'] . '/thread/'. $thread['id'] . '/checklist',['name'=>'Break Down Task'],[],[],['HTTP_X-Requested-With' => 'XMLHttpRequest']);

		$name = $this->parseResponse($response, true);

		$this->assertArrayHasKey('name',$name);

		$this->assertEquals('Break Down Task', $name['name']);

		$this->assertResponseStatus(200);

		// $this->beAdmin();

		// $response = $this
		// 		->withSession($this->buildSession())
		// 		->call('POST', '/api/v1/project/'. $thread['project_id'] . '/thread/'. $thread['id'] .'/checklist/'. $name['id'] ,['name'=>'Break Down Task','only_me' => 1],[],[],['HTTP_X-Requested-With' => 'XMLHttpRequest']);

		// $this->assertResponseStatus(403);

		$this->beDeveloper();

		$response = $this
				->withSession($this->buildSession())
				->call('POST', '/api/v1/project/'. $thread['project_id'] . '/thread/'. $thread['id'] .'/checklist/'. $name['id'] ,['name'=>'Break Down Task','only_me' => 1],[],[],['HTTP_X-Requested-With' => 'XMLHttpRequest']);

		$this->assertResponseStatus(200);


		$data = $this->parseResponse($response, true);

		$this->assertEquals(1, $data['only_me']);

		$response = $this
			->withSession($this->buildSession())
			->call('POST', '/api/v1/project/'. $thread['project_id'] . '/thread/'. $thread['id'] .'/checklist/'. $data['id'] . '/checkitem' ,['name'=>'Break Down Task','only_me' => 1],[],[],['HTTP_X-Requested-With' => 'XMLHttpRequest']);

		$this->assertResponseStatus(422);

		// $this->beAdmin();

		// $response = $this
		// 	->withSession($this->buildSession())
		// 	->call('POST', '/api/v1/project/'. $thread['project_id'] . '/thread/'. $thread['id'] .'/checklist/'. $data['id'] . '/checkitem' ,['text'=>'First Task'],[],[],['HTTP_X-Requested-With' => 'XMLHttpRequest']);

		// $this->assertResponseStatus(403);

		$this->beDeveloper();

		$response = $this
			->withSession($this->buildSession())
			->call('POST', '/api/v1/project/'. $thread['project_id'] . '/thread/'. $thread['id'] .'/checklist/'. $data['id'] . '/checkitem' ,['text'=>'First Task'],[],[],['HTTP_X-Requested-With' => 'XMLHttpRequest']);


		$this->assertResponseStatus(200);

		$checkitem = $this->parseResponse($response, true);

		$this->assertEquals('First Task', $checkitem['text']);

		$response = $this
			->withSession($this->buildSession())
			->call('POST', '/api/v1/project/'. $thread['project_id'] . '/thread/'. $thread['id'] .'/checklist/'. $data['id'] . '/checkitem/'.$checkitem['id'] ,['text'=>'Edited'],[],[],['HTTP_X-Requested-With' => 'XMLHttpRequest']);

		$this->assertResponseStatus(200);

		$_checkitem = $this->parseResponse($response, true);

		$this->assertEquals('Edited', $_checkitem['text']);
		$this->assertEquals($checkitem['id'], $_checkitem['id']);
		$this->assertEquals(0, $_checkitem['checked']);

		$response = $this
			->withSession($this->buildSession())
			->call('POST', '/api/v1/project/'. $thread['project_id'] . '/thread/'. $thread['id'] .'/checklist/'. $data['id'] . '/checkitem/'.$checkitem['id'].'/toogle');

		$this->assertResponseStatus(200);

		$response = $this->parseResponse($response, true);

		$this->assertEquals(1, $response['checked']);

		$response = $this
				->withSession($this->buildSession())
				->call('POST', '/api/v1/project/'. $thread['project_id'] . '/thread/'. $thread['id'] .'/checklist/'. $name['id'] ,['name'=>'Break Down Task','only_me' => 0],[],[],['HTTP_X-Requested-With' => 'XMLHttpRequest']);

		$this->assertResponseStatus(200);

		$this->beAdmin();

		$response = $this
			->withSession($this->buildSession())
			->call('POST', '/api/v1/project/'. $thread['project_id'] . '/thread/'. $thread['id'] .'/checklist/'. $data['id'] . '/checkitem/'.$checkitem['id'].'/toogle');

		$this->assertResponseStatus(200);

		$response = $this->parseResponse($response, true);

		$this->assertEquals(0, $response['checked']);

		$response = $this
			->withSession($this->buildSession())
			->call('POST', '/api/v1/project/'. $thread['project_id'] . '/thread/'. $thread['id'] .'/checklist/'. $data['id'] . '/checkitem/'.$checkitem['id'].'/toogle');

		$this->assertResponseStatus(200);

		$response = $this->parseResponse($response, true);

		$this->assertEquals(1, $response['checked']);

		$response = $this
			->withSession( $this->buildSession() )
			->call('DELETE', '/api/v1/project/'. $thread['project_id'] . '/thread/'. $thread['id'] .'/checklist/'. $data['id'] . '/checkitem/'.$checkitem['id']);

		$this->assertResponseStatus(403);

		$this->beDeveloper();

		$response = $this
			->withSession( $this->buildSession() )
			->call('DELETE', '/api/v1/project/'. $thread['project_id'] . '/thread/'. $thread['id'] .'/checklist/'. $data['id'] . '/checkitem/'.$checkitem['id']);

		$this->assertResponseStatus(200);

		$response = $this
			->withSession($this->buildSession())
			->call('DELETE', '/api/v1/project/'. $thread['project_id'] . '/thread/'. $thread['id'] .'/checklist/'. $data['id']);

		$this->assertResponseStatus(200);

	}

	public function test_commenting_on_thread(){

		$threads = $this->buildFakeThreads();

		$thread = $threads[0];

		$this
			->withSession($this->buildSession())
			->call('POST', '/api/v1/comment',[],[],[],['HTTP_X-Requested-With' => 'XMLHttpRequest']);

		$this->assertResponseStatus(422);

		$response = $this
			->withSession($this->buildSession())
			->call('POST', '/api/v1/comment',[ 'text' => 'Hello I am Comment'],[],[],['HTTP_X-Requested-With' => 'XMLHttpRequest']);

		$this->assertResponseStatus(200);

		$comment = $this->parseResponse($response, true);

		$this->assertEquals('Hello I am Comment', $comment['text']);

		$response = $this
			->withSession($this->buildSession())
			->call('POST', '/api/v1/comment/'.$comment['id'],[ 'text' => 'Hello I am Comment *'],[],[],['HTTP_X-Requested-With' => 'XMLHttpRequest']);

		$this->assertResponseStatus(200);

		$comment = $this->parseResponse($response, true);

		$this->assertEquals('Hello I am Comment *', $comment['text']);

		$response = $this
			->withSession($this->buildSession())
			->call('DELETE', '/api/v1/comment/'.$comment['id']);

		$this->assertResponseStatus(200);

		$comments[] = $this->callPOST('comment',['text' => 'My First Comment']);
		$comments[] = $this->callPOST('comment',['text' => 'My second Comment']);
		$comments[] = $this->callPOST('comment',['text' => 'My third Comment']);

		$this->callPOST('project/' . $thread['project_id'] . '/thread/'. $thread['id'] . '/comment/' . $comments[0]['id']);

		$_comment = App\Comment::find($comments[0]['id']);

		$this->assertNotNull($_comment);

		$this->assertEquals($thread['id'] , $_comment->commentable_id );

		$this->callDELETE('comment/' . $_comment->id);

		$_comment = App\Comment::find($comments[0]['id']);

		$this->assertNull($_comment);
	}


	public function test_labels(){
		
		$threads = $this->buildFakeThreads();

		$thread = $threads[0];

		$this->callPOST('project/label',[],[], 422);
		
		$res = $this->callPOST('project/label',['name'=> 'bug', 'color'=>'red']);

		$this->assertEquals('bug', $res['name'] );

		$this->callPOST('project/label',['name' =>'Bug'],[],403, 2);

		$res = $this->callPOST('project/label/' . $res['id'],['name' =>'Bug']);

		$this->assertEquals('Bug', $res['name'] );

		$this->callDELETE('project/label/' . $res['id'] );

		$label = $this->callPOST('project/label',['name'=> 'bug', 'color'=>'red']);

		$this->callPOST('project/' . $thread['project_id'] . '/thread/' . $thread['id'] .'/label/'. $label['id'] . '/assign');
			
		$this->callDELETE('project/' . $thread['project_id'] . '/thread/' . $thread['id'] .'/label/'. $label['id'] . '/revoke');
	}


	public function test_resource_with_upload_feature(){

		$threads = $this->buildFakeThreads();

		$file = $this->makeFile('Abbott.png');

		$project = Project::find($threads[0]['project_id']);

		$response = $this->callPOST('upload', ['path' => $project->slug . '/thread','caption'=>'This is sasadsad my work'],['uploaded_file' => $file]);

		// $this->assertEquals('Abbott.png', $response['client_name']);
		$this->assertEquals('This is sasadsad my work', $response['caption']);

		$this->assertTrue(Storage::exists('abc-traders/' . $project->slug . '/thread'));

		$resource = $this->callPOST('project/'.$threads[0]['project_id'] . '/thread/'.$threads[0]['id'].'/resource/' . $response['id']);

		$this->assertEquals($resource['imagable_id'], $threads[0]['id']);

		$this->assertTrue(Storage::exists($resource['path']));

		$this->callDELETE('resource/delete/' . $resource['id']);

		$this->assertFalse(Storage::exists($resource['path']));

		$files[] = $this->callPOST('upload', ['path' => $project->slug . '/thread','caption'=>'This is asdasd my work'],['uploaded_file' => $file]);

		$files[] = $this->callPOST('upload', ['path' => $project->slug . '/thread','caption'=>'This isasd  my work'],['uploaded_file' => $file]);

		$files[] =  $this->callPOST('upload', ['path' => $project->slug . '/thread','caption'=>'This isasd  my work'],['uploaded_file' => $file]);

		$this->callPOST('project/'.$threads[0]['project_id'] . '/thread/'.$threads[0]['id'].'/resource/' . $files[0]['id']);
		$files[1] = $this->callPOST('project/'.$threads[0]['project_id'] . '/thread/'.$threads[0]['id'].'/resource/' . $files[1]['id']);
		$this->callPOST('project/'.$threads[0]['project_id'] . '/thread/'.$threads[0]['id'].'/resource/' . $files[2]['id']);

		$files[2] = $this->callPOST('project/'.$threads[0]['project_id'] . '/thread/'.$threads[0]['id'].'/resource/'. $files[2]['id']  .'/make-cover');
	}


	public function test_all_projects(){
		$res = $this->callGET('project/all');

	}


	public function callPOST($url, $data =  [],$files = [], $statusCode = 200, $companyId = 1)
	{
		return $this->_call('POST', $url, $data, $files , $statusCode, $companyId);
	}

	public function callGET($url, $data =  [],$files = [], $statusCode = 200, $companyId = 1)
	{
		return $this->_call('GET', $url, $data, $files , $statusCode, $companyId);
	}

	public function callDELETE($url, $data =  [], $files = [], $statusCode = 200, $companyId = 1)
	{
		return $this->_call('DELETE', $url, $data, $files , $statusCode, $companyId);
	}

	public function _call( $method, $url, $data, $files = [], $statusCode, $companyId)
	{
		$response = $this->withSession($this->buildSession($companyId))
			->call($method, '/api/v1/' . $url,$data, [], $files, ['HTTP_X-Requested-With' => 'XMLHttpRequest']);
		
		$this->assertResponseStatus($statusCode);

		return $this->parseResponse($response)['data'];
	}

	public function buildSession($companyId = 1){
		return ['company' => json_encode([
				'id' => $companyId,
				'unique' => 'abc-traders'
			])];
	}

	public function parseResponse($response, $pluckData = false){
		$res = json_decode($response->getContent(), true); 
		return $pluckData ? $res['data'] : $res;
	}

	public function makeFile($name)
	{
		$stub = '/var/www/html/content/tests/files/' . $name;
                
        $path = sys_get_temp_dir() . '/' .$name;
        
        copy( $stub, $path );

        $file = new UploadedFile( $path, $name, filesize($path), 'image/png', null, true);

        return $file;
	}
}

//https://github.com/Leimi/drawingboard.js
//https://github.com/websanova/wPaint