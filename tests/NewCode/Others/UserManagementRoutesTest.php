<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Company;
use Modules\UserManagement\Entities\Invitation;

use App\User;

class UserMangementRoutesTest extends TestCase
{
	use Illuminate\Foundation\Testing\DatabaseTransactions;

	protected $session;

	public function setUp()
	{
		parent::setUp();
		$this->session = ['company' => 
			json_encode([
				'id' => 1,
				'unique' => 'abc-traders'
			])
		];

		$this->be( User::find(1) );
	}

	public function test_get_user_by_roles_response_codes(){
		
		$this->be( User::find(4) );

		$response = $this->withSession(
			
			$this->session

		)->call('GET','/api/v1/role/1/users');

		$this->assertResponseStatus(200);

		$this->be( User::find(1) );

		$response = $this->withSession(
			
			$this->session

		)->call('GET','/api/v1/role/1/users');

		$this->assertResponseStatus(200);
	}


	public function test_get_user_list_response(){
		
		$response = $this->withSession(
			
			$this->session

		)->call('GET','/api/v1/role/admin/users');

		$this->assertResponseStatus(200);

		$response = $this->parseResponse($response, true);

		$this->assertTrue( is_array( $response ) );

		if( count($response) !== 0 )
		{
			$first = $response[ 0];
			$this->assertTrue(isset($first['id']));		
			$this->assertEquals('admin', $first['role']['name']);
		}

		$response = $this->withSession(
			
			$this->session

		)->call('GET','/api/v1/role/all/users');

		$response = $this->parseResponse($response, true);

		$allRoles = (array_column(array_column($response,'role'),'name'));

		// $this->assertTrue(in_array('admin',$allRoles));
		// $this->assertTrue(in_array('client',$allRoles));
		// $this->assertTrue(in_array('developer',$allRoles));

		$this->assertResponseStatus(200);
	}

	public function test_update_profile(){

		// $response = $this
		// 	->withSession($this->session)
		// 	->call('POST','/api/v1/update/profile', [ 'email'=>'bikram@gmail' ], [], [], ['HTTP_X-Requested-With' => 'XMLHttpRequest'] );

		// $this->assertResponseStatus(422);

		$response = $this
			->withSession($this->session)
			->call('POST','/api/v1/update/profile', [ 'name'=>'Ram Prasad' ], [], [], ['HTTP_X-Requested-With' => 'XMLHttpRequest'] );

		$this->assertResponseStatus( 200 );
		
		$data = $this->parseResponse($response, true);

		$this->assertEquals('Ram Prasad', $data['name']);
	}

	public function test_invite_user() {

		$response = $this
			->withSession($this->session)
			->call('POST','/api/v1/admin/invite/user/role/2', [ 'email'=>'sishir@gmail.com' ], [], [], ['HTTP_X-Requested-With' => 'XMLHttpRequest'] );

		$this->assertResponseStatus( 200 );

		$data = $this->parseResponse($response, true);

		$this->assertEquals('sishir@gmail.com', $data['email']);

		$in = Invitation::whereEmail('sishir@gmail.com')->first();

		$this->assertNotNull($in);

		$this->assertEquals(0, $in->accepted);

		$response = $this->call('GET','invitation/'.$in->id.'/token/'. $in->token );

		$this->assertResponseStatus( 200 );
/*
		To-do: need to make post request with token by asserting that response is view

		$in = Invitation::whereEmail('sishir@gmail.com')->first();

		$in->accepted = 1;
		
		$in->save();

		$user = new User();

		$this->assertEquals(1, $in->accepted);

		$user = User::find($in->user_id);

		$this->assertTrue($user->hasRole('client'));

		$this->assertNotNull($user);

		$company = $in->company;

		$this->assertTrue($company->belongs($user));

		DB::enableQueryLog();

		$response = $this
			->withSession($this->buildSession())
			->call('POST', '/api/v1/admin/update/role/4/user/' . $user->id);

		$this->assertTrue( $user->hasRole('developer') );
		
		$this->assertResponseStatus( 200 );

		$this->be( User::find(4) );

		$response = $this
			->withSession($this->buildSession(2))
			->call('DELETE','/api/v1/admin/remove/user/'.$user->id);

		$this->be( User::find(1) );

		$response = $this
			->withSession($this->buildSession())
			->call('DELETE','/api/v1/admin/remove/user/'.$user->id);

		$this->assertResponseStatus( 200 );

		$company = Company::find($company->id);

		$this->assertFalse($company->belongs($user));
		*/
	}


	public function test_change_password()
	{
		$this
			->call('POST','api/v1/change/password', [ 'password'=>'strong_password' ],[],[],['HTTP_X-Requested-With' => 'XMLHttpRequest']);
		
		// $this->assertResponseStatus( 200 );

		// $user =  User::find(1);

		// $this->assertTrue(Hash::check('strong_password', $user->password));
	}

	public function buildSession($companyId = 1){
		return ['company' => json_encode([
				'id' => $companyId,
				'unique' => 'abc-traders'
			])];
	}

	public function parseResponse($response, $pluckData = false){
		$res = json_decode($response->getContent(), true);
		return $pluckData ? $res['data'] : $res;
	}
}