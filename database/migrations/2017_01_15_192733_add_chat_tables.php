<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddChatTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('companies', function(Blueprint $table){
            $table->increments('id');
            $table->string('name');
            $table->string('sub_domain');
            $table->boolean('active')->default(1);
            $table->boolean('paid')->default(0);
            $table->timestamps();
        });

        Schema::create('company_users', function(Blueprint $table){
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('company_id')->unsigned();
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');
            $table->timestamps();
        });

        Schema::create('channels', function(Blueprint $table){
            $table->increments('id');
            $table->string('unique');

            $table->integer('company_id')->unsigned();
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');
           
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
           
            $table->string('name')->nullable();
            $table->string('description')->nullable();
            $table->boolean('private'); //private channel 1, normal channel 0
            $table->boolean('for_users'); //user based channel 1, //basic chaneel, 0
            $table->timestamps();
        });

        Schema::create('channel_users', function(Blueprint $table){
            $table->increments('id');
            $table->integer('channel_id')->unsigned()->nullable();
            $table->foreign('channel_id')->references('id')->on('channels')->onDelete('cascade');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });

        Schema::create('chats', function(Blueprint $table){

            $table->increments('id');
            
            $table->integer('company_id')->unsigned();
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');

            $table->integer('author_id')->unsigned();
            $table->foreign('author_id')->references('id')->on('users')->onDelete('cascade');
            
            $table->integer('to')->unsigned()->nullable();
            $table->foreign('to')->references('id')->on('users')->onDelete('cascade');
            
            $table->integer('channel_id')->unsigned()->nullable();
            $table->foreign('channel_id')->references('id')->on('channels')->onDelete('cascade');


            $table->tinyInteger('type'); //1 stands for private, 2 stands for channel
            $table->boolean('edited')->default(0);
            
            $table->longText('message');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
